@extends('layouts.auth')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6 mt-5 sm-mt-0">
        <div class="form-content">
            <h4 class="title divider-3 text-uppercase text-white">{{ __('Login') }}</h4>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="section-field mb-2">
                    <div class="field-widget"> <i class="glyph-icon flaticon-user"></i>
                        <input id="username" class="web @error('email') is-invalid @enderror" type="email" placeholder="Email" name="email" value="{{ old('email') }}"/>
                    </div>
                </div>
                <div class="section-field mb-1">
                    <div class="field-widget"> <i class="glyph-icon flaticon-padlock"></i>
                        <input id="Password" class="Password @error('password') is-invalid @enderror" type="password" placeholder="Password" name="password" value="{{ old('password') }}" />
                    </div>
                </div>
                <div class="section-field mb-1">
                    <div class="col-md-6">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="section-field text-uppercase">
                    <a href="{{ route('password.request') }}" class="float-right text-white">Forgot Password?</a>
                </div>
                <div class="clearfix"></div>
                <div class="section-field text-uppercase text-right mt-2">
                    <button type="submit" class="button  btn-lg btn-theme full-rounded animated right-icn"><span>login<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></button>
                </div>
                <div class="clearfix"></div>
                <div class="section-field mt-2 text-center text-white">
                    <p class="lead mb-0">Don’t have an account? <a class="text-white" href="{{ route('register') }}"><u>Register now!</u> </a></p>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
