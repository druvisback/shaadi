@extends('layouts.auth')

@section('content')
<div class="login-form" style="background: url("{{ {{ ExtraController::assetURL('images/login-full-bg.png') }}") center no-repeat; background-size: contain;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-5 sm-mt-0">
                <div class="form-content register-form-content">
                    <h4 class="title divider-3 text-uppercase text-white">zzzzzzzzzzzz</h4>
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <div class="section-field mb-2">
                            <div class="field-widget"> <i class="glyph-icon flaticon-user"></i>
                                <input id="username" class="web @error('email') is-invalid @enderror" type="email" placeholder="{{ __('E-Mail Address') }}" name="email" />
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="section-field mb-1">
                            <div class="field-widget"> <i class="glyph-icon flaticon-padlock"></i>
                                <input id="Password" class="Password @error('password') is-invalid @enderror" type="password" placeholder="Password" name="password" required autocomplete="new-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="section-field mb-1">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="password" name="password_confirmation" placeholder="Password Confirmation" required autocomplete="new-password" id="remember" {{ old('password_confirmation') }} />
                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
