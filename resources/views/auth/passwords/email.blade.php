@extends('layouts.auth')

@section('content')
<div class="login-form" style="background: url("{{ ExtraController::assetURL('images/login-full-bg.png') }}") center no-repeat; background-size: contain;">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8 mt-5 sm-mt-0">

                <div class="form-content register-form-content">
                    <h4 class="title divider-3 text-uppercase text-white">{{ __('Reset Password') }}</h4>

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="section-field mb-2">
                            <div class="field-widget"> <i class="glyph-icon flaticon-user"></i>
                                <input id="username" class="web @error('email') is-invalid @enderror" type="email" placeholder="{{ __('E-Mail Address') }}" name="email" />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="section-field text-uppercase text-right mt-2">
                            <button type="submit" class="button btn-lg btn-theme full-rounded animated right-icn">
                                {{ __('Reset Password') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
