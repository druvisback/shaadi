@extends('layouts.auth')

@section('content')
<div class="login-form" style="background: url({{ ExtraController::assetURL('images/login-full-bg.png') }}) center no-repeat; background-size: contain;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-5 sm-mt-0">
                <div class="form-content register-form-content">
                    <h4 class="title divider-3 text-uppercase text-white">Register</h4>
                    <form action="{{ route('register') }}" method="post">
                    @csrf
                    <div class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Name</h5>
                            </div>
                            <div class="col-md-4">
                                <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" name="first_name" class="form-control" type="text" placeholder="First name" value="{{ old('first_name') }}"/>
                            </div>
                            <div class="col-md-4">
                                <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" name="last_name" class="form-control" type="text" placeholder="Last name" value="{{ old('last_name') }}"/>
                            </div>
                        </div>
                    </div>

                    <div class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Email</h5>
                            </div>
                            <div class="col-md-4">
                                <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" name="email" class="form-control" type="email" placeholder="Email" value="{{ old('email') }}"/>
                            </div>
                        </div>
                    </div>

                    <div class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Password</h5>
                            </div>
                            <div class="col-md-4">
                                <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" name="password" class="form-control" type="password" placeholder="Password"/>
                            </div>
                        </div>
                    </div>

                    <div class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Confirm Password</h5>
                            </div>
                            <div class="col-md-4">
                                <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" name="password_confirmation" class="form-control" type="password" placeholder="Confirm Password"/>
                            </div>
                        </div>
                    </div>

                    <div class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Profile for</h5>
                            </div>
                            <div class="col-md-4">
                                <select name="profile_for" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        $old_profile_for = old('profile_for');
                                    ?>
                                    <option value="me" {{ ($old_profile_for == 'me')?' selected':'' }}>Me</option>
                                    <option value="daughter" {{ ($old_profile_for == 'daughter')?' selected':'' }}>My Daughter</option>
                                    <option value="son" {{ ($old_profile_for == 'son')?' selected':'' }}>My Son</option>
                                    <option value="sister" {{ ($old_profile_for == 'sister')?' selected':'' }}>My Sister</option>
                                    <option value="brother" {{ ($old_profile_for == 'brother')?' selected':'' }}>My Brother</option>
                                    <option value="father" {{ ($old_profile_for == 'father')?' selected':'' }}>Friend</option>
                                    <option value="mother" {{ ($old_profile_for == 'mother')?' selected':'' }}>Relative</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Gender</h5>
                            </div>
                            <?php
                                $old_gender = old('gender');
                                if(!isset($old_gender)):
                                    $old_gender = 'female';
                                endif;
                            ?>
                            <div class="col-md-2">
                                <div class="radio">
                                    <input name="gender" @if($old_gender == "female") checked="checked" @endif id="radio1" type="radio" value="female">
                                    <label for="radio1">Female</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="radio">
                                    <input name="gender" id="radio2" type="radio" value="male" @if($old_gender == "male") checked="checked" @endif />
                                    <label for="radio2">Male</label>
                                </div>
                            </div>
{{--                            <div class="col-md-2">--}}
{{--                                <div class="radio">--}}
{{--                                    <input name="gender" id="radio3" type="radio" value="other" @if($old_gender == "other") checked="checked" @endif />--}}
{{--                                    <label for="radio3">Transgender</label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div id="religion-area" class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Religion</h5>
                            </div>
                            <div class="col-md-8">
                                <select name="religion" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        $old_religion = old('religion');
                                        $religions = \App\model\Religion::all()->sortBy('name');
                                        foreach($religions as $religion):
                                    ?>
                                    <option value="{{ $religion->id }}"{{(isset($old_religion) && $old_religion == $religion->id)?' selected':''}}>{{ $religion->name }}</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="cast-area" class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Caste</h5>
                            </div>
                            <div class="col-md-8">
                                <select name="cast" class="form-control">
                                    <?php
                                    $old_cast = old('cast');
                                    $casts = [];
                                    if(isset($old_religion)):
                                        $casts = \App\model\Cast::where('ref_religion',$old_religion)->get()->sortBy('name');
                                    endif;

                                    if(empty($casts)): ?>
                                        <option  value="">None</option>
                                    <?php else: ?>
                                       <option value="">Select</option>
                                    <?php foreach($casts as $cast): ?>
                                        <option value="{{ $cast->id }}"{{(isset($old_cast) && $old_cast == $cast->id)?' selected':''}}>{{ $cast->name }}</option>
                                    <?php endforeach; endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Mother Tongue</h5>
                            </div>
                            <div class="col-md-8">
                                <select name="language" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        $old_language = old('language');
                                        $languages = \App\model\Language::all()->sortBy('name');
                                        foreach($languages as $language):
                                    ?>
                                        <option value="{{ $language->id }}"{{(isset($old_language) && $old_language == $language->id)?' selected':''}}>{{ $language->name }}</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="section-field mb-3">
                        <div class="row sm-mt-2">
                            <div class="col-md-4">
                                <h5 class="text-white">Living in</h5>
                            </div>
                            <div class="col-md-8">
                                <select name="country" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        $old_country = old('country');
                                        $countries = \App\model\Country::all()->sortBy('name');
                                        foreach($countries as $country):
                                    ?>
                                    <option value="{{ $country->id }}"{{(isset($old_country) && $old_country == $country->id)?' selected':''}}>{{ $country->name }}</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                        <div class="section-field mb-3">
                            <div class="row sm-mt-2">
                                <div class="col-md-4">
                                    <h5 class="text-white">Referral ID</h5>
                                </div>
                                <div class="col-md-8">
                                    <input class="form-control" value="{{ old('referral_profile_id') }}" placeholder="#PROFILE ID" name="referral_profile_id"/>
                                    <!--<select name="country" class="form-control"></select>-->
                                </div>
                            </div>
                        </div>

                    <div class="section-field mb-3 mt-6 text-white text-center" style="padding-left: 30px;">
                        <div class="checkbox">
                            <?php
                                $old_terms_and_condition = old('terms_and_condition');
                                $old_terms_and_condition = isset($old_terms_and_condition);
                            ?>
                            <input name="terms_and_condition" id="check1" type="checkbox" @if($old_terms_and_condition) checked="checked" @endif/>
                            <label for="check1" class="text-white">I accept Lagan Utsav's <a href="#" class="text-white"><u>Terms and Condition</u></a> and <a class="text-white" href="#"><u>Privacy Policy </u></a></label>
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="section-field text-uppercase text-right mt-2">
                        <button type="submit" class="button btn-lg btn-theme full-rounded animated right-icn"><span>register<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="section-field mt-2 text-center text-white">
                        <p class="lead mb-0">Already have an account? <a class="text-white" href="{{ route('login') }}"><u>Login now!</u> </a></p>
                    </div>
</form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    let $dom_religionSelector = $('#religion-area select');
    let $dom_castSelector = $('#cast-area select');

    if($dom_religionSelector.length && $dom_castSelector.length){
        $dom_religionSelector.on('change',function(){
            let $dom = $(this);
            let reigionId = $dom.val();
            $.ajax({
                url: "{{ route('resources.getCastByReligionID') }}",
                type: 'GET',
                data: {id: reigionId},
                success: function(response){
                    if(response.success){
                        $dom_castSelector.empty();
                        let data = response.data;
                        $dom_castSelector.append("<option value='' selected>Select</option>");
                        for(let i=0; i < data.length; i++) {
                            $dom_castSelector.append("<option value=\""+data[i].id+"\">"+data[i].name+"</option>");
                        }
                    }else{
                        $dom_castSelector.empty();
                        $dom_castSelector.append("<option value=''>None</option>");
                    }
                },
                error: function(data){
                    alert("Sorry! No casts found related to religion.");
                }
            });
            //console.log("Changed => ", $dom.val());
        });
    }else{
        alert("Religion & Cast module not working properly, please reload or contact admin...");
    }
</script>
@endsection
