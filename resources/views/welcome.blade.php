@include('layouts.header')

<!--=================================
 banner -->
<section id="main-slider" class="fullscreen">
    <div class="banner banner-3 bg-1 bg-overlay-red" style="background-image: url('{{ ExtraController::assetURL('images/bg/bg-6.jpg') }}');">
        <div class="slider-static">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 col-md-12 mt-5 sm-mt-0">
                        <div class="banner-form">
                            <form class="form-inner" action="{{route('search.quick-search')}}" method="get">

                                <div class="form-group">

                                    <ul>
                                        <li>
                                            <label class="control-label">I'm looking for</label>
                                            <select required name="gender" class="form-control">
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
{{--                                                <option value="other">Other</option>--}}
                                            </select>
                                        </li>
                                        <li>
                                            <label class="control-label">From Age</label>
                                            <select required name="from_age" class="form-control">
                                                @for($i = 18; $i <= 80; $i++)
                                                <option>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </li>
                                        <li>
                                            <label class="control-label">To Age</label>
                                            <select required name="to_age" class="form-control">
                                                @for($i = 18; $i <= 80; $i++)
                                                    <option {{$i==23?'selected':''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </li>
                                        <li id="religion-area">
                                            <label class="control-label">of Religion</label>
                                            <select required name="ref_religion" class="form-control">
                                                <option value="any">Any</option>
                                                <?php
                                                    $religions = \App\model\Religion::all()->sortBy('name');
                                                    foreach($religions as $religion):
                                                ?>
                                                    <option value="{{$religion->id}}">{{$religion->name}}</option>
                                                <?php
                                                    endforeach
                                                ?>
                                            </select>
                                        </li>
                                        <li id="cast-area">
                                            <label class="control-label">caste/community</label>
                                            <select required name="ref_cast" class="form-control">
                                                <option value="any">Any</option>
                                            </select>
                                        </li>
                                        <li>
                                            <label class="control-label">Mother tongue</label>
                                            <select required name="ref_language" class="form-control">
                                                <option value="any">Any</option>
                                                <?php
                                                    $languages = \App\model\Language::all()->sortBy('name');
                                                    foreach($languages as $language):
                                                ?>
                                                    <option value="{{$language->id}}">{{$language->name}}</option>
                                                <?php
                                                    endforeach
                                                ?>
                                            </select>
                                        </li>
                                        <li style="text-align: center;padding: 0px;display: inline-flex;justify-content: center;">
                                            <button style="text-align: center;background-color: brown;display: flex;justify-content: center;align-items: center;" type="submit" class="btn">search <i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></button>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    let $dom_religionSelector = $('#religion-area select');
    let $dom_castSelector = $('#cast-area select');

    if($dom_religionSelector.length && $dom_castSelector.length){
        $dom_religionSelector.on('change',function(){
            let $dom = $(this);
            let reigionId = $dom.val();
            $.ajax({
                url: "{{ route('resources.getCastByReligionID') }}",
                type: 'GET',
                data: {id: reigionId},
                success: function(response){
                    if(response.success){
                        $dom_castSelector.empty();
                        let data = response.data;
                        $dom_castSelector.append("<option value='' selected>Select</option>");
                        for(let i=0; i < data.length; i++) {
                            $dom_castSelector.append("<option value=\""+data[i].id+"\">"+data[i].name+"</option>");
                        }
                    }else{
                        $dom_castSelector.empty();
                        $dom_castSelector.append("<option value=''>None</option>");
                    }
                },
                error: function(data){
                    alert("Sorry! No casts found related to religion.");
                }
            });
            //console.log("Changed => ", $dom.val());
        });
    }else{
        alert("Religion & Cast module not working properly, please reload or contact admin...");
    }
</script>


<section class="page-section-ptb grey-bg story-slider">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h2 class="title title3 divider wow fadeInUp">They Found True Love</h2>
            </div>
        </div>
    </div>
    <div class="owl-carousel" data-nav-dots="true" data-items="4" data-md-items="4" data-sm-items="2" data-xx-items="1" data-space="30">
        @foreach($found_love as $pair)
            <div class="item">
                <div class="story-item">
                    <div class="story-image clearfix">
                        <div class="row" style="width: 100%;padding: 0px;margin: 0px;">
                            <div class="col-6" style="height: 243px;padding: 0px;">
                                <div style="background: url('<?=ExtraController::getProfilePhotoURL($pair['male']); ?>') center no-repeat; background-size: cover; width: 100%; height: 100%;"></div>
                            </div>
                            <div class="col-6" style="height: 243px;padding: 0px;">
                                <div style="background: url('<?=ExtraController::getProfilePhotoURL($pair['female']); ?>') center no-repeat; background-size: cover; width: 100%; height: 100%;"></div>
                            </div>
                        </div>
                        <!--<div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>-->
                    </div>
                    <div class="story-details text-center">
                        <h5 class="title divider-3">{{ $pair['male']->first_name}} &amp; {{$pair['female']->first_name}}</h5>
                    </div>
                </div>
            </div>
        @endforeach
        <!--<div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/02.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">Adam &amp; Eve</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/03.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">Bella &amp; Edward</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/04.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">DEMI &amp; HEAVEN</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/05.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">David &amp; Bathsheba</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/06.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">Eros &amp; Psychi</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/07.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">Hector &amp; Andromache</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/08.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">Bonnie &amp; Clyde</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/09.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">Henry &amp; Clare</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/10.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">Casanova &amp; Francesca</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/11.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">Jack &amp; Sally</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="story-item">
                <div class="story-image clearfix"><img class="img-fluid" src="{{ ExtraController::assetURL('images/story/12.jpg') }}" alt="" />
                    <div class="story-link"><a href="#"><i class="glyph-icon flaticon-add"></i></a></div>
                </div>
                <div class="story-details text-center">
                    <h5 class="title divider-3">James &amp; Lilly</h5>
                    <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
                </div>
            </div>
        </div>-->
    </div>
</section>


<section class="page-section-ptb text-center bg fixed" style="background-image:url({{ ExtraController::assetURL('images/bg/bg-7.jpg') }})">
    <div class="container">
        <div class="row justify-content-center mb-5 sm-mb-3">
            <div class="col-md-8">
                <h2 class="title title2 divider text-white mb-3 wow fadeInUp animated">Find your Special Soul Mate</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 sm-mb-3">
                <div class="timeline-badge mb-2 wow slideInLeft"><img class="img-center" src="{{ ExtraController::assetURL('images/timeline/01.png') }}" alt="" /></div>
                <h4 class="title mb-3 text-white text-capitalize"><a href="{{route('cpanel')}}">Sign Up</a></h4>

            </div>
            <div class="col-md-4 sm-mb-3">
                <div class="timeline-badge mb-2"><img class="img-center" src="{{ ExtraController::assetURL('images/timeline/02.png') }}" alt="" /></div>
                <h4 class="title mb-3 text-white text-capitalize">Connect</h4>

            </div>
            <div class="col-md-4">
                <div class="timeline-badge mb-2 wow slideInRight"><img class="img-center" src="{{ ExtraController::assetURL('images/timeline/03.png') }}" alt="" /></div>
                <h4 class="title mb-3 text-white text-capitalize">Interract</h4>

            </div>
        </div>
    </div>
</section>



<!--=================================
Testimonial-->

<section class="page-section-ptb pb-130 sm-pb-6">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h2 class="title title3 divider wow fadeInUp">Testimonials</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="owl-carousel" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1">
                    <?php
                        $testimonials = \App\model\Testimonial::all();

                        foreach($testimonials as $testimonial):
                    ?>
                    <div class="item">
                        <div class="testimonial left_pos">
                            <div class="testimonial-avatar"> <img alt="" src="{{ ExtraController::testimonialPhotoURL($testimonial) }}"> </div>
                            <div class="testimonial-info">{{$testimonial->desc}}</div>
                            <div class="author-info"> <strong>{{$testimonial->name}} - <span>{{$testimonial->country}}</span></strong> </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>


<!--=================================
Membership to connect -->

<div class="page-section-ptb grey-bg text-center membership">
    <div class="container">
        <div class="row justify-content-center mb-5 sm-mb-3">
            <div class="col-md-8">
                <h2 class="title title3 divider mb-3 wow fadeInUp animated">Membership To Connect With Those You Like</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 sm-mb-3">
                <div class="block">
                    <div class="timeline-badge mb-2 wow slideInLeft"><img class="img-center" src="{{ ExtraController::assetURL('images/timeline/phone-call.png') }}" alt="" /></div>
                    <h4 class="title mb-3 text-capitalize"><a href="{{route('page.contact')}}">View Contacts</a></h4>
                    <p>Access contact number and connect directly on call or via sms</p>
                </div>


            </div>
            <div class="col-md-4 sm-mb-3">
                <div class="block">
                    <div class="timeline-badge mb-2"><img class="img-center" src="{{ ExtraController::assetURL('images/timeline/letter.png') }}" alt="" /></div>
                    <h4 class="title mb-3 text-capitalize"><a href="{{route('page.contact')}}">View Email</a></h4>
                    <p>Get the email id of your selected profile and reach via email</p>
                </div>


            </div>
            <div class="col-md-4">
                <div class="block">
                    <div class="timeline-badge mb-2 wow slideInRight"><img class="img-center" src="{{ ExtraController::assetURL('images/timeline/like.png') }}" alt="" /></div>
                    <h4 class="title mb-3 text-capitalize"><a href="{{route('page.contact')}}">Chat</a></h4>
                    <p>Chat instantly with all other
                        online users</p>
                </div>

            </div>
        </div>
    </div>
</div>

<!--=================================
 page-section -->

<section class="page-section-ptb bg fixed text-white bg-overlay-black-50" style="background-image:url({{ ExtraController::assetURL('images/bg/bg-5.jpg') }})">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h2 class="title title2 divider mb-3 wow fadeInUp">Get The Best Dating Team Now</h2>
                <h5 class="pb-20">Want to hear more story, register today.</h5>
                @if(Auth::check())
                    <a href="{{route('search')}}" class="button  btn-lg btn-theme full-rounded animated right-icn"><span>Get Your Soul Mate<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a> </div>
                @else
                      <a href="{{route('register')}}" class="button  btn-lg btn-theme full-rounded animated right-icn"><span>Become a Member<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a> </div>
                @endif
        </div>
    </div>
</section>

@include('layouts.footer')
