<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lagan Utsav</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-slider.min.css" rel="stylesheet" type="text/css" />

    <link href="css/mega-menu/mega_menu.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="css/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="css/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />


    <link href="css/general.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <!-- Style customizer -->
    <!--<link rel="stylesheet" type="text/css" href="css/skins/skin-default.css" data-style="styles" />-->

</head>

<body>


<!--=================================
header -->
<header id="header" class="fancy">

    <!--=================================
   mega menu -->
    <div class="menu header-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- menu start -->
                    <nav id="menu" class="mega-menu">
                        <!-- menu list items container -->
                        <section class="menu-list-items">
                            <!-- menu logo -->
                            <ul class="menu-logo">
                                <li><a href="index.html"><img id="logo_color_img" src="images/dark-logo.png" alt="logo" /> </a></li>
                            </ul>
                            <!-- menu links -->
                            <ul class="menu-links">
                                <!-- active class -->
                                <li class="active"><a href="#"> Matches </a></li>
                                <li><a href="#"> Search </a></li>
                                <li><a href="#"> Inbox </a></li>
                                <li><a href="#"> Upgrade Now </a></li>
                                <li><a href="#"> Help </a></li>
                                <li><a href="#"> Account <i class="fa fa-angle-down fa-indicator"></i></a>
                                    <ul class="drop-down-multilevel right-menu">
                                        <li><a href="#">My Profile</a></li>
                                        <li><a href="#">Account settings</a></li>
                                        <li><a href="#">Contact filter</a></li>
                                        <li><a href="#">Email/SMS Alert</a></li>
                                        <li><a href="#">Privacy option</a></li>
                                        <li><a href="#">Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </section>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-header">
        <ul class="menu-bottom-links">
            <li><a href="#"> New Matches (100) </a></li>
            <li><a href="#"> Today's Matches (20) </a></li>
            <li><a href="#"> My Matches (800) </a></li>
            <li><a href="#"> Near Me (20) </a></li>
            <li><a href="#"> Recently Viewed (10) </a></li>
            <li><a href="#"> More Matches </a></li>
        </ul>
    </div>
</header>

<!--=================================
 header -->




<!--=================================
 Page Section -->
<section class="page-section-ptb dashboard">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="profile-indiv-info">
                    <div class="profile-content">
                        <h4 class="divider-3">Match List</h4>
                        <div class="list-single">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                  <img src="images/profile/06.jpg" class="list-profile">
                                    <p style="margin-top: 5px;">To view his photo <br> <a href="#">Register Now</a></p>
                                </div>
                                <div class="col-md-9">
                                   <div class="row">
                                       <div class="col-md-8">
                                           <div class="id-list">
                                              <p>Matrimony ID: <span>566TREC533D</span></p>
                                               <p>Online 2 days ago</p>
                                           </div>
                                           <div class="profile-listing">
                                                        <ul>
                                                            <li><span>Age/Height</span>22/ 5.8'</li>
                                                            <li><span>Marital Status</span> Never Married</li>
                                                            <li><span>Religion / Community</span>Hindu</li>
                                                            <li><span>Location</span>Patna</li>
                                                            <li><span>Mother Tongue</span>Hindi</li>
                                                        </ul>
                                                    </div>
                                       </div>
                                       <div class="col-md-4">
                                           <ul class="des-list">
                                               <li><a href="#" class="btn-interest">I'm interested</a></li>
                                               <li><a href="#"><i class="fa fa-star" style="color: #888;"></i>Shortlist</a></li>
                                               <li><a href="#"><i class="fa fa-minus-circle" style="color: #fb0303;"></i>Ignore</a></li>
                                               <li><a href="#"><i class="fa fa-user" style="color: #3e8bff;"></i> View full Profile</a></li>
                                           </ul>
                                       </div>
                                   </div>
                                   <div class="decription">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dolorum esse id ipsam ipsum libero minima, minus numquam perspiciatis provident quasi quo...
                                       <a href="#">Read more</a> </p>
                                   </div>
                                </div>
                            </div>
                        </div>

                        <div class="list-single">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <img src="images/profile/01.jpg" class="list-profile">
                                    <p style="margin-top: 5px;">To view his photo <br> <a href="#">Register Now</a></p>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="id-list">
                                                <p>Matrimony ID: <span>566TREC533D</span></p>
                                                <p>Online 2 days ago</p>
                                            </div>
                                            <div class="profile-listing">
                                                <ul>
                                                    <li><span>Age/Height</span>22/ 5.8'</li>
                                                    <li><span>Marital Status</span> Never Married</li>
                                                    <li><span>Religion / Community</span>Hindu</li>
                                                    <li><span>Location</span>Patna</li>
                                                    <li><span>Mother Tongue</span>Hindi</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="des-list">
                                                <li><a href="#" class="btn-interest">I'm interested</a></li>
                                                <li><a href="#"><i class="fa fa-star" style="color: #888;"></i>Shortlist</a></li>
                                                <li><a href="#"><i class="fa fa-minus-circle" style="color: #fb0303;"></i>Ignore</a></li>
                                                <li><a href="#"><i class="fa fa-user" style="color: #3e8bff;"></i> View full Profile</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="decription">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dolorum esse id ipsam ipsum libero minima, minus numquam perspiciatis provident quasi quo...
                                            <a href="#">Read more</a> </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="list-single">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <img src="images/profile/02.jpg" class="list-profile">
                                    <p style="margin-top: 5px;">To view his photo <br> <a href="#">Register Now</a></p>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="id-list">
                                                <p>Matrimony ID: <span>566TREC533D</span></p>
                                                <p>Online 2 days ago</p>
                                            </div>
                                            <div class="profile-listing">
                                                <ul>
                                                    <li><span>Age/Height</span>22/ 5.8'</li>
                                                    <li><span>Marital Status</span> Never Married</li>
                                                    <li><span>Religion / Community</span>Hindu</li>
                                                    <li><span>Location</span>Patna</li>
                                                    <li><span>Mother Tongue</span>Hindi</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="des-list">
                                                <li><a href="#" class="btn-interest">I'm interested</a></li>
                                                <li><a href="#"><i class="fa fa-star" style="color: #888;"></i>Shortlist</a></li>
                                                <li><a href="#"><i class="fa fa-minus-circle" style="color: #fb0303;"></i>Ignore</a></li>
                                                <li><a href="#"><i class="fa fa-user" style="color: #3e8bff;"></i> View full Profile</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="decription">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dolorum esse id ipsam ipsum libero minima, minus numquam perspiciatis provident quasi quo...
                                            <a href="#">Read more</a> </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="list-single">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <img src="images/profile/08.jpg" class="list-profile">
                                    <p style="margin-top: 5px;">To view his photo <br> <a href="#">Register Now</a></p>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="id-list">
                                                <p>Matrimony ID: <span>566TREC533D</span></p>
                                                <p>Online 2 days ago</p>
                                            </div>
                                            <div class="profile-listing">
                                                <ul>
                                                    <li><span>Age/Height</span>22/ 5.8'</li>
                                                    <li><span>Marital Status</span> Never Married</li>
                                                    <li><span>Religion / Community</span>Hindu</li>
                                                    <li><span>Location</span>Patna</li>
                                                    <li><span>Mother Tongue</span>Hindi</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="des-list">
                                                <li><a href="#" class="btn-interest">I'm interested</a></li>
                                                <li><a href="#"><i class="fa fa-star" style="color: #888;"></i>Shortlist</a></li>
                                                <li><a href="#"><i class="fa fa-minus-circle" style="color: #fb0303;"></i>Ignore</a></li>
                                                <li><a href="#"><i class="fa fa-user" style="color: #3e8bff;"></i> View full Profile</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="decription">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dolorum esse id ipsam ipsum libero minima, minus numquam perspiciatis provident quasi quo...
                                            <a href="#">Read more</a> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!--=================================
 page-section -->


<!--=================================
footer -->
<footer class="text-white text-center">
    <div class="footer-widget sm-mt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img src="images/dark-logo.png" class="img-fluid" alt="logo" id="footer-logo">
                </div>
                <div class="col-md-3">
                    <h6>Main Menu</h6>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Premium Plans</a></li>
                        <li><a href="#">Happy Stories</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>

                </div>
                <div class="col-md-3">
                    <h6>Quick search</h6>
                    <ul>
                        <li><a href="#">All Members</a></li>
                        <li><a href="#">Premium Members</a></li>
                        <li><a href="#">Free Members</a></li>
                    </ul>

                </div>
                <div class="col-md-3">
                    <h6>useful links</h6>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>

                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <p class="text-white">© 2019  - LaganUtsav | All Right Reserved </p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!--=================================
footer -->


<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-level-up"></i></a></div>

<!--=================================
 jquery -->

<!-- jquery  -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/bootstrap-slider.min.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/mega-menu/mega_menu.js"></script>
<script type="text/javascript" src="js/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    var wow = new WOW(
        {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       100,          // distance to the element when triggering the animation (default is 0)
            mobile:       true,       // trigger animations on mobile devices (default is true)
            live:         true,       // act on asynchronously loaded content (default is true)
            callback:     function(box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            }
        }
    );
    wow.init();
</script>

</body>

</html>
