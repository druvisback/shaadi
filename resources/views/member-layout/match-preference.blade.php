@extends('layouts.member')

@section('body-content')
    <section class="page-section-ptb dashboard">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="profile-indiv-info">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-area" action="{{ route('cpanel.member.preference-setup') }}" method="post">
                                @csrf()
                                <div class="profile-content">
                                    <h4 class="divider-3">Tell us what you are looking for in a life partner</h4>

                                    <div class="form-content">

                                        <div class="section-field mb-5">
                                            <div class="row sm-mt-2">
                                                <div class="col-md-4">
                                                    <h6>Age preference</h6>
                                                </div>
                                                <div class="col-md-8">
                                                    <div>
                                                        <?php
                                                            $from_age_def = 18;
                                                            if(isset($preference_data)):
                                                                $from_age_def = $preference_data['from_age'];
                                                            endif;
                                                            $from_age = (old('from_age'))?old('from_age'):$from_age_def;
                                                        ?>
                                                        <label>From Age (<span class="lbl_from_age">{{ $from_age }}</span>)</label>
                                                        <input type="range" min="18" max="100" value="{{ $from_age }}" class="form-control slider" name="from_age"/>
                                                        <br>
                                                        <?php
                                                            $to_age_def = 23;
                                                            if(isset($preference_data)):
                                                                $to_age_def = $preference_data['to_age'];
                                                            endif;
                                                            $to_age = (old('to_age'))?old('to_age'):$to_age_def;
                                                        ?>
                                                        <label>To Age (<span class="lbl_to_age">{{ $to_age }}</span>)</label>
                                                        <input type="range" min="18" max="100" value="{{ $to_age }}" class="form-control slider" name="to_age"/>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="section-field mb-5">
                                            <div class="row sm-mt-2">
                                                <div class="col-md-4">
                                                    <h6>Height preference</h6>
                                                </div>
                                                <div class="col-md-8">
                                                    <div>
                                                        <label>From Height (cm)</label>
                                                        <?php
                                                            $to_height_cm_def = 182;
                                                            if(isset($preference_data)):
                                                                $to_height_cm_def = $preference_data['to_height_cm'];
                                                            endif;
                                                            $to_height_cm = (old('to_height_cm'))?old('to_height_cm'):$to_height_cm_def;

                                                            $from_height_cm_def = 170;
                                                            if(isset($preference_data)):
                                                                $from_height_cm_def = $preference_data['from_height_cm'];
                                                            endif;
                                                            $from_height_cm = (old('from_height_cm'))?old('from_height_cm'):$from_height_cm_def;
                                                        ?>
                                                        <input type="number" min="100" value="{{ $from_height_cm }}" class="form-control slider" name="from_height_cm"/>
                                                        <br>
                                                        <label>To Height (cm)</label>
                                                        <input type="number" min="100" value="{{ $to_height_cm }}" class="form-control slider" name="to_height_cm"/>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="section-field mb-3">
                                            <div class="row sm-mt-2">
                                                <div class="col-md-4">
                                                    <h6>Looking For</h6>
                                                </div>
                                                <div class="col-md-8">
                                                    <?php
                                                        $looking_for_def = '';
                                                        if(isset($preference_data)):
                                                            $looking_for_def = $preference_data['looking_for'];
                                                        endif;
                                                        $looking_for = (old('looking_for'))?old('looking_for'):$looking_for_def;
                                                    ?>
                                                    <select name="looking_for" class="form-control">
                                                        <option value="">Select</option>
                                                        <option value="male" {{ ($looking_for == 'male')?'selected':'' }}>Male</option>
                                                        <option value="female" {{ ($looking_for == 'female')?'selected':'' }}>Female</option>
                                                        <option value="other" {{ ($looking_for == 'other')?'selected':'' }}>Trans</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="section-field mb-3">
                                            <div class="row sm-mt-2">
                                                <div class="col-md-4">
                                                    <h6>Marital Status</h6>
                                                </div>
                                                <div class="col-md-8">
                                                    <?php
                                                    $marital_status_def = '';
                                                    if(isset($preference_data)):
                                                        $marital_status_def = $preference_data['marital_status'];
                                                    endif;
                                                    $marital_status = (old('marital_status'))?old('marital_status'):$marital_status_def;
                                                    ?>
                                                    <select name="marital_status" class="form-control">
                                                        <option value="">Select</option>
                                                        <option value="unmarried" {{ ($marital_status == 'unmarried')?'selected':'' }}>Never Married</option>
                                                        <option value="divorced" {{ ($marital_status == 'divorced')?'selected':'' }}>Divorced</option>
                                                        <option value="widower" {{ ($marital_status == 'widower')?'selected':'' }}>Widower</option>
                                                        <option value="any" {{ ($marital_status == 'any')?'selected':'' }}>Any</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="religion-area" class="section-field mb-3">
                                            <div class="row sm-mt-2">
                                                <div class="col-md-4">
                                                    <h6>Religion</h6>
                                                </div>
                                                <div class="col-md-8">
                                                    <?php
                                                    $ref_religion_def = '';
                                                    if(isset($preference_data)):
                                                        $ref_religion_def = $preference_data['ref_religion'];
                                                    endif;
                                                    $ref_religion = (old('ref_religion'))?old('ref_religion'):$ref_religion_def;
                                                    ?>
                                                    <select name="ref_religion" class="form-control">
                                                        <option value="">Select</option>
                                                        <?php
                                                        $old_religion = $ref_religion;
                                                        $religions = \App\model\Religion::all();
                                                        foreach($religions as $religion):
                                                            ?>
                                                            <option value="{{ $religion->id }}"{{(isset($old_religion) && $old_religion == $religion->id)?' selected':''}}>{{ $religion->name }}</option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="cast-area" class="section-field mb-3">
                                            <div class="row sm-mt-2">
                                                <div class="col-md-4">
                                                    <h6>Caste/community</h6>
                                                </div>
                                                <div class="col-md-8">
                                                    <?php
                                                    $ref_cast_def = '';
                                                    if(isset($preference_data)):
                                                        $ref_cast_def = $preference_data['ref_cast'];
                                                    endif;
                                                    $ref_cast = (old('ref_cast'))?old('ref_cast'):$ref_cast_def;
                                                    ?>
                                                    <select name="ref_cast" class="form-control">
                                                        <?php
                                                        $old_cast = $ref_cast;
                                                        $casts = [];
                                                        if(isset($old_religion)):
                                                            $casts = \App\model\Cast::where('ref_religion',$old_religion)->get();
                                                        endif;

                                                        if(empty($casts)): ?>
                                                            <option  value="">None</option>
                                                        <?php else: ?>
                                                            <option value="">Select</option>
                                                            <?php foreach($casts as $cast): ?>
                                                                <option value="{{ $cast->id }}"{{(isset($old_cast) && $old_cast == $cast->id)?' selected':''}}>{{ $cast->name }}</option>
                                                            <?php endforeach; endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="section-field mb-3">
                                            <div class="row sm-mt-2">
                                                <div class="col-md-4">
                                                    <h6>Mother Tongue</h6>
                                                </div>
                                                <div class="col-md-8">
                                                    <?php
                                                        $ref_language_def = '';
                                                        if(isset($preference_data)):
                                                            $ref_language_def = $preference_data['ref_language'];
                                                        endif;
                                                        $ref_language = (old('ref_language'))?old('ref_language'):$ref_language_def;
                                                    ?>
                                                    <select name="ref_language" class="form-control">
                                                        <option value="">Select</option>
                                                        <?php
                                                        $old_language = $ref_language;
                                                        $languages = \App\model\Language::all();
                                                        foreach($languages as $language):
                                                            ?>
                                                            <option value="{{ $language->id }}"{{(isset($old_language) && $old_language == $language->id)?' selected':''}}>{{ $language->name }}</option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="profile-content">
                                    <h5>Location Details</h5>
                                    <div id="country-area" class="section-field mb-3">
                                        <div class="row sm-mt-2">
                                            <div class="col-md-4">
                                                <h6>Country Living in</h6>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                $ref_country_def = '';
                                                if(isset($preference_data)):
                                                    $ref_country_def = $preference_data['ref_country'];
                                                endif;
                                                $ref_country = (old('ref_country'))?old('ref_country'):$ref_country_def;
                                                ?>
                                                <select name="ref_country" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    $old_country = $ref_country;
                                                    $countries = \App\model\Country::all();
                                                    foreach($countries as $country):
                                                        ?>
                                                        <option value="{{ $country->id }}"{{(isset($old_country) && $old_country == $country->id)?' selected':''}}>{{ $country->name }}</option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="state-area" class="section-field mb-3">
                                        <div class="row sm-mt-2">
                                            <div class="col-md-4">
                                                <h6>State Living in</h6>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                    $ref_state_def = '';
                                                    if(isset($preference_data)):
                                                        $ref_state_def = $preference_data['ref_state'];
                                                    endif;
                                                    $ref_state = (old('ref_state'))?old('ref_state'):$ref_state_def;
                                                ?>
                                                <select name="ref_state" class="form-control">
                                                    <?php
                                                    $old_state = $ref_state;
                                                    $casts = [];
                                                    if(isset($old_country)):
                                                        $states = \App\model\State::where('ref_country',$old_country)->get();
                                                    endif;

                                                    if(empty($states)): ?>
                                                        <option  value="">None</option>
                                                    <?php else: ?>
                                                        <option value="">Select</option>
                                                        <?php foreach($states as $state): ?>
                                                            <option value="{{ $state->id }}"{{(isset($old_state) && $old_state == $state->id)?' selected':''}}>{{ $state->name }}</option>
                                                        <?php endforeach; endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="city-area" class="section-field mb-3">
                                        <div class="row sm-mt-2">
                                            <div class="col-md-4">
                                                <h6>City/District</h6>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                    $ref_city_def = '';
                                                    if(isset($preference_data)):
                                                        $ref_city_def = $preference_data['ref_city'];
                                                    endif;
                                                    $ref_city = (old('ref_city'))?old('ref_city'):$ref_city_def;
                                                ?>
                                                <select name='ref_city' class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    $old_city = $ref_city;
                                                    $cities = [];
                                                    if(isset($old_state)):
                                                        $cities = \App\model\City::where('ref_state',$old_state)->get();
                                                    endif;

                                                    if(empty($cities)): ?>
                                                        <option  value="">None</option>
                                                    <?php else: ?>
                                                        <?php foreach($cities as $city): ?>
                                                            <option value="{{ $city->id }}"{{(isset($old_city) && $old_city == $city->id)?' selected':''}}>{{ $city->name }}</option>
                                                        <?php endforeach; endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="section-field text-right text-uppercase mt-5">
                                    <button type="submit" class="button  btn-lg btn-theme full-rounded animated right-icn"><span>@if(isset($preference_data)) Update @else Save & Continue @endif<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></button>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
<script>
    let $dom_religionSelector = $('#religion-area select');
    let $dom_castSelector = $('#cast-area select');



    if($dom_religionSelector.length && $dom_castSelector.length){
        $dom_religionSelector.on('change',function(){
            let $dom = $(this);
            let reigionId = $dom.val();
            $.ajax({
                url: "{{ route('resources.getCastByReligionID') }}",
                type: 'GET',
                data: {id: reigionId},
                success: function(response){
                    if(response.success){
                        $dom_castSelector.empty();
                        let data = response.data;
                        $dom_castSelector.append("<option value='' selected>Select</option>");
                        for(let i=0; i < data.length; i++) {
                            $dom_castSelector.append("<option value=\""+data[i].id+"\">"+data[i].name+"</option>");
                        }
                    }else{
                        $dom_castSelector.empty();
                        $dom_castSelector.append("<option value=''>None</option>");
                    }
                },
                error: function(data){
                    alert("Sorry! No casts found related to religion.");
                }
            });
            //console.log("Changed => ", $dom.val());
        });
    }else{
        alert("Religion & Cast module not working properly, please reload or contact admin...");
    }

    let $dom_countrySelector = $('#country-area select');
    let $dom_stateSelector = $('#state-area select');
    let $dom_citySelector = $('#city-area select');

    if($dom_countrySelector.length && $dom_stateSelector.length && $dom_citySelector.length){
        $dom_countrySelector.on('change',function(){
            let $dom = $(this);
            let countryId = $dom.val();
            $.ajax({
                url: "{{ route('resources.getStateByCountryID') }}",
                type: 'GET',
                data: {id: countryId},
                success: function(response){
                    if(response.success){
                        $dom_stateSelector.empty();
                        let data = response.data;
                        $dom_stateSelector.append("<option value='' selected>Select</option>");
                        for(let i=0; i < data.length; i++) {
                            $dom_stateSelector.append("<option value=\""+data[i].id+"\">"+data[i].name+"</option>");
                        }
                    }else{
                        $dom_stateSelector.empty();
                        $dom_stateSelector.append("<option value=''>None</option>");
                    }
                    $dom_stateSelector.trigger('change');
                },
                error: function(data){
                    alert("Sorry! No states found related to country.");
                }
            });
            //console.log("Changed => ", $dom.val());
        });
        $dom_stateSelector.on('change',function(){
            let $dom = $(this);
            let stateId = $dom.val();
            $.ajax({
                url: "{{ route('resources.getCityByCountryID') }}",
                type: 'GET',
                data: {id: stateId},
                success: function(response){
                    $dom_citySelector.empty();
                    if(response.success){
                        let data = response.data;
                        $dom_citySelector.append("<option value='' selected>Select</option>");
                        for(let i=0; i < data.length; i++) {
                            $dom_citySelector.append("<option value=\""+data[i].id+"\">"+data[i].name+"</option>");
                        }
                    }else{
                        $dom_citySelector.append("<option value=''>None</option>");
                    }
                },
                error: function(data){
                    alert("Sorry! No cities found related to state.");
                }
            });
            //console.log("Changed => ", $dom.val());
        });
        
        //Age slider
        let $lbl_from_age = $("span.lbl_from_age");
        let $range_from_age = $('input[name=from_age]');
        $range_from_age.on('input', function(){
            $lbl_from_age.html($(this).val());
        });
        
        let $lbl_to_age = $("span.lbl_to_age");
        let $range_to_age = $('input[name=to_age]');
        $range_to_age.on('input', function(){
            $lbl_to_age.html($(this).val());
        });
    }else{
        alert("Country, State & City module not working properly, please reload or contact admin...");
    }
</script>
@stop


