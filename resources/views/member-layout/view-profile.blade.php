@extends('layouts.member')

@section('body-content')
    <style>
        .form-group{
            margin: 0;
            display: flex;
            position: inherit !important;
        }
        .form-group label{
            margin: 0 !important;

            font-family: 'Montserrat', sans-serif;
            font-style: normal;
            font-size: 13px !important;
            line-height: 24px !important;
            color: #3d3d3d !important;
            font-weight: 600;
            padding: 0 !important;
            display: flex;
            /* justify-content: center; */
            align-items: center;
        }
        .form-group .form-control, .form-group .form-control-plaintext, .form-group input, .form-group select{
            line-height: 24px;
            font-size: 13px;
            font-family: 'Montserrat', sans-serif;
            padding: 5px;
            font-weight: 600;
            background-color: white;
            color: black;
            border: darkgrey solid thin;
        }
        .profile-listing ul{
            display: flex;
            flex-wrap: wrap;
        }
        .profile-listing ul li{
            padding: 0 10px;
        }
    </style>
        <section class="page-section-ptb dashboard">

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="profile-indiv-info">
                        <button onclick="window.history.go(-1); return false;" class="btn btn-warning"><- Go Back</button>
                        </div>

                        <div class="profile-indiv-info">
                            <div class="row">
                                <div class="col-md-3">
                                    <figure>
                                        <a href="#">
                                            <?php
                                            $gender = $profile->gender;
                                            $photo_url = ExtraController::getProfilePhotoURL($profile);
                                            if(!isset($photo_url)):
                                                switch($gender):
                                                    case 'male':
                                                        $photo_url = asset('public/images/male-icon.png');
                                                        break;
                                                    case 'female':
                                                        $photo_url = asset('public/images/female-icon.png');
                                                        break;
                                                    default:
                                                        $photo_url = asset('public/images/trans-icon.png');
                                                        break;
                                                endswitch;
                                            endif;
                                            ?>
                                            <img src="{{ $photo_url }}" class="img-fluid" alt="">
                                        </a>
                                    </figure>
                                </div>
                                <div class="col-md-9">
                                    <div class="profile-content">
                                        <h3>{{ $profile->first_name." ".$profile->last_name }}</h3>
                                        <div class="profile-listing">
                                            <ul>
                                                <?php
                                                //Age Calculate
                                                $age = "Not Specified";
                                                $dob = $profile->dob;
                                                if($dob):
                                                    $age = Carbon\Carbon::parse($dob)->age.' yrs';
                                                endif;

                                                //Height Calculate
                                                $height_str = "Not Specified";
                                                $height_cm = $profile->height_cm;
                                                if($height_cm):
                                                    $height_inches = $height_cm/2.54;
                                                    $height_feet = intval($height_inches/12);
                                                    $height_inches = $height_inches%12;
                                                    $height_str = $height_feet.'\''.$height_inches.'"';
                                                endif;
                                                ?>
                                                <li><span>Age/Height</span>{{$age}} / {{$height_str}}</li>
                                                <?php
                                                //Marital Status
                                                $str_marital_status = "Not Specified";
                                                $marital_status = $profile->marital_status;
                                                if($marital_status):
                                                    switch($marital_status):
                                                        case 'unmarried':
                                                            $str_marital_status = "Un-married";
                                                            break;
                                                        case 'married':
                                                            $str_marital_status = "Married";
                                                            break;
                                                        case 'divorced':
                                                            $str_marital_status = "Divorced";
                                                            break;
                                                        case 'widow':
                                                            $str_marital_status = "Widow";
                                                            break;
                                                    endswitch;
                                                endif;
                                                ?>
                                                <li><span>Marital Status</span> {{$str_marital_status}}</li>
                                                <?php
                                                //Sun Sign Status
                                                $str_sun_sign = "Not Specified";
                                                $sun_sign = $profile->sun_sign;
                                                if($sun_sign):
                                                    $str_sun_sign = ucfirst($sun_sign);
                                                endif;
                                                ?>
                                                <li><span>Sun Sign</span>{{$str_sun_sign}}</li>
                                                <li><span>Religion / Community</span>{{ App\model\Religion::where('id', $profile->ref_religion)->first()->name }}</li>
                                                <li><span>Location</span>{{ App\model\Country::where('id', $profile->ref_country)->first()->name }}</li>
                                                <li><span>Mother Tongue</span>{{ App\model\Language::where('id', $profile->ref_language)->first()->name }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="profile-indiv-info">
                            <h4 class="divider-3">About Myself</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="profile-content">
                                        <div class="profile-listing">
                                            <h5>Basic & lifestyle</h5>
                                            @if(!$profile->have_privacy_basic_lifestyle)
                                            <ul>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Date of Birth :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <input class="form-control-plaintext" disabled value="<?php $dob = $profile->dob; $date = \Illuminate\Support\Carbon::make($dob);  echo $date->format('d F, Y'); ?>"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Marital Status :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_marital_status = old('marital_status',$profile->marital_status);
                                                                $marital_status_txt = "unknown";
                                                                switch($old_marital_status):
                                                                    case 'unmarried':
                                                                        $marital_status_txt = "Un-Married";
                                                                        break;
                                                                    case 'married':
                                                                        $marital_status_txt = "Married";
                                                                        break;
                                                                    case 'divorced':
                                                                        $marital_status_txt = "Divorced";
                                                                        break;
                                                                    case 'widow':
                                                                        $marital_status_txt = "Widow";
                                                                        break;
                                                                endswitch;
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{$marital_status_txt}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Height (in cms):</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_height_cm = old('height_cm',$profile->height_cm);
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{$old_height_cm}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Body Type :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_body_type = old('body_type',$profile->body_type);
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{ucfirst($old_body_type)}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Weight (in kgs):</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_weight_kg = old('weight_kg',$profile->weight_kg);
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{$old_weight_kg}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Grew Up In :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_ref_country_grew_up_in = old('ref_country_grew_up_in',$profile->ref_country_grew_up_in);
                                                                $country = \App\model\Country::where('id',$old_ref_country_grew_up_in)->first();
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{$country->name}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Diet :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_diet = old('diet',$profile->diet);
                                                                $txt_diet = "unknown";
                                                                switch($old_diet):
                                                                    case 'veg':
                                                                        $txt_diet = "Vegetarian";
                                                                        break;
                                                                    case 'nonveg':
                                                                        $txt_diet = "Non-Vegetarian";
                                                                        break;
                                                                    case 'both':
                                                                        $txt_diet = "Veg & Non-Veg";
                                                                        break;
                                                                endswitch;
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{$txt_diet}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Drink :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_drink = old('drink',$profile->drink);
                                                                $txt_drink = "unknown";
                                                                switch($old_drink):
                                                                    case 'no':
                                                                        $txt_drink = "Not at All";
                                                                        break;
                                                                    case 'occasionally':
                                                                        $txt_drink = "Occasionally";
                                                                        break;
                                                                    case 'regular':
                                                                        $txt_drink = "Regular";
                                                                        break;
                                                                    case 'heavy':
                                                                        $txt_drink = "Heavy Drunker";
                                                                        break;
                                                                endswitch;
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{$txt_drink}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Smoke :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_smoke = old('smoke',$profile->smoke);
                                                                $txt = "unknown";
                                                                switch($old_smoke):
                                                                    case 'no':
                                                                        $txt = "Not at All";
                                                                        break;
                                                                    case 'occasionally':
                                                                        $txt = "Occasionally";
                                                                        break;
                                                                    case 'regular':
                                                                        $txt = "Regular";
                                                                        break;
                                                                    case 'heavy':
                                                                        $txt = "Heavy Smoke";
                                                                        break;
                                                                endswitch;
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{$txt}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Sun Sign :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_sun_sign = old('sun_sign',$profile->sun_sign);
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{ucfirst($old_sun_sign)}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Blood Group :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_blood_group = old('blood_group',$profile->blood_group);
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{ucfirst($old_blood_group)}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Disability :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_have_disability = old('have_disability',$profile->have_disability);
                                                                ?>
                                                                <input class="form-control-plaintext" disabled value="{{($old_have_disability)?'Yes':'No'}}"/>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Health Information :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_health_description = old('health_description',$profile->health_description);
                                                                ?>
                                                                <textarea rows="6" disabled class="form-control-plaintext">{{ $old_health_description }}</textarea>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Self Description :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_self_description = old('self_description',$profile->self_description);
                                                                ?>
                                                                <textarea rows="6" disabled class="form-control-plaintext" >{{ $old_self_description }}</textarea>
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ul>
                                            @else
                                                <div class="alert alert-light" role="alert">
                                                    Basic & Lifestyle information are private.
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="profile-indiv-info">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="profile-content">
                                        <div class="profile-listing">
                                            <h5>Family Details</h5>
                                            @if(!$profile->have_privacy_family_details)
                                                <ul>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Father Info. :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_family_father_status = old('family_father_status',$profile->family_father_status);
                                                                ?>
                                                                <textarea rows="6" disabled class="form-control-plaintext">{{ $old_family_father_status }}</textarea>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Mother Info. :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_family_mother_status = old('family_mother_status',$profile->family_mother_status);
                                                                ?>
                                                                <textarea rows="6" disabled class="form-control-plaintext" >{{ $old_family_mother_status }}</textarea>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">No. of Sisters :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_family_no_of_sister = old('family_no_of_sister',$profile->family_no_of_sister);
                                                                ?>
                                                                <input type="number" name="family_no_of_sister" class="form-control-plaintext" required disabled value="{{ $old_family_no_of_sister }}" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">No. of Brothers :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_family_no_of_brother = old('family_no_of_brother',$profile->family_no_of_brother);
                                                                ?>
                                                                <input type="number" name="family_no_of_brother" class="form-control-plaintext" required disabled value="{{ $old_family_no_of_brother }}" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Family Values/Description :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $old_family_description = old('family_description',$profile->family_description);
                                                                ?>
                                                                <textarea rows="6" name="family_description" class="form-control-plaintext" disabled required>{{ $old_family_description }}</textarea>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            @else
                                                <div class="alert alert-light" role="alert">
                                                    Family Details information are private.
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="profile-indiv-info">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="profile-content">
                                        <div class="profile-listing contact-list">
                                            <h5>Location Details</h5>
                                            @if(!$profile->have_privacy_location_details)
                                                <ul>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">Country :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <input type="text" class="form-control-plaintext" required value="<?=\App\model\Country::where('id',$profile->ref_country)->first()->name; ?>" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">State :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $f = \App\model\State::where('id',$profile->ref_state)->first();
                                                                ?>
                                                                <input type="text" class="form-control-plaintext" required value="<?=$f?$f->name:' -- '; ?>" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group row">
                                                            <label class="col-sm-12 col-form-label">City/District :</label>
                                                            <div class="col-sm-12" style="padding: 0;">
                                                                <?php
                                                                $f = \App\model\City::where('id',$profile->ref_city)->first();
                                                                ?>
                                                                <input type="text" class="form-control-plaintext" required value="<?=$f?$f->name:' -- '; ?>" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            @else
                                                <div class="alert alert-light" role="alert">
                                                    Location Details information are private.
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                            $gallery = $profile->gallery_photos;
                            if($gallery):
                                $arrGallery = unserialize($gallery);
                                if($arrGallery && is_array($arrGallery) && !empty($arrGallery)):
                        ?>
                        <div class="profile-indiv-info">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="profile-content">
                                        <div class="profile-listing contact-list">
                                            <h5>My Photo Gallery ({{count($arrGallery)}})</h5>
                                            @if(!$profile->have_privacy_gallery_photo)
                                                <div class="container">
                                                    <div class="row">
                                                        @foreach($arrGallery as $photo)
                                                            <?php $img_url = 'http://webinch.com/developed/shaadi/storage/app/public/user_gallery/'.$photo; ?>
                                                            <div class="col-md-3 col-dm-12">
                                                                <div class="card omg-img" style="height:150px; background: url('{{ $img_url }}') center no-repeat; background-size: cover;">
                                                                    {{--<a href="{{ $img_url }}" target="_blank">--}}
                                                                    {{--<img class="card-img-top" src="{{ $img_url }}" alt="Card image cap">--}}
                                                                    {{--</a>--}}
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @else
                                                <div class="alert alert-light" role="alert">
                                                    Gallery Photo information are private.
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; endif; ?>
                    </div>
                </div>
            </div>

        </section>


    <script>
        $(".card.omg-img").on("contextmenu",function(){
            return false;
        });
    </script>
@stop
