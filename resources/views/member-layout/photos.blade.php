@extends('layouts.member')

@section('body-content')
    <style>
        .form-group{
            margin: 0;
            display: flex;
            position: inherit !important;
        }
        .form-group label{
            margin: 0 !important;

            font-family: 'Montserrat', sans-serif;
            font-style: normal;
            font-size: 13px !important;
            line-height: 24px !important;
            color: #3d3d3d !important;
            font-weight: 600;
            padding: 0 !important;
            display: flex;
            /* justify-content: center; */
            align-items: center;
        }
        .form-group .form-control, .form-group .form-control-plaintext, .form-group input, .form-group select{
            line-height: 24px;
            font-size: 13px;
            font-family: 'Montserrat', sans-serif;
            border: black solid thin;
            padding: 5px;
            background-color: #e6f5ee;
            font-weight: 600;
        }
        .profile-listing ul{
            display: flex;
            flex-wrap: wrap;
        }
        .profile-listing ul li{
            padding: 0 10px;
        }
        .my-gallery{
            padding: 13px 8px;
        }
        .my-gallery .col-md-2.col-sm-12{
            padding: 5px;
            overflow: hidden;
        }
        .my-gallery .col-md-2.col-sm-12 .omg-close a{
            color: #f72222;
        }
        .my-gallery .col-md-2.col-sm-12 .omg-close{
            opacity: 0;
            z-index: 999999999;
            position: absolute;
            left: 5px;
            top: 5px;
            transition: all 0.7s;
            color: #f72222;
            background-color: #ffffff78;
            padding: 2px 13px;
        }
        .my-gallery .col-md-2.col-sm-12:hover > .omg-close{
            opacity: 1;
            left: 158px;
        }
        .my-gallery img{
            max-width: 100%;
            transition: all 0.7s;
        }
        .my-gallery img:hover{
            transform: scale(1.5);
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="form-group" style="display: flex;
    justify-content: center;
    text-align: center;">
                <form action="{{ route('cpanel.member.photo-setup') }}" method="post" enctype="multipart/form-data">
                    @csrf()
                <h4 class="divider-3">Add Photos For Gallery  </h4>
                <input type="file" class="form-control" name="new_photo"/>
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            </div>
            <div class="my-gallery col-md-12">
                <div class="row">
                    <?php
                        $user = \Illuminate\Support\Facades\Auth::user();
                        $profile = \App\model\Profile::where('ref_user', $user->id)->first();

                        $gallery_response = \App\Http\Controllers\Member\Pages\PhotoController::getPhotoURLs();
                        if($gallery_response['success']):
                    foreach($gallery_response['data'] as $photo):
                        $url = $photo['url'];
                        $path = $photo['path'];
                        //$file = \Illuminate\Support\Facades\Storage::get('public/user_gallery'.$path);
                        //dd($file);
                        /*$list = collect(Storage::files('public/user_gallery'))->map(function($file) {
                            return Storage::url($file);
                        });


                        dd($list);*/
                        $img_url = \App\Http\Controllers\ExtraController::getGalleryPhotoURL($photo['path'], $profile->id);
                    ?>
                    <div class="col-md-2 col-sm-12">
                        <div class="omg-close"><a href="{{route('cpanel.member.photo-delete',$path)}}">X</a></div>
                        <a href="{{$img_url}}" target="_blank">
                        <img alt="gallery-photo" src="{{$img_url}}"/>
                        </a>
                    </div>
                    <?php endforeach; else: ?>
                        <div class="alert alert-warning" role="alert">
                            Please upload photo for gallery!
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

@stop
