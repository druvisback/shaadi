@extends('layouts.member')

@section('body-content')

    <script>
        function onClickDeleteProfile(e) {
            if(confirm('Are you want to delete your account permanently?')){
                let confirmPassword = prompt("Please confirm your password", "");
                confirmPassword = confirmPassword.trim();
                if(confirmPassword && confirmPassword.length > 0){
                    let confirmPasswordLink = "{{route('cpanel.member.delete-profile',[''])}}/"+confirmPassword;
                    console.log({confirmPasswordLink});
                    location.href = confirmPasswordLink;
               }
            }
            e.preventDefault();
            return false;
        }
    </script>

<style>
    .form-group{
        margin: 0;
        display: flex;
        position: inherit !important;
    }
    .form-group label{
        margin: 0 !important;

        font-family: 'Montserrat', sans-serif;
        font-style: normal;
        font-size: 13px !important;
        line-height: 24px !important;
        color: #3d3d3d !important;
        font-weight: 600;
        padding: 0 !important;
        display: flex;
        /* justify-content: center; */
        align-items: center;
    }
    .form-group .form-control, .form-group .form-control-plaintext, .form-group input, .form-group select{
        line-height: 24px;
        font-size: 13px;
        font-family: 'Montserrat', sans-serif;
        border: black solid thin;
        padding: 5px;
        background-color: #e6f5ee;
        font-weight: 600;
    }
    .profile-listing ul{
        display: flex;
        flex-wrap: wrap;
    }
    .profile-listing ul li{
        padding: 0 10px;
    }
    .photo_upload_icon{
        position: absolute;
        top: 0px;
        bottom: 0px;
        height: 91%;
        width: 76%;
        display: flex;
        justify-content: center;
        align-items: center;
        opacity: 0;
        transition: all 0.7s;
        cursor: pointer;
     }
    .photo_upload_icon:hover{
        opacity: 0.7;
    }
</style>
<form action="{{ route('cpanel.member.profile-setup') }}" method="post" enctype="multipart/form-data">
<section class="page-section-ptb dashboard">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    @csrf()
                <div class="profile-indiv-info">
                    <div class="row">
                        <div class="col-md-3">
                            <figure>
                                    <div class="photo_upload_icon">
                                        <img src="{{ ExtraController::assetURL('images/camera.png') }}" width="70px" height="70px"/>

                                    </div>
                                    <?php
                                        $gender = $profile->gender;
                                        $photo_url = \App\Http\Controllers\ExtraController::getProfilePhotoURL($profile);
                                    ?>
                                    <img class="img-fluid preview_profile_photo" src="{{ $photo_url }}" alt="">
                            </figure>
                        </div>
                        <input type="file" name="profile_photo" style="display: none;"/>
                        <div class="col-md-9">
                            <div class="profile-content">
                                <h3>{{ $profile->first_name." ".$profile->last_name }}</h3>
                                <div class="profile-listing">
                                    <ul>
                                        <?php
                                            //Age Calculate
                                            $age = "Not Specified";
                                            $dob = $profile->dob;
                                            if($dob):
                                                $age = Carbon\Carbon::parse($dob)->age.' yrs';
                                            endif;

                                            //Height Calculate
                                            $height_str = "Not Specified";
                                            $height_cm = $profile->height_cm;
                                            if($height_cm):
                                                $height_inches = $height_cm/2.54;
                                                $height_feet = intval($height_inches/12);
                                                $height_inches = $height_inches%12;
                                                $height_str = $height_feet.'\''.$height_inches.'"';
                                            endif;
                                        ?>
                                        <li><span>Age/Height</span>{{$age}} / {{$height_str}}</li>
                                        <?php
                                            //Marital Status
                                            $str_marital_status = "Not Specified";
                                            $marital_status = $profile->marital_status;
                                            if($marital_status):
                                                switch($marital_status):
                                                    case 'unmarried':
                                                        $str_marital_status = "Un-married";
                                                        break;
                                                    case 'married':
                                                        $str_marital_status = "Married";
                                                        break;
                                                    case 'divorced':
                                                        $str_marital_status = "Divorced";
                                                        break;
                                                    case 'widow':
                                                        $str_marital_status = "Widow";
                                                        break;
                                                endswitch;
                                            endif;
                                        ?>
                                        <li><span>Marital Status</span> {{$str_marital_status}}</li>
                                        <?php
                                        //Sun Sign Status
                                        $str_sun_sign = "Not Specified";
                                        $sun_sign = $profile->sun_sign;
                                        if($sun_sign):
                                            $str_sun_sign = ucfirst($sun_sign);
                                        endif;
                                        ?>
                                        <li><span>Sun Sign</span>{{$str_sun_sign}}</li>
                                        <li><span>Religion</span>{{ App\model\Religion::where('id', $profile->ref_religion)->first()->name }}</li>
                                        <li><span>Location</span>{{ App\model\Country::where('id', $profile->ref_country)->first()->name }}</li>
                                        <li><span>Mother Tongue</span>{{ App\model\Language::where('id', $profile->ref_language)->first()->name }}</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a  onclick="onClickDeleteProfile(event)" href="#" type="button" class="btn btn-secondary">Delete Account</a>
                            </div>

                        </div>
                    </div>
                </div>


                    <div class="profile-indiv-info">
                        <h4 class="divider-3">About Myself</h4>
                        <div class="row" >
                            <div class="col-md-12">
                                <div class="profile-content">
                                    <div class="profile-listing">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <h5>Profile Picture Privacy</h5>
                                            </div>
                                            <div class="col-md-6 col-sm-6" style="text-align: right;">
                                                <?php
                                                $old_privacy_basic = old("have_privacy_dp");
                                                $val = ($old_privacy_basic??$profile->have_privacy_dp??0);
                                                ?>
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-secondary {{$val == 0?"focus active":""}}">
                                                    <input type="radio" name="have_privacy_dp" value="0" autocomplete="off" {{  ($val == 0)?"checked":"" }}/> Public
                                                    </label>
                                                    <label class="btn btn-secondary {{$val == 1?"focus active":""}}">
                                                    <input type="radio" name="have_privacy_dp" value="1" autocomplete="off" {{  ($val == 1)?"checked":"" }}/> Private
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-content">
                                    <div class="profile-listing">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <h5>Gallery Privacy</h5>
                                            </div>
                                            <div class="col-md-6 col-sm-6" style="text-align: right;">
                                                <?php
                                                $old_privacy_basic = old("have_privacy_gallery_photo");
                                                $val = ($old_privacy_basic??$profile->have_privacy_gallery_photo??0);
                                                ?>
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-secondary {{$val == 0?"focus active":""}}">
                                                        <input type="radio" name="have_privacy_gallery_photo" value="0" autocomplete="off" {{  ($val == 0)?"checked":"" }}/> Public
                                                    </label>
                                                    <label class="btn btn-secondary {{$val == 1?"focus active":""}}">
                                                        <input type="radio" name="have_privacy_gallery_photo" value="1" autocomplete="off" {{  ($val == 1)?"checked":"" }}/> Private
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-content">
                                    <div class="profile-listing">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                            <h5>Basic & lifestyle</h5>
                                            </div>
                                            <div class="col-md-6 col-sm-6" style="text-align: right;">
                                                <?php
                                                    $old_privacy_basic = old("have_privacy_basic_lifestyle");
                                                    $val = ($old_privacy_basic??$profile->have_privacy_basic_lifestyle??0);
                                                ?>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-secondary {{$val == 0?"focus active":""}}">
                                                            <input type="radio" name="have_privacy_basic_lifestyle" value="0" autocomplete="off" {{  ($val == 0)?"checked":"" }}/> Public
                                                        </label>
                                                        <label class="btn btn-secondary {{$val == 1?"focus active":""}}">
                                                            <input type="radio" name="have_privacy_basic_lifestyle" value="1" autocomplete="off" {{  ($val == 1)?"checked":"" }}/> Private
                                                        </label>
                                                    </div>
                                            </div>
                                        </div>
                                        <ul>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Date of Birth :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <input name="dob" type="date" class="form-control-plaintext" required value="{{ old('dob',$profile->dob) }}"/>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Marital Status :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <select name="marital_status" class="form-control-plaintext"  required>
                                                            <option value="">Select</option>
                                                            <?php
                                                                $old_marital_status = old('marital_status',$profile->marital_status);
                                                            ?>
                                                            <option value="unmarried" {{ ($old_marital_status == 'unmarried')?'selected':'' }}>Un-Married</option>
                                                            <option value="divorced"  {{ ($old_marital_status == 'divorced')?'selected':'' }}>Divorced</option>
                                                            <option value="widow"  {{ ($old_marital_status == 'widow')?'selected':'' }}>Widow</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Height (in cms):</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                            $old_height_cm = old('height_cm',$profile->height_cm);
                                                        ?>
                                                        <input step="0.01" name="height_cm" type="number" class="form-control-plaintext"  required value="{{$old_height_cm}}"/>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Body Type :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                            $old_body_type = old('body_type',$profile->body_type);
                                                        ?>
                                                        <select name="body_type" class="form-control-plaintext"  required>
                                                            <option value="">Select</option>
                                                            <option value="athlete" {{ ($old_body_type == 'athlete')?'selected':'' }}>Athlete</option>
                                                            <option value="average" {{ ($old_body_type == 'average')?'selected':'' }}>Average</option>
                                                            <option value="slim" {{ ($old_body_type == 'slim')?'selected':'' }}>Slim</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Weight (in kgs):</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                        $old_weight_kg = old('weight_kg',$profile->weight_kg);
                                                        ?>
                                                        <input step="0.01" name="weight_kg" type="number" class="form-control-plaintext"  value="{{$old_weight_kg}}" required/>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Grew Up In :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <select name="ref_country_grew_up_in" class="form-control-plaintext"  required>
                                                            <option value="">Select</option>
                                                            <?php
                                                                $old_ref_country_grew_up_in = old('ref_country_grew_up_in',$profile->ref_country_grew_up_in);
                                                                $countries = \App\model\Country::all();
                                                                foreach($countries as $country):
                                                            ?>
                                                            <option value="{{ $country->id }}" {{ ($old_ref_country_grew_up_in == $country->id)?'selected':'' }}>{{ $country->name }}</option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Diet :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                        $old_diet = old('diet',$profile->diet);
                                                        ?>
                                                        <select name="diet" class="form-control-plaintext"  required>
                                                            <option value="">Select</option>
                                                            <option value="veg" {{ ($old_diet == 'veg')?'selected':'' }}>Vegetarian</option>
                                                            <option value="nonveg" {{ ($old_diet == 'nonveg')?'selected':'' }}>Non-Vegetarian</option>
                                                            <option value="both" {{ ($old_diet == 'both')?'selected':'' }}>Both</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Drink :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                        $old_drink = old('drink',$profile->drink);
                                                        ?>
                                                        <select name="drink" class="form-control-plaintext"  required>
                                                            <option value="">Select</option>
                                                            <option value="no" {{ ($old_drink == 'no')?'selected':'' }}>No</option>
                                                            <option value="occasionally" {{ ($old_drink == 'occasionally')?'selected':'' }}>Occasionally</option>
                                                            <option value="regular" {{ ($old_drink == 'regular')?'selected':'' }}>Regular</option>
                                                            <option value="heavy" {{ ($old_drink == 'heavy')?'selected':'' }}>Heavy Drunker</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Smoke :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                        $old_smoke = old('smoke',$profile->smoke);
                                                        ?>
                                                        <select name="smoke" class="form-control-plaintext"  required>
                                                            <option value="">Select</option>
                                                            <option value="no" {{ ($old_smoke == 'no')?'selected':'' }}>No</option>
                                                            <option value="occasionally" {{ ($old_smoke == 'occasionally')?'selected':'' }}>Occasionally</option>
                                                            <option value="regular" {{ ($old_smoke == 'regular')?'selected':'' }}>Regular</option>
                                                            <option value="heavy" {{ ($old_smoke == 'heavy')?'selected':'' }}>Chain Smoker</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Sun Sign :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                        $old_sun_sign = old('sun_sign',$profile->sun_sign);
                                                        ?>
                                                        <select name="sun_sign" class="form-control-plaintext"  required>
                                                            <option value="">Select</option>
                                                            <option value="virgo" {{ ($old_sun_sign == 'virgo')?'selected':'' }}>Virgo</option>
                                                            <option value="leo" {{ ($old_sun_sign == 'leo')?'selected':'' }}>Leo</option>
                                                            <option value="libra" {{ ($old_sun_sign == 'libra')?'selected':'' }}>Libra</option>
                                                            <option value="scorpio" {{ ($old_sun_sign == 'scorpio')?'selected':'' }}>Scorpio</option>
                                                            <option value="cancer" {{ ($old_sun_sign == 'cancer')?'selected':'' }}>Cancer</option>
                                                            <option value="gemini" {{ ($old_sun_sign == 'gemini')?'selected':'' }}>Gemini</option>
                                                            <option value="taurus" {{ ($old_sun_sign == 'taurus')?'selected':'' }}>Taurus</option>
                                                            <option value="aries" {{ ($old_sun_sign == 'aries')?'selected':'' }}>Aries</option>
                                                            <option value="pisces" {{ ($old_sun_sign == 'pisces')?'selected':'' }}>Pisces</option>
                                                            <option value="aquarius" {{ ($old_sun_sign == 'aquarius')?'selected':'' }}>Aquarius</option>
                                                            <option value="capricorn" {{ ($old_sun_sign == 'capricorn')?'selected':'' }}>Capricorn</option>
                                                            <option value="sagittarius" {{ ($old_sun_sign == 'sagittarius')?'selected':'' }}>Sagittarius</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Blood Group :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                        $old_blood_group = old('blood_group',$profile->blood_group);
                                                        ?>
                                                        <select name="blood_group" class="form-control-plaintext"  required>
                                                            <option value="">Select</option>
                                                            <option value="a+" {{ ($old_blood_group == 'a+')?'selected':'' }}>A+</option>
                                                            <option value="b+" {{ ($old_blood_group == 'b+')?'selected':'' }}>B+</option>
                                                            <option value="ab+" {{ ($old_blood_group == 'ab+')?'selected':'' }}>AB+</option>
                                                            <option value="a-" {{ ($old_blood_group == 'a-')?'selected':'' }}>A-</option>
                                                            <option value="b-" {{ ($old_blood_group == 'b-')?'selected':'' }}>B-</option>
                                                            <option value="ab-" {{ ($old_blood_group == 'ab-')?'selected':'' }}>AB-</option>
                                                            <option value="o+" {{ ($old_blood_group == 'o+')?'selected':'' }}>O+</option>
                                                            <option value="o-" {{ ($old_blood_group == 'o-')?'selected':'' }}>O-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Disability :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                        $old_have_disability = old('have_disability',$profile->have_disability);
                                                        ?>
                                                        <select name="have_disability" class="form-control-plaintext"  required>
                                                            <option value="">Select</option>
                                                            <option value="1" {{ ($old_have_disability == '1')?'selected':'' }}>Yes</option>
                                                            <option value="0" {{ ($old_have_disability == '0')?'selected':'' }}>No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Health Information :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                        $old_health_description = old('health_description',$profile->health_description);
                                                        ?>
                                                        <textarea name="health_description" class="form-control-plaintext"  required>{{ $old_health_description }}</textarea>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group row">
                                                    <label class="col-sm-12 col-form-label">Self Description :</label>
                                                    <div class="col-sm-12" style="padding: 0;">
                                                        <?php
                                                        $old_self_description = old('self_description',$profile->self_description);
                                                        ?>
                                                        <textarea name="self_description" class="form-control-plaintext" required>{{ $old_self_description }}</textarea>
                                                    </div>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





                <div class="profile-indiv-info">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="profile-listing contact-list">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>Educational Details</h5>
                                        </div>

                                        <div class="col-md-6 col-sm-6" style="text-align: right;">
                                            <?php
                                            $old_privacy_basic = old("have_privacy_educational_details");
                                            $val = ($old_privacy_basic??$profile->have_privacy_educational_details??0);
                                            ?>
                                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-secondary {{$val == 0?"focus active":""}}">
                                                    <input type="radio" name="have_privacy_educational_details" value="0" autocomplete="off" {{  ($val == 0)?"checked":"" }}/> Public
                                                </label>
                                                <label class="btn btn-secondary {{$val == 1?"focus active":""}}">
                                                    <input type="radio" name="have_privacy_educational_details" value="1" autocomplete="off" {{  ($val == 1)?"checked":"" }}/> Private
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">

                                          <div class="form-group row">
                                              <label class="col-sm-12 col-form-label">Last Education Name :</label>
                                              <div class="col-sm-12" style="padding: 0;">
                                                  <?php
                                                      $old_last_education_name = old('last_education_name',$profile->last_education_name);
                                                  ?>
                                                  <input  name="last_education_name" type="text" class="form-control-plaintext"  required value="{{$old_last_education_name}}"/>
                                              </div>
                                          </div>


                                          <div class="form-group row">
                                              <label class="col-sm-12 col-form-label">Institute Name:</label>
                                              <div class="col-sm-12" style="padding: 0;">
                                                  <?php
                                                      $old_last_education_institude_name = old('last_education_institude_name',$profile->last_education_institude_name);
                                                  ?>
                                                  <input  name="last_education_institude_name" type="text" class="form-control-plaintext"  required value="{{$old_last_education_institude_name}}"/>
                                              </div>
                                          </div>

                                          <div class="form-group row">
                                              <label class="col-sm-12 col-form-label">Date Of passing :</label>
                                              <div class="col-sm-12" style="padding: 0;">
                                                  <input name="last_education_passing_year" type="date" class="form-control-plaintext" required value="{{ old('last_education_passing_year',$profile->last_education_passing_year) }}"/>
                                              </div>
                                          </div>


                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>


            <div class="profile-indiv-info">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="profile-listing">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>Family Details</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6" style="text-align: right;">
                                            <?php
                                            $old_privacy_basic = old("have_privacy_family_details");
                                            $val = ($old_privacy_basic??$profile->have_privacy_family_details??0);
                                            ?>
                                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-secondary {{$val == 0?"focus active":""}}">
                                                    <input type="radio" name="have_privacy_family_details" value="0" autocomplete="off" {{  ($val == 0)?"checked":"" }}/> Public
                                                </label>
                                                <label class="btn btn-secondary {{$val == 1?"focus active":""}}">
                                                    <input type="radio" name="have_privacy_family_details" value="1" autocomplete="off" {{  ($val == 1)?"checked":"" }}/> Private
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">Father Status :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_family_father_status = old('family_father_status',$profile->family_father_status);
                                                    ?>
                                                    <textarea name="family_father_status" class="form-control-plaintext" required>{{ $old_family_father_status }}</textarea>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">Mother Status :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_family_mother_status = old('family_mother_status',$profile->family_mother_status);
                                                    ?>
                                                    <textarea name="family_mother_status" class="form-control-plaintext" required>{{ $old_family_mother_status }}</textarea>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">No. of Sisters :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_family_no_of_sister = old('family_no_of_sister',$profile->family_no_of_sister);
                                                    ?>
                                                    <input type="number" name="family_no_of_sister" class="form-control-plaintext" required value="{{ $old_family_no_of_sister }}" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">No. of Brothers :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_family_no_of_brother = old('family_no_of_brother',$profile->family_no_of_brother);
                                                    ?>
                                                    <input type="number" name="family_no_of_brother" class="form-control-plaintext" required value="{{ $old_family_no_of_brother }}" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">Family Values/Description :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_family_description = old('family_description',$profile->family_description);
                                                    ?>
                                                    <textarea name="family_description" class="form-control-plaintext" required>{{ $old_family_description }}</textarea>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="profile-indiv-info">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="profile-listing contact-list">

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>Contact Details</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6" style="text-align: right;">
                                            <?php
                                            $old_privacy_basic = old("have_privacy_contact_details");
                                            $val = ($old_privacy_basic??$profile->have_privacy_contact_details??0);
                                            ?>
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-secondary {{$val == 0?"focus active":""}}">
                                                        <input type="radio" name="have_privacy_contact_details" value="0" autocomplete="off" {{  ($val == 0)?"checked":"" }}/> Public
                                                    </label>
                                                    <label class="btn btn-secondary {{$val == 1?"focus active":""}}">
                                                        <input type="radio" name="have_privacy_contact_details" value="1" autocomplete="off" {{  ($val == 1)?"checked":"" }}/> Private
                                                    </label>
                                                </div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">Mobile :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_mobile_no = old('mobile_no',$profile->mobile_no);
                                                    ?>
                                                    <input type="tel" name="mobile_no" class="form-control-plaintext" required value="{{ $old_mobile_no }}" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">Business Email (Will conatct by other member):</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_business_email = old('business_email',$profile->business_email);
                                                    $old_business_email = ($old_business_email)?$old_business_email:$user->email;
                                                    ?>
                                                    <input type="email" name="business_email" class="form-control-plaintext" required value="{{ $old_business_email }}" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">Contact Person :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_contact_person = old('contact_person',$profile->contact_person);
                                                    ?>
                                                    <textarea name="contact_person" class="form-control-plaintext" >{{ $old_contact_person }}</textarea>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">Relationship with The Member :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_relationship_with_member = old('relationship_with_member',$profile->relationship_with_member);
                                                    ?>
                                                    <select name="relationship_with_member" class="form-control-plaintext"  required>
                                                        <option value="">Select</option>
                                                        <option value="father" {{ ($old_relationship_with_member == 'father')?'selected':'' }}>Father</option>
                                                        <option value="mother" {{ ($old_relationship_with_member == 'mother')?'selected':'' }}>Mother</option>
                                                        <option value="brother" {{ ($old_relationship_with_member == 'brother')?'selected':'' }}>Brother</option>
                                                        <option value="sister" {{ ($old_relationship_with_member == 'sister')?'selected':'' }}>Sister</option>
                                                        <option value="friend" {{ ($old_relationship_with_member == 'friend')?'selected':'' }}>Friend</option>
                                                        <option value="relative" {{ ($old_relationship_with_member == 'relative')?'selected':'' }}>Relative</option>
                                                        <option value="self" {{ ($old_relationship_with_member == 'self')?'selected':'' }}>Self</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">Convenient Time to Call :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_time_to_call = old('time_to_call',$profile->time_to_call);
                                                    ?>
                                                    <textarea name="time_to_call" class="form-control-plaintext">{{ $old_time_to_call }}</textarea>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-indiv-info">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="profile-listing contact-list">



                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <h5>Location Details</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6" style="text-align: right;">
                                            <?php
                                            $old_privacy_basic = old("have_privacy_location_details");
                                            $val = ($old_privacy_basic??$profile->have_privacy_location_details??0);
                                            ?>
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-secondary {{$val == 0?"focus active":""}}">
                                                        <input type="radio" name="have_privacy_location_details" value="0" autocomplete="off" {{  ($val == 0)?"checked":"" }}/> Public
                                                    </label>
                                                    <label class="btn btn-secondary {{$val == 1?"focus active":""}}">
                                                        <input type="radio" name="have_privacy_location_details" value="1" autocomplete="off" {{  ($val == 1)?"checked":"" }}/> Private
                                                    </label>
                                                </div>
                                        </div>
                                        </div>

                                        <ul>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">Country :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                        $old_country_id = old('ref_country',$profile->ref_country??'');
                                                        $country_list = \App\model\Country::all();
                                                    ?>
                                                    <select name="ref_country" class="form-control-plaintext">
                                                    <?php
                                                        if($country_list->isNotEmpty()):
                                                            ?>
                                                            <option value="">Select</option>
                                                            <?php
                                                            foreach($country_list as $country):
                                                                ?>
                                                                <option value="{{$country->id}}" {{$old_country_id==$country->id?'selected':''}}>{{$country->name}}</option>
                                                                <?php
                                                            endforeach;
                                                        else:
                                                            ?>
                                                            <option value="">None</option>
                                                        <?php
                                                        endif;
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">State :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_state_id = old('ref_state',$profile->ref_state??'');

                                                    $state_list = null;
                                                    if($old_country_id != "" || $old_country_id != null):
                                                        $state_list = \App\model\State::where('ref_country', $old_country_id)->get();
                                                    endif;


                                                    ?>
                                                    <select name="ref_state" class="form-control-plaintext">
                                                        <?php
                                                        if($state_list && $state_list->isNotEmpty()):
                                                            ?>
                                                            <option value="">Select</option>
                                                            <?php
                                                            foreach($state_list as $state):
                                                                ?>
                                                                <option value="{{
                                                                $state->id}}" {{$old_state_id==$state->id?'selected':''}}>{{$state->name}}</option>
                                                            <?php
                                                            endforeach;
                                                        else:
                                                            ?>
                                                            <option value="">None</option>
                                                        <?php
                                                        endif;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-form-label">City/District :</label>
                                                <div class="col-sm-12" style="padding: 0;">
                                                    <?php
                                                    $old_city_id = old('ref_city',$profile->ref_city??'');
                                                    $city_list = null;
                                                    if($old_city_id != "" || $old_city_id != null):
                                                        $city_list = \App\model\City::where('ref_state', $old_state_id)->get();
                                                    endif;


                                                    ?>
                                                    <select name="ref_city" class="form-control-plaintext">
                                                        <?php
                                                        if($city_list && $city_list->isNotEmpty()):
                                                            ?>
                                                            <option value="">Select</option>
                                                            <?php
                                                            foreach($city_list as $city):
                                                                ?>
                                                                <option value="{{$city->id}}" {{$old_city_id==$city->id?'selected':''}}>{{$city->name}}</option>
                                                            <?php
                                                            endforeach;
                                                        else:
                                                            ?>
                                                            <option value="">None</option>
                                                        <?php
                                                        endif;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-indiv-info col-md-12" style="text-align: right">
                    <button type="submit" class="btn btn-info">Save & Update</button>
                </div>
            </div>
        </div>
    </div>

</section>
</form>

<script>
    let $dom_selectCountry = $('select[name=ref_country]');
    let $dom_selectState = $('select[name=ref_state]');
    let $dom_selectCity = $('select[name=ref_city]');

    function setState(states){
        $dom_selectState.empty();
        if(states.length > 0) {
            $dom_selectState.append('<option value="">Select</option>');
            $.each(states, function (i, d) {
                $dom_selectState.append('<option value="' + d.id + '">' + d.name + '</option>');
            });
        }else{
            setCity([]);
            $dom_selectState.append('<option value="">None</option>');
        }
    }

    function setCity(list){
        $dom_selectCity.empty();
        if(list.length > 0) {
            $dom_selectCity.append('<option value="">Select</option>');
            $.each(list, function (i, d) {
                $dom_selectCity.append('<option value="' + d.id + '">' + d.name + '</option>');
            });
        }else{
            $dom_selectCity.append('<option value="">None</option>');
        }
    }

    let $dom_icon = $('.photo_upload_icon');
    let $profile_photo_selector = $('input[name=profile_photo]');

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('img.preview_profile_photo').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }else{
            console.log("No input file...");
        }
    }


    $(document).ready(function(){
       if($dom_selectCountry.length > 0){
           $dom_selectCountry.on('change',function () {
               let country_val = $(this).val();
               $.ajax({
                   url: '{{route("resources.getStateByCountryID")}}',
                   data: {
                       id: country_val
                   },
                   type: 'GET',
                   success: function(res){
                       if(res.success){
                           let state_list = res.data;
                           setState(state_list);
                       }else{
                           setState([]);
                       }
                   },
                   error: function(data){
                        setState([]);
                        alert('Error!');
                   }
               });
           });
           $dom_selectState.on('change',function () {
               let val = $(this).val();
               $.ajax({
                   url: '{{route("resources.getCityByCountryID")}}',
                   data: {
                       id: val
                   },
                   type: 'GET',
                   success: function(res){
                       if(res.success){
                           let list = res.data;
                           setCity(list);
                       }else{
                           setCity([]);
                       }
                   },
                   error: function(data){
                       setCity([]);
                       alert('Error!');
                   }
               });
           });
       }else{
           alert("Select country not working properly, please reload again...");
       }


        $dom_icon.on('click',function () {
            $profile_photo_selector.trigger('click');
        });
        $profile_photo_selector.on('change',function () {
            console.log("changed => ", this.files[0]);
            readURL(this);
        });
    });
</script>
@stop
