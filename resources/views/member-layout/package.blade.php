@extends('layouts.member')

@section('body-content')
<div class="container">
    <div class="jumbotron" style="text-align: center;">
        <h1 class="display-4" style="color: black;">My Current Package</h1>
        <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
        <hr class="my-4">
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <a class="btn btn-primary btn-lg" href="#" role="button">Upgrade Current Package</a>
        <a class="btn btn-primary btn-lg" href="{{ route('page.premium-plans') }}" role="button">Get All Package List</a>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Current Package</th>
            <th scope="col">Issued From</th>
            <th scope="col">Expiry Date</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
        </tr>
        </tbody>
    </table>
</div>
@stop
