@extends('layouts.member')

@section('body-content')
<style>
    .form-group{
        margin: 0;
        display: flex;
        position: inherit !important;
    }
    .form-group label{
        margin: 0 !important;

        font-family: 'Montserrat', sans-serif;
        font-style: normal;
        font-size: 13px !important;
        line-height: 24px !important;
        color: #3d3d3d !important;
        font-weight: 600;
        padding: 0 !important;
        display: flex;
        /* justify-content: center; */
        align-items: center;
    }
    .form-group .form-control, .form-group .form-control-plaintext, .form-group input, .form-group select{
        line-height: 24px;
        font-size: 13px;
        font-family: 'Montserrat', sans-serif;
        border: black solid thin;
        padding: 5px;
        background-color: #e6f5ee;
        font-weight: 600;
    }
    .profile-listing ul{
        display: flex;
        flex-wrap: wrap;
    }
    .profile-listing ul li{
        padding: 0 10px;
    }
    .photo_upload_icon{
        position: absolute;
        top: 0px;
        bottom: 0px;
        height: 91%;
        width: 76%;
        display: flex;
        justify-content: center;
        align-items: center;
        opacity: 0;
        transition: all 0.7s;
        cursor: pointer;
     }
    .photo_upload_icon:hover{
        opacity: 0.7;
    }
</style>
<form action="{{ route('cpanel.member.submit-docs') }}" method="post" enctype="multipart/form-data">
<section class="page-section-ptb dashboard">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    @csrf()
                <div class="profile-indiv-info">
                    <h4 class="divider-3">Upload (Proof of Address - POA)</h4>
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="profile-listing">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <select class="form-control" name="type_of_poa">
                                                <option value="">-- Select Type of POA --</option>
                                                <?php
                                                    $type_of_poa = old('type_of_poa');
                                                    $poa_types = [
                                                        'adhar' => 'Aadhaar Identity Card',
                                                        'passport' => 'Passport',
                                                        'driving_licence' => 'Driving License (Issued from Maharashtra state not valid )',
                                                        'voter_card' => 'Election Commission’s Voter ID Card',
                                                        'vechicle_reg_smart_card' => 'Vehicle Registration Certificate / Smart card',
                                                    ];

                                                    foreach($poa_types as $key => $val):
                                                ?>
                                                        <option value="{{$key}}" {{($type_of_poa == $key)?"selected":''}}>{{$val}}</option>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6" style="text-align: right;">
                                            <input class="form-control" type="file" name="poa_file"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-indiv-info">
                    <h4 class="divider-3">Upload (Proof of Identification - POI)</h4>
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="profile-listing">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <select class="form-control" name="type_of_poi">
                                                <option value="">-- Select Type of POA --</option>
                                                <?php
                                                $type_of_poi = old('type_of_poi');
                                                $poa_types = [
                                                    'adhar' => 'Aadhaar Identity Card',
                                                    'passport' => 'Passport',
                                                    'driving_licence' => 'Driving License',
                                                    'pan_card' => 'Income Tax PAN Card',
                                                    'credit_debit_photo_card' => 'Photo Credit Card/ Debit card (with Photo)',
                                                ];

                                                foreach($poa_types as $key => $val):
                                                    ?>
                                                    <option value="{{$key}}" {{($type_of_poi == $key)?"selected":''}}>{{$val}}</option>
                                                <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6" style="text-align: right;">
                                            <input class="form-control" type="file" name="poi_file"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-indiv-info col-md-12" style="text-align: right">
                    <button type="submit" class="btn btn-info">Save & Update</button>
                </div>
            </div>
        </div>
    </div>

</section>
</form>
@stop
