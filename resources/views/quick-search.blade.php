@extends('layouts.member')

@section('body-content')
    <style>
        .profile-listing ul li {
            display: flex !important;
            flex-wrap: wrap;
        }

        .profile-listing ul li span {
            width: 50% !important;
        }

        .omg-filter-list{
            /* background-color: #de9c8724; */
            border: #00000021 solid thin;
            padding: 10px;
        }
        .omg-filter-list h5{
            color: white;
            background-color: black;
            padding: 5px;
            text-align: center;
            margin: 0px;
        }
        .omg-filter-list .form-control{
            padding: 5px !important;
            border: #b9b9b9 solid thin !important;
            border-radius: 7px !important;
            color: black !important;
        }
        .omg-filter-list .form-group label{
            color: #8586b5;
            font-weight: 600;
            font-size: 16px;
            line-height: 40px;
        }
    </style>
    <div class="bottom-header">
        <ul class="menu-bottom-links">
            <li><a href="#"> My Matches (0) </a></li>
            <li><a href="#"> Recently Viewed (0) </a></li>
        </ul>
    </div>

    <section class="page-section-ptb dashboard">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="profile-indiv-info">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile-content">
                                    <form action="{{route('search.quick-search')}}" method="get">
                                        <?php
                                        $pre_gender = false;
                                        $pre_from_age = false;
                                        $pre_to_age = false;
                                        $pre_looking_for = false;
                                        $pre_ref_religion = false;
                                        $pre_ref_cast = false;
                                        $pre_ref_language = false;

                                        $pre_ref_country = false;
                                        $pre_ref_country_grew_up_in = false;
                                        $pre_marital_status = false;

                                        if(isset($filter_data)):
                                            $pre_gender = $filter_data['gender']??false;
                                            $pre_from_age = $filter_data['from_age']??false;
                                            $pre_to_age = $filter_data['to_age']??false;
                                            $pre_ref_language = $filter_data['ref_language']??false;
                                            $pre_ref_cast = $filter_data['ref_cast']??false;
                                            $pre_ref_religion = $filter_data['ref_religion']??false;


                                            $pre_ref_country = ($filter_data['ref_country'])?$filter_data['ref_country']:false;
                                            $pre_ref_country_grew_up_in = $filter_data['ref_country_grew_up_in']?$filter_data['ref_country_grew_up_in']:false;
                                            $pre_marital_status = $filter_data['marital_status']?$filter_data['marital_status']:false;
                                        endif;
                                        ?>
                                        <div class="filter-menu">
                                            <div class="omg-filter-list">
                                                <!--<button type="reset" class="btn btn-info">Reset Search
                                                </button>
                                                <button type="submit" class="btn btn-info">Search
                                                    <i class="fa fa-angle-double-right"></i>
                                                </button>-->
                                                <h5>Looking For</h5>
                                                <div class="form-group">
                                                    <label>Select Gender</label>
                                                    <?php
                                                    $looking_for_def = old('looking_for', $pre_gender??'any');
                                                    ?>s
                                                    <select name="gender" class="form-control">
                                                        <option value="any" {{ ($looking_for_def == 'any')?'selected':'' }}> Any </option>
                                                        <option value="male" {{ ($looking_for_def == 'male')?'selected':'' }}> Male </option>
                                                        <option value="female" {{ ($looking_for_def == 'female')?'selected':'' }}> Female </option>
                                                        {{--                                                        <option value="other" {{ ($looking_for_def == 'other')?'selected':'' }}> Trans </option>--}}
                                                    </select>
                                                </div>


                                                <h5>Age Preference</h5>
                                                <div class="form-group">
                                                    <label>From Age</label>
                                                    <?php $from_age_def = old('from_age', ($pre_from_age) ? $pre_from_age : 18); ?>
                                                    <input type="text" class="form-control" name="from_age" value="{{$from_age_def}}"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>To Age</label>
                                                    <?php $to_age_def = old('to_age', ($pre_to_age) ? $pre_to_age : 32); ?>
                                                    <input type="text" class="form-control" name="to_age" value="{{$to_age_def}}"/>
                                                </div>


                                                <h5>Religion & Caste/community</h5>
                                                <div class="form-group">
                                                    <label>Religion</label>
                                                    <?php
                                                    $ref_religion_def = old('search_ref_religion', ($pre_ref_religion) ? $pre_ref_religion : 'any');
                                                    ?>
                                                    <select name="ref_religion" class="form-control">
                                                        <option value="any" {{($ref_religion_def == "any")?' selected':''}}> Any </option>
                                                        <?php
                                                        $old_religion = $ref_religion_def;
                                                        $religions = \App\model\Religion::all()->sortBy('name');
                                                        foreach($religions as $religion):
                                                        ?>
                                                        <option value="{{ $religion->id }}"{{(isset($old_religion) && $old_religion == $religion->id)?' selected':''}}>{{ $religion->name }}</option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Caste/community</label>
                                                    <?php
                                                    $ref_cast_def = old('search_ref_cast', ($pre_ref_cast) ? $pre_ref_cast : 'any');
                                                    ?>
                                                    <select name="ref_cast" class="form-control">
                                                        <option value="any" {{($ref_cast_def == "any")?' selected':''}}>Any</option>
                                                        <?php
                                                        $old_cast = $ref_cast_def;
                                                        $casts = [];
                                                        if (isset($old_religion)):
                                                            $casts = \App\model\Cast::where('ref_religion', $old_religion)->get()->sortBy('name');
                                                        endif;

                                                        if(empty($casts)): ?>
                                                        <option value="">None</option>
                                                        <?php else: ?>
                                                        <option value="">Select</option>
                                                        <?php foreach($casts as $cast): ?>
                                                        <option
                                                            value="{{ $cast->id }}"{{(isset($old_cast) && $old_cast == $cast->id)?' selected':''}}>{{ $cast->name }}</option>
                                                        <?php endforeach; endif; ?>
                                                    </select>
                                                </div>

                                                <h5>Mother Tongue</h5>
                                                <div class="form-group">
                                                    <label>Language</label>
                                                    <?php
                                                    $ref_language_def = old('ref_language', ($pre_ref_language) ? $pre_ref_language : 'any');
                                                    ?>
                                                    <select name="ref_language" class="form-control">
                                                        <option value="any" {{($ref_language_def == "any")?' selected':''}}> Any </option>
                                                        <?php
                                                        $old_language = $ref_language_def;
                                                        $languages = \App\model\Language::all()->sortBy('name');
                                                        foreach($languages as $language):
                                                        ?>
                                                        <option
                                                            value="{{ $language->id }}"{{(isset($old_language) && $pre_ref_language == $language->id)?' selected':''}}>{{ $language->name }}</option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <h5>Live In</h5>
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <?php
                                                    $ref_country_def = old('ref_country', ($pre_ref_country) ? $pre_ref_country : 'any');
                                                    ?>
                                                    <select name="ref_country" class="form-control">
                                                        <option value="any" {{($ref_country_def == "any")?' selected':''}}> Any </option>
                                                        <?php
                                                        $old_country = $ref_country_def;
                                                        $countries = \App\model\Country::all()->sortBy('name');
                                                        foreach($countries as $country):
                                                        ?>
                                                        <option
                                                            value="{{ $country->id }}"{{(isset($old_country) && $pre_ref_country == $country->id)?' selected':''}}>{{ $country->name }}</option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Grew Up</label>
                                                    <?php
                                                    $ref_country_grew_up_in_def = old('ref_country_grew_up_in', ($pre_ref_country_grew_up_in) ? $pre_ref_country_grew_up_in : 'any');
                                                    ?>
                                                    <select name="ref_country_grew_up_in" class="form-control">
                                                        <option value="any" {{($ref_country_grew_up_in_def == "any")?' selected':''}}> Any </option>
                                                        <?php
                                                        $old_country = $ref_country_grew_up_in_def;
                                                        $countries = \App\model\Country::all()->sortBy('name');
                                                        foreach($countries as $country):
                                                        ?>
                                                        <option
                                                            value="{{ $country->id }}"{{(isset($old_country) && $pre_ref_country_grew_up_in == $country->id)?' selected':''}}>{{ $country->name }}</option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                                <h5>Marital Status</h5>
                                                <div class="form-group">
                                                    <label>Types</label>
                                                    <?php
                                                    $marital_status_def = old('marital_status', ($pre_marital_status) ? $pre_marital_status : 'any');
                                                    ?>
                                                    <select name="marital_status" class="form-control">
                                                        <option value="any" {{($marital_status_def == "any")?' selected':''}}> Any </option>
                                                        <?php $old_marital_status = $marital_status_def; ?>
                                                        <option value="unmarried" {{(isset($old_marital_status) && $pre_marital_status == 'unmarried')?' selected':''}}>Un-Married</option>
                                                        <option value="widower" {{(isset($old_marital_status) && $pre_marital_status == 'widower')?' selected':''}}>Widower</option>
                                                        <option value="divorced" {{(isset($old_marital_status) && $pre_marital_status == 'divorced')?' selected':''}}>Divorced</option>
                                                    </select>
                                                </div>

                                                <button type="reset" class="btn btn-info">Reset Filter</button>
                                                <button type="submit" class="btn btn-info">
                                                    Search <i class="fa fa-angle-double-right"></i>
                                                </button>
                                            </div>
                                        </div>



                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="profile-content">
                                    <h4 class="divider-3">Match List ({{ ($profiles)?$profiles->count():0 }}) - {{$use_preference?"Preference Filter":"Custom Filter"}}</h4>
                                    @if($profiles->count() > 0)
                                        @foreach($profiles as $profile)
                                            <div class="list-single">
                                                <div class="row">
                                                    <div class="col-md-3 text-center">
                                                        <?php

                                                            //Profile Photo...
                                                            $gender = "others";
                                                            if (isset($profile)):
                                                                $gender = $profile->gender;
                                                            endif;
                                                            $photo_url = ExtraController::getProfilePhotoURL($profile);
                                                            if (!isset($photo_url)):
                                                                switch ($gender):
                                                                    case 'male':
                                                                        $photo_url = asset('public/images/male-icon.png');
                                                                        break;
                                                                    case 'female':
                                                                        $photo_url = asset('public/images/female-icon.png');
                                                                        break;
                                                                    default:
                                                                        $photo_url = asset('public/images/trans-icon.png');
                                                                        break;
                                                                endswitch;
                                                            endif;


                                                            //Age Calculate
                                                            $age = "Not Specified";
                                                            $dob = $profile->dob;
                                                            if ($dob):
                                                                $age = Carbon\Carbon::parse($dob)->age . ' yrs';
                                                            endif;

                                                            //Height Calculate
                                                            $height_str = "Not Specified";
                                                            $height_cm = $profile->height_cm;
                                                            if ($height_cm):
                                                                $height_inches = $height_cm / 2.54;
                                                                $height_feet = intval($height_inches / 12);
                                                                $height_inches = $height_inches % 12;
                                                                $height_str = $height_feet . '\'' . $height_inches . '"';
                                                            endif;

                                                            //Maritial Status...
                                                            $str_marital_status = "--";
                                                            $marital_status = $profile->marital_status;
                                                            if ($marital_status):
                                                                switch ($marital_status):
                                                                    case 'unmarried':
                                                                        $str_marital_status = "Un-married";
                                                                        break;
                                                                    case 'married':
                                                                        $str_marital_status = "Married";
                                                                        break;
                                                                    case 'divorced':
                                                                        $str_marital_status = "Divorced";
                                                                        break;
                                                                    case 'widower':
                                                                        $str_marital_status = "Widower";
                                                                        break;
                                                                    default:
                                                                        $str_marital_status = "Not Specified";
                                                                endswitch;
                                                            endif;

                                                            //Profile ID
                                                            $profile_id = $profile->profile_id;
                                                            $profile_id = trim($profile_id);
                                                            $profile_id = ($profile_id == "")?'Not Specified':$profile_id;

                                                            //To view photos url...
                                                            $gallery_url = route('member.view-profile', $profile->profile_id).'#gallery';
                                                            $is_logged = \Illuminate\Support\Facades\Auth::guest();
                                                            if($is_logged):
                                                                $gallery_url = route('register');
                                                            endif;
                                                        ?>
                                                        <img src="{{$photo_url}}" class="list-profile">
                                                        <p style="margin-top: 5px;">To view his photos <br> <a href="{{$gallery_url}}"><?=(!$is_logged)?'View Photos':'Register Now'; ?></a></p>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="id-list">
                                                                    <p>Matrimony ID: <span>{{ $profile_id }}</span></p>
                                                                    <p>Online 2 days ago</p>
                                                                </div>
                                                                <div class="profile-listing">
                                                                    <ul>
                                                                        <li><span>Age/Height</span>{{$age}}/ {{$height_str}}</li>
                                                                        <li><span>Marital Status</span> {{$str_marital_status}}</li>
                                                                        <li>
                                                                            <span>Religion / Community</span>
                                                                            {{ App\model\Religion::where('id', $profile->ref_religion)->first()->name }} / {{ App\model\Cast::where('id', $profile->ref_cast)->first()->name }}
                                                                        </li>
                                                                        <li><span>Location</span>{{ App\model\Country::where('id', $profile->ref_country)->first()->name }}</li>
                                                                        <li><span>Mother Tongue</span>{{ App\model\Language::where('id', $profile->ref_language)->first()->name }}</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <ul class="des-list">
                                                                    <li><a href="#" class="btn-interest">I'm interested</a></li>
                                                                    <li><a href="#"><i class="fa fa-star" style="color: #888;"></i>Shortlist</a></li>
                                                                    <li><a href="#"><i class="fa fa-minus-circle" style="color: #fb0303;"></i>Ignore</a></li>
                                                                    <li><a href="{{route('member.view-profile', $profile->profile_id)}}"><i class="fa fa-user" style="color: #3e8bff;"></i> View full Profile</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="decription">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dolorum esse id ipsam ipsum libero minima, minus numquam perspiciatis provident quasi quo...
                                                                <a href="#">Read more</a> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                    <div class="alert alert-danger" role="alert">
                                        No profile found! {!! $use_preference?"You need to tune your preference <a class=\"btn btn-info btn-sm\" href=\"".route('cpanel.member.preference-setup')."\">Preference Setup</a>":"Please alter your filter options for best possible results." !!}
                                    </div>
                                    @endif
                                </div>



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        </div>
        </div>
        </div>
    </section>
    </section>

@stop
