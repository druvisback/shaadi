@extends('layouts.member')

@section('body-content')
    <style>
        .profile-listing ul li {
            display: flex !important;
            flex-wrap: wrap;
        }

        .profile-listing ul li span {
            width: 50% !important;
        }
    </style>
    <section class="page-section-ptb dashboard">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="profile-indiv-info">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="profile-content">
                                    <form action="{{route('search.matches')}}" method="get">
                                        <?php
                                        $pre_gender = (isset($predefined_filter) && array_key_exists('gender', $predefined_filter)) ? $predefined_filter['gender'] : false;
                                        $pre_from_age = (isset($predefined_filter) && array_key_exists('from_age', $predefined_filter)) ? $predefined_filter['from_age'] : false;
                                        $pre_to_age = (isset($predefined_filter) && array_key_exists('to_age', $predefined_filter)) ? $predefined_filter['to_age'] : false;
                                        $pre_from_height_cm = (isset($predefined_filter) && array_key_exists('from_height_cm', $predefined_filter)) ? $predefined_filter['from_height_cm'] : false;
                                        $pre_to_height_cm = (isset($predefined_filter) && array_key_exists('to_height_cm', $predefined_filter)) ? $predefined_filter['to_height_cm '] : false;
                                        $pre_looking_for = (isset($predefined_filter) && array_key_exists('looking_for', $predefined_filter)) ? $predefined_filter['looking_for'] : false;
                                        $pre_marital_status = (isset($predefined_filter) && array_key_exists('marital_status', $predefined_filter)) ? $predefined_filter['marital_status'] : false;
                                        $pre_ref_religion = (isset($predefined_filter) && array_key_exists('ref_religion', $predefined_filter)) ? $predefined_filter['ref_religion'] : false;
                                        $pre_ref_cast = (isset($predefined_filter) && array_key_exists('ref_cast', $predefined_filter)) ? $predefined_filter['ref_cast'] : false;
                                        $pre_ref_language = (isset($predefined_filter) && array_key_exists('ref_language', $predefined_filter)) ? $predefined_filter['ref_language'] : false;
                                        $pre_ref_country = (isset($predefined_filter) && array_key_exists('ref_country', $predefined_filter)) ? $predefined_filter['ref_country'] : false;
                                        $pre_ref_state = (isset($predefined_filter) && array_key_exists('ref_state', $predefined_filter)) ? $predefined_filter['ref_state'] : false;
                                        $pre_ref_city = (isset($predefined_filter) && array_key_exists('ref_city', $predefined_filter)) ? $predefined_filter['ref_city'] : false;
                                        ?>
                                        <button type="reset" id="CategView">Refine Search<i
                                                class="fa fa-angle-double-right"></i></button>
                                        <div class="filter-menu">
                                            <h5>Age Preference</h5>
                                            <div>
                                                <label>From Age</label>
                                                <?php
                                                $from_age_def = old('from_age', ($pre_from_age) ? $pre_from_age : 18);
                                                ?>
                                                <input type="number" min="18" max="100" value="{{$from_age_def}}"
                                                       class="slider" name="from_age"/>
                                                <br>
                                                <label>To Age</label>
                                                <?php
                                                $to_age_def = old('to_age', ($pre_to_age) ? $pre_to_age : 32);
                                                ?>
                                                <input type="number" min="18" max="100" value="{{$to_age_def}}"
                                                       class="slider" name="to_age"/>
                                            </div>

                                            <h5>Height Preference</h5>
                                            <div>
                                                <label>From Height (cm)</label>
                                                <?php
                                                $from_height_cm_def = old('from_height_cm', ($pre_from_height_cm) ? $pre_from_height_cm : 181);
                                                ?>
                                                <input type="number" min="100" value="" class="slider"
                                                       name="from_height_cm"/>
                                                <br>
                                                <label>To Height (cm)</label>
                                                <?php
                                                $to_height_cm_def = old('to_height_cm', ($pre_to_height_cm) ? $pre_to_height_cm : 192);
                                                ?>
                                                <input type="number" min="100" value="" class="slider"
                                                       name="to_height_cm"/>
                                            </div>

                                            <h6>Looking For</h6>
                                            <div class="col-md-8">
                                                <?php
                                                $looking_for_def = old('looking_for', ($pre_looking_for) ? $pre_looking_for : 'male');
                                                ?>
                                                <select name="looking_for" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="male" {{ ($looking_for_def == 'male')?'selected':'' }}>
                                                        Male
                                                    </option>
                                                    <option
                                                        value="female" {{ ($looking_for_def == 'female')?'selected':'' }}>
                                                        Female
                                                    </option>
                                                    <option value="other" {{ ($looking_for_def == 'other')?'selected':'' }}>
                                                        Trans
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <h6>Marital Status</h6>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                $marital_status_def = old('search_marital_status', ($pre_marital_status) ? $pre_marital_status : 'any');
                                                ?>
                                                <select name="marital_status" class="form-control">
                                                    <option value="">Select</option>
                                                    <option
                                                        value="unmarried" {{ ($marital_status_def == 'unmarried')?'selected':'' }}>
                                                        Never Married
                                                    </option>
                                                    <option
                                                        value="divorced" {{ ($marital_status_def == 'divorced')?'selected':'' }}>
                                                        Divorced
                                                    </option>
                                                    <option
                                                        value="widow" {{ ($marital_status_def == 'widow')?'selected':'' }}>
                                                        Widow
                                                    </option>
                                                    <option value="any" {{ ($marital_status_def == 'any')?'selected':'' }}>
                                                        Any
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <h6>Religion</h6>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                $ref_religion_def = old('search_ref_religion', ($pre_ref_religion) ? $pre_ref_religion : 'any');
                                                ?>
                                                <select name="ref_religion" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="any" {{($ref_religion_def == "any")?' selected':''}}>
                                                        Any
                                                    </option>
                                                    <?php
                                                    $old_religion = $ref_religion_def;
                                                    $religions = \App\model\Religion::all();
                                                    foreach($religions as $religion):
                                                    ?>
                                                    <option
                                                        value="{{ $religion->id }}"{{(isset($old_religion) && $old_religion == $religion->id)?' selected':''}}>{{ $religion->name }}</option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>

                                            <div class="col-md-8">
                                                <?php
                                                $ref_cast_def = old('search_ref_cast', ($pre_ref_cast) ? $pre_ref_cast : 'any');
                                                ?>
                                                <select name="ref_cast" class="form-control">
                                                    <option value="any" {{($ref_cast_def == "any")?' selected':''}}>Any
                                                    </option>
                                                    <?php
                                                    $old_cast = $ref_cast_def;
                                                    $casts = [];
                                                    if (isset($old_religion)):
                                                        $casts = \App\model\Cast::where('ref_religion', $old_religion)->get();
                                                    endif;

                                                    if(empty($casts)): ?>
                                                    <option value="">None</option>
                                                    <?php else: ?>
                                                    <option value="">Select</option>
                                                    <?php foreach($casts as $cast): ?>
                                                    <option
                                                        value="{{ $cast->id }}"{{(isset($old_cast) && $old_cast == $cast->id)?' selected':''}}>{{ $cast->name }}</option>
                                                    <?php endforeach; endif; ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <h6>Mother Tongue</h6>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                $ref_language_def = old('search_from_age', ($pre_ref_language) ? $pre_ref_language : 'any');
                                                ?>
                                                <select name="ref_language" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="any" {{($ref_language_def == "any")?' selected':''}}>
                                                        Any
                                                    </option>
                                                    <?php
                                                    $old_language = $ref_language_def;
                                                    $languages = \App\model\Language::all();
                                                    foreach($languages as $language):
                                                    ?>
                                                    <option
                                                        value="{{ $language->id }}"{{(isset($old_language) && $old_language == $language->id)?' selected':''}}>{{ $language->name }}</option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <h6>Country Living in</h6>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                $ref_country_def = old('search_ref_country', ($pre_ref_country) ? $pre_ref_country : 'any');
                                                ?>
                                                <select name="ref_country" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="any" {{($ref_country_def == "any")?' selected':''}}>Any
                                                    </option>
                                                    <?php
                                                    $old_country = $ref_country_def;
                                                    $countries = \App\model\Country::all();
                                                    foreach($countries as $country):
                                                    ?>
                                                    <option
                                                        value="{{ $country->id }}"{{(isset($old_country) && $old_country == $country->id)?' selected':''}}>{{ $country->name }}</option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <h6>State Living in</h6>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                $ref_state_def = old('search_ref_state', ($pre_ref_state) ? $pre_ref_state : 'selected');
                                                ?>
                                                <select name="ref_state" class="form-control">
                                                    <option value="any" {{($ref_state_def == "any")?' selected':''}}>Any
                                                    </option>
                                                    <?php
                                                    $old_state = $ref_state_def;
                                                    $casts = [];
                                                    if (isset($old_country)):
                                                        $states = \App\model\State::where('ref_country', $old_country)->get();
                                                    endif;

                                                    if(empty($states)): ?>
                                                    <option value="">None</option>
                                                    <?php else: ?>
                                                    <option value="">Select</option>
                                                    <?php foreach($states as $state): ?>
                                                    <option
                                                        value="{{ $state->id }}"{{(isset($old_state) && $old_state == $state->id)?' selected':''}}>{{ $state->name }}</option>
                                                    <?php endforeach; endif; ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <h6>City/District</h6>
                                            </div>
                                            <div class="col-md-8">
                                                <?php
                                                $ref_city_def = old('search_ref_city', ($pre_ref_city) ? $pre_ref_city : 'selected');
                                                ?>
                                                <select name='ref_city' class="form-control">
                                                    <option value="any" {{($ref_city_def == "any")?' selected':''}}>Any
                                                    </option>
                                                    <option value="">Select</option>
                                                    <?php
                                                    $old_city = $ref_city_def;
                                                    $cities = [];
                                                    if (isset($old_state)):
                                                        $cities = \App\model\City::where('ref_state', $old_state)->get();
                                                    endif;

                                                    if(empty($cities)): ?>
                                                    <option value="">None</option>
                                                    <?php else: ?>
                                                    <option value="">Select</option>
                                                    <?php foreach($cities as $city): ?>
                                                    <option
                                                        value="{{ $city->id }}"{{(isset($old_city) && $old_city == $city->id)?' selected':''}}>{{ $city->name }}</option>
                                                    <?php endforeach; endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                </div>
                            </div>
                                            <div class="col-md-8">
                                            <div class="profile-content">
                                                <h4 class="divider-3">Best Matches
                                                    ({{ ($match_profiles)?$match_profiles->count():0 }})</h4>

                                                <div class="matches-block">
                                                    @if($match_profiles)
                                                        @foreach($match_profiles as $match_profile)
                                                            <div class="row single-match">
                                                                <div class="col-md-3">
                                                                    <?php
                                                                    $gender = "others";
                                                                    if (isset($match_profile)):
                                                                        $gender = $match_profile->gender;
                                                                    endif;
                                                                    $photo_url = ExtraController::getProfilePhotoURL($match_profile);
                                                                    if (!isset($photo_url)):
                                                                        switch ($gender):
                                                                            case 'male':
                                                                                $photo_url = {{ ExtraController::assetURL('images/male-icon.png');
                                                                                break;
                                                                            case 'female':
                                                                                $photo_url = {{ ExtraController::assetURL('images/female-icon.png');
                                                                                break;
                                                                            default:
                                                                                $photo_url = {{ ExtraController::assetURL('images/trans-icon.png');
                                                                                break;
                                                                        endswitch;
                                                                    endif;

                                                                    ?>
                                                                    <img class="img-fluid pro-img"
                                                                         src="{{ $photo_url }}" alt=""/>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <h3>{{ $match_profile->first_name.' '.$match_profile->last_name }}</h3>
                                                                    <div class="profile-listing">
                                                                        <ul>
                                                                            <?php
                                                                            //Age Calculate
                                                                            $age = "Not Specified";
                                                                            $dob = $match_profile->dob;
                                                                            if ($dob):
                                                                                $age = Carbon\Carbon::parse($dob)->age . ' yrs';
                                                                            endif;

                                                                            //Height Calculate
                                                                            $height_str = "Not Specified";
                                                                            $height_cm = $match_profile->height_cm;
                                                                            if ($height_cm):
                                                                                $height_inches = $height_cm / 2.54;
                                                                                $height_feet = intval($height_inches / 12);
                                                                                $height_inches = $height_inches % 12;
                                                                                $height_str = $height_feet . '\'' . $height_inches . '"';
                                                                            endif;
                                                                            ?>
                                                                            <li><span>Age/Height</span>{{ $age }}
                                                                                / {{ $height_str }}</li>
                                                                            <?php
                                                                            $str_marital_status = "--";
                                                                            $marital_status = $match_profile->marital_status;
                                                                            if ($marital_status):
                                                                                switch ($marital_status):
                                                                                    case 'unmarried':
                                                                                        $str_marital_status = "Un-married";
                                                                                        break;
                                                                                    case 'married':
                                                                                        $str_marital_status = "Married";
                                                                                        break;
                                                                                    case 'divorced':
                                                                                        $str_marital_status = "Divorced";
                                                                                        break;
                                                                                    case 'widow':
                                                                                        $str_marital_status = "Widow";
                                                                                        break;
                                                                                    default:
                                                                                        $str_marital_status = "Not Specified";
                                                                                endswitch;
                                                                            endif;
                                                                            ?>
                                                                            <li>
                                                                                <span>Marital Status</span> {{ $str_marital_status }}
                                                                            </li>
                                                                            <li>
                                                                                <span>Religion / Cast</span>{{ App\model\Religion::where('id', $match_profile->ref_religion)->first()->name }}
                                                                                / {{ App\model\Cast::where('id', $match_profile->ref_cast)->first()->name }}
                                                                            </li>
                                                                            <li>
                                                                                <span>Location</span>{{ App\model\Country::where('id', $match_profile->ref_country)->first()->name }}
                                                                            </li>
                                                                            <li>
                                                                                <span>Mother Tongue</span>{{ App\model\Language::where('id', $match_profile->ref_language)->first()->name }}
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 btn-connected">
                                                                    <li class="{{ Request::is('/profile/{profile}') ? 'active' : '' }}">
                                                                        <a href="{{route('cpanel.member.view-profile', $match_profile->id)}}">
                                                                            View Profile<i
                                                                                class="glyph-icon flaticon-hearts"
                                                                                aria-hidden="true"></i>
                                                                        </a>
                                                                    </li>
                                                                </div>

                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="alert alert-danger" role="alert">
                                                            No profile found!
                                                        </div>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
    </section>
@stop
