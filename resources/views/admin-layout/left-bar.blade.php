<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ \App\Http\Controllers\ExtraController::getProfilePhotoURL($profile) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ ($profile)?$profile->first_name." ".$profile->last_name: 'Unknown' }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>


            <li><a href="{{route('cpanel.admin.dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
{{--            <li><a href="{{route('cpanel.admin.user-list')}}"><i class="fa fa-users"></i> <span>Users</span></a></li>--}}

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Members</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{route('cpanel.admin.user-list')}}"><i class="fa fa-circle-o"></i>All Members</a></li>
                    <li><a href="{{route('cpanel.admin.new-user-list')}}"><i class="fa fa-circle-o"></i>New Members</a></li>
                    <li><a href="{{route('cpanel.admin.kyc-user-list')}}"><i class="fa fa-circle-o"></i>Kyc Of Members</a></li>
                </ul>
            </li>

            <li><a href="{{route('cpanel.admin.testimonials')}}"><i class="fa fa-user-circle"></i> <span>Testimonials</span></a></li>
            <li><a href="{{route('cpanel.admin.view-faq')}}"><i class="fa fa-user-circle"></i> <span>Faq</span></a></li>
            <li><a href="{{route('cpanel.admin.package')}}"><i class="fa fa-calendar"></i> <span>Packages</span></a></li>
            <li><a href="{{route('cpanel.admin.contact-list')}}"><i class="fa fa-phone"></i> <span>Contacts</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-phone"></i>
                    <span>Religions & Casts</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{route('cpanel.admin.show-religions')}}"><i class="fa fa-circle-o"></i>All Religions</a></li>
                    <li><a href="{{route('cpanel.admin.add-religion')}}"><i class="fa fa-circle-o"></i>New Religion</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-phone"></i>
                    <span>Country & State</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{route('cpanel.admin.show-country')}}"><i class="fa fa-circle-o"></i>All Country</a></li>
                    <li><a href="{{route('cpanel.admin.add-country')}}"><i class="fa fa-circle-o"></i>New Country</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Advertisement</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{route('cpanel.admin.view-add')}}"><i class="fa fa-circle-o"></i>All Advertisement</a></li>
                    <li><a href="{{route('cpanel.admin.add-add')}}"><i class="fa fa-circle-o"></i>New Advertisement</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Terms & condition</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{route('cpanel.admin.view-terms')}}"><i class="fa fa-circle-o"></i>All Terms</a></li>
                    <li><a href="{{route('cpanel.admin.add-add')}}"><i class="fa fa-circle-o"></i>New Terms</a></li>
                </ul>
            </li>



          <li class="treeview">
              <a href="#">
                  <i class="fa fa-users"></i>
                  <span>Success Story</span>
                  <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                  <li><a href="{{route('cpanel.admin.view-story')}}"><i class="fa fa-circle-o"></i>All Story</a></li>
                  <li><a href="{{route('cpanel.admin.add-story')}}"><i class="fa fa-circle-o"></i>New Story</a></li>
              </ul>
          </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Privacy & Policy</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{route('cpanel.admin.view-policy')}}"><i class="fa fa-circle-o"></i>All Pricay</a></li>
                    <li><a href="{{route('cpanel.admin.add-policy')}}"><i class="fa fa-circle-o"></i>New Privacy</a></li>
                </ul>
            </li>




            <li><a href="{{route('cpanel.admin.profile')}}"><i class="fa fa-user-circle"></i> <span>My Profile</span></a></li>
            <li><a href="{{route('cpanel.admin.change-password')}}"><i class="fa fa-key"></i> <span>Change Password</span></a></li>
            <li><a href="{{route('logout')}}"><i class="fa fa-lock"></i> <span>Logout</span></a></li>
        </ul>
    </section>
</aside>
