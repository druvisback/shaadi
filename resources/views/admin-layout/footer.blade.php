<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        We <b>Care</b> You
    </div>
    <strong>Copyright &copy; {{date('Y')}} <a target="_blank" href="https://cybetiq.com">Cybetiq Pvt. Ltd., Noida</a>.</strong> All rights
    reserved.
</footer>
