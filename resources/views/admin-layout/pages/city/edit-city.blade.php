@extends('admin-layout.app')

@section('body-content')
    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.edit-city',['state_id' => $state->id, 'city_id' => $city->id])}}" method="post" >
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">Edit City Name '{{$city->name}}' for State '{{$state->name}}'</label>
                <input name="name" type="text" class="form-control" placeholder="Enter city name" value="{{old('name',$city->name)}}"/>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
@endsection
