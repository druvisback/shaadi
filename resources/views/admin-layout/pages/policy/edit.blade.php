@extends('admin-layout.app')

@section('body-content')

    <div class="row" style="padding: 15px;">
    <form class="form-horizontal" action="{{route('cpanel.admin.update-policy')}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$tById->id}}"/>
        <div class="form-group">
            <label class="col-md-3 control-label">Please Type Your FAQ Title</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="heading" value="{{$tById->heading}}" placeholder="please type heading" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Please Type Your Question</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="contain" value="{{$tById->contain}}" placeholder="please type contain" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12" style="text-align: center">
                <button type="submit" class="btn btn-sm btn-success">Submit</button>
            </div>
        </div>

    </form>
</div>
</div>
<!-- end panel -->
</div>

@endsection
