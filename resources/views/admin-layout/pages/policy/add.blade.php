@extends('admin-layout.app')


@section('body-content')

    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.save-policy')}}" method="post" enctype="multipart/form-data">
            @csrf
            @if(isset($faq_id))
                <?php $faq =  \App\model\Policy::find($t_id); ?>
                <input type="hidden" name="id" value="{{$t_id}}"/>
                <div class="form-group">
                    <label for="exampleInputPassword1">Heading</label>
                    <input name="heading" type="text" class="form-control" placeholder="heading" value="{{old('heading',$faq->heading)}}"/>
                </div><div class="form-group">
                    <label for="exampleInputPassword1">Contain</label>
                    <input name="contain" type="text" class="form-control" placeholder="contain" value="{{old('contain',$faq->contain)}}"/>
                </div>

                @else
                <div class="form-group">
                    <label for="exampleInputPassword1">Heading</label>
                    <input name="heading" type="text" class="form-control" placeholder="heading" value="{{old('heading')}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Contain</label>
                    <input name="contain" type="text" class="form-control" placeholder="contain" value="{{old('contain')}}"/>
                </div>


            @endif
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>

@endsection
