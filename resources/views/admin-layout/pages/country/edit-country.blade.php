@extends('admin-layout.app')

@section('body-content')

    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.update-country', $country->id)}}" method="post" >
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">Country Name</label>
                <input name="name" type="text" class="form-control" placeholder="Enter country name" value="{{old('name',$country->name)}}"/>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Update</button>
            </div>
        </form>
        <div class="container">
            <h2>Related State List</h2>
            <a href="{{route('cpanel.admin.add-state',$country->id)}}" class="btn btn-primary cast-new">Add New State</a>
            <table class="table casts_table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Total city</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($states as $state):
                $related_city_count = \App\model\City::where('ref_state',$state->id)->count();
                ;
                ?>
                <tr class="cast-row">
                    <td>
                        <b>{{$state->name}}</b>
                    </td>
                    <td>{{ $related_city_count }}</td>
                    <td>
                        <a href="{{route('cpanel.admin.edit-state',['country_id' => $country->id, 'state_id' => $state->id])}}" class="btn btn-primary">Edit State</a>
                        <a onclick="onClickDelete(event)" href="{{route('cpanel.admin.delete-state',['country_id' => $country->id, 'state_id' => $state->id])}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>


    </div>
@endsection
