@extends('admin-layout.app')

@section('body-content')

    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.add-country')}}" method="post" >
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">Country Name</label>
                <input name="name" type="text" class="form-control" placeholder="Enter country name" value="{{old('name','')}}"/>
                <input type="hidden" value="" name="states" class="states_input"/>
            </div>
            <div class="container">
                <h2>Related States List</h2>
                <button class="btn btn-primary state-new">Add New State</button>
                <table class="table states_table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="state-row">
                        <td>
                            <input class="form-control country_state" placeholder="Enter state name"/>
                        </td>
                        <td>
                            <button class="btn btn-danger">Delete</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
    <script>
        let $btn_submit = $("button[type=submit]");
        let $all_rows = null;
        let $all_rows_delete = null;
        function reDeclaredAvailableRows(){
            if($all_rows_delete){
                $all_rows_delete.off("click");
            }

            $all_rows = $states_table.find('tr.state-row');
            $all_rows_delete = $all_rows.find("button.btn.btn-danger");
            $all_rows_delete.on("click", function(e){
                e.preventDefault();
                let index = $all_rows_delete.index(this);
                $all_rows_delete.off("click");
                $all_rows[index].remove();
                reDeclaredAvailableRows();
            });
        }

        let $hidden_input = $('input.states_input');
        let $btn_state_new = $("button.state-new");
        let $states_table = $('table.table.states_table tbody');
        let $template_state_row = $states_table.find('tr.state-row').clone();
        $all_rows = $template_state_row;
        reDeclaredAvailableRows();
        $btn_state_new.on("click",function(e){
            e.preventDefault();
            $states_table.append($template_state_row.clone());
            reDeclaredAvailableRows();
        });

        $btn_submit.on("click",function(e){
            if($all_rows.length > 0){
                let final_json_data = [];
                let have_error = false;
                $.each($all_rows, function(index, dom){
                    let val = $.trim($(dom).find('input').val());
                    if(!have_error && val.length <= 0){
                        have_error = true;
                    }
                    final_json_data.push(val);
                });
                if(have_error){
                    alert("State name field should not be blanked.");
                    e.preventDefault();
                    return false;
                }else{
                    $hidden_input.val(JSON.stringify(final_json_data));
                }
            }else{
                alert('You need to enter at-least one state for this country.');
                e.preventDefault();
                return false;
            }
        });
    </script>
@endsection
