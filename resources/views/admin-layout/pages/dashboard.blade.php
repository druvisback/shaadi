@extends('admin-layout.app')

@section('body-content')

    <!-- Small boxes (Stat box) -->
    <div class="row">

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ App\User::all()->count() }}</h3>

                    <p>All Members </p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{route('cpanel.admin.user-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <?php
            $admin_user = \App\User::where('type','admin')->first();

            $profiles_males_total = \App\model\Profile::where('ref_user','!=',$admin_user->id)->where('gender','male')->count();
            $profiles_females_total = \App\model\Profile::where('ref_user','!=',$admin_user->id)->where('gender','female')->count();
            $profiles_status_total = \App\model\Profile::where('ref_user','!=',$admin_user->id)->where('status','Inactive')->count();
        ?>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{$profiles_status_total}}</h3>

                    <p>New Members</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="{{route('cpanel.admin.new-user-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$profiles_females_total}}</h3>
                    <p>Female Members</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{route('cpanel.admin.female-user-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$profiles_males_total}}</h3>
                    <p>Male Members</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="{{route('cpanel.admin.male-user-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>

@stop
