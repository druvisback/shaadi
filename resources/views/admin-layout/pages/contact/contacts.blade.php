@extends('admin-layout.app')

@section('body-content')
    <div class="row" style="padding: 15px;">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <!-- /.box -->

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Contact Table</h3>

                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <table id="example1" class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Full Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Time</th>
                                    <th scope="col">Comment</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $contacts = \App\model\Contact::all();
                                if($contacts->isNotEmpty()):
                                $count = 0;
                                foreach($contacts as $contact):
                                $count++;
                                ?>
                                <tr>
                                    <th scope="row">{{$count}}</th>
                                    <td>{{ $contact->full_name  }}</td>
                                    <td>{{ $contact->email }}</td>
                                    <td>{{ $contact->comment }}</td>
                                    <td>{{ $contact->created_at }}</td>

                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">

                                            <a href="{{route('cpanel.admin.message',$contact->id)}}" class="btn btn-secondary">Message</a>
{{--

                                        </div>
                                    </td>

                                    {{--                <td>--}}
                                    {{--                    <div class="btn-group" role="group" aria-label="Basic example">--}}
                                    {{--                        <a href="{{route('cpanel.admin.edit-testimonial',$testimonial->id)}}" class="btn btn-secondary">Edit</a>--}}
                                    {{--                        <!--<a href="{{route('cpanel.admin.delete-testimonial',$testimonial->id)}}" type="button" class="btn btn-danger">Delete</a>-->--}}
                                    {{--                    </div>--}}
                                    {{--                </td>--}}
                                </tr>
                                <?php endforeach;
                                else:
                                ?>
                                <tr><td colspan="7">
                                        <div class="alert alert-danger" role="alert">
                                            No Testimonial exist on system, please add new
                                        </div>
                                    </td></tr>
                                <?php
                                endif; ?>
                                </tbody>
                            </table>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>


    </div>


@stop
