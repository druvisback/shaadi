@extends('admin-layout.app')

@section('body-content')
    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.save-story')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">Story Content</label>
                <textarea name="content" rows="10" class="form-control">{{$story_content}}</textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@stop
