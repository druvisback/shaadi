@extends('admin-layout.app')

@section('body-content')

    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.save-faq')}}" method="post" enctype="multipart/form-data">
            @csrf
            @if(isset($faq_id))
                <?php $faq =  \App\model\Faq::find($faq_id); ?>
                <input type="hidden" name="id" value="{{$faq_id}}"/>
                <div class="form-group">
                    <label for="exampleInputPassword1">Title</label>
                    <input name="title" type="text" class="form-control" placeholder="title" value="{{old('title',$faq->title)}}"/>
                </div><div class="form-group">
                    <label for="exampleInputPassword1">Question</label>
                    <input name="question" type="text" class="form-control" placeholder="question" value="{{old('question',$faq->question)}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Answer</label>
                    <input name="answer" type="text" class="form-control" placeholder="answer" value="{{old('answer',$faq->answer)}}"/>
                </div>
                @else
                <div class="form-group">
                    <label for="exampleInputPassword1">Title</label>
                    <input name="title" type="text" class="form-control" placeholder="title" value="{{old('title')}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">question</label>
                    <input name="question" type="text" class="form-control" placeholder="question" value="{{old('question')}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">answer</label>
                    <input name="answer" type="text" class="form-control" placeholder="answer" value="{{old('answer')}}"/>
                </div>

            @endif
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>

@endsection
