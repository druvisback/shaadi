@extends('admin-layout.app')

@section('body-content')

    <div class="row" style="padding: 15px;">
    <form class="form-horizontal" action="{{route('cpanel.admin.update-faq')}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$faqById->id}}"/>
        <div class="form-group">
            <label class="col-md-3 control-label">Please Type Your FAQ Title</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="title" value="{{$faqById->title}}" placeholder="please type faq title" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Please Type Your Question</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="question" value="{{$faqById->question}}" placeholder="please type your question" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Please Type Your Answer</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="answer" value="{{$faqById->answer}}" placeholder="please type your answer" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12" style="text-align: center">
                <button type="submit" class="btn btn-sm btn-success">Submit</button>
            </div>
        </div>

    </form>
</div>
</div>
<!-- end panel -->
</div>

@endsection


