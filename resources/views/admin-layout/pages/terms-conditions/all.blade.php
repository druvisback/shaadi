@extends('admin-layout.app')

@section('body-content')

    <script>
        function onClickDelete(e) {
            if(!confirm('Are you want to delete ?')){
                e.preventDefault();
                return false;
            }
        }
    </script>
    <div class="row" style="padding: 15px;">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <!-- /.box -->

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Faq Table</h3>
                            <div>
                                <a class="btn btn-info" href="{{route('cpanel.admin.add-terms')}}">Add Terms & Condition</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Heading</th>
                                    <th scope="col">contain</th>

                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $faqs = \App\model\Condition::all();
                                if($faqs->isNotEmpty()):
                                $count = 0;
                                foreach($faqs as $faq):
                                $count++;
                                ?>
                                <tr>
                                    <th scope="row">{{$count}}</th>
                                    <td>{{ $faq->heading }}</td>
                                    <td>{{ $faq->contain }}</td>

                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="{{route('cpanel.admin.edit-terms',$faq->id)}}" class="btn btn-secondary">Edit</a>
                                            <a onclick="onClickDelete(event)" href="{{route('cpanel.admin.delete-terms',$faq->id)}}" type="button" class="btn btn-danger">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach;
                                else:
                                ?>
                                <tr><td colspan="7">
                                        <div class="alert alert-danger" role="alert">
                                            No Terms & condition exist on system, please add new
                                        </div>
                                    </td></tr>
                                <?php
                                endif; ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>


    </div>
@stop
