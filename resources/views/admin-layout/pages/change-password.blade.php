@extends('admin-layout.app')

@section('body-content')
    <div class="row">
            <form style="padding: 15px;" class="form-group" action="{{route('cpanel.admin.change-password')}}" method="post">
                @csrf()
                <div class="form-group">
                    <label>Old Password</label>

                        <input type="password" class="form-control" name="old_pass" placeholder="Enter Old Password" />

                </div>


                <label>New Password</label>
                <input type="password" class="form-control" value="{{ old('new_password','') }}" name="new_password"/>
                <label>Confirm Password</label>
                <input type="password" class="form-control" value="{{ old('cnf_password','') }}" name="cnf_password"/>
                <br>
                <button style="width: 100%;" class="btn btn-info" type="submit">Submit</button>

            </form>
    </div>
@stop
