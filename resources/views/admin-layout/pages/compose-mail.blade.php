@extends('admin-layout.app')

@section('body-content')
    <div class="row">
        <?php
            if($target_user):
                $target_profile = \App\model\Profile::where('ref_user',$target_user->id)->first();

                $str_name = false;
                if($target_profile):
                    $str_name = ucfirst($target_profile->first_name)." ".ucfirst($target_profile->last_name);
                endif;
            //$target_user->id;
        ?>
            <form style="padding: 15px;" class="form-group" action="{{route('cpanel.admin.compose-mail',$target_user->id)}}" method="post">
                @csrf()
                <label>To</label>
                <input type="text" class="form-control" disabled value="{{($str_name)?$str_name.' ('.$target_user->email.')':$target_user->email}}"/>
                <input type="hidden"  value="{{$target_user->email}}" name="to"/>
                <label>Subject</label>
                <input name="subject" type="text" class="form-control" value="{{env('APP_NAME','SHAADI.COM')}} - //YOUR_SUBJECT"/>
                       <label>Message</label>
                        <textarea rows="10" type="text" class="form-control" name="message">Dear {{$str_name}},

   Message:
      We glad to tell you, that //Write  your message//

Sincere, {{env('APP_NAME','SHAADI.COM')}} Team</textarea>



                        <label>Action</label>
                        <button style="width: 100%;" class="btn btn-info" type="submit">Submit</button>

            </form>
        <?php endif; ?>
    </div>
@stop
