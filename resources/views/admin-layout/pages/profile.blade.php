@extends('admin-layout.app')

@section('body-content')

<?php
    $first_name = ($profile)?$profile->first_name:"";
    $gender = ($profile)?$profile->gender:"";
    $last_name = ($profile)?$profile->last_name:"";
    $photo = ($profile)?$profile->profile_photo:"";
?>

    <div class="row">
        <form style="padding: 15px;" class="form-group" action="{{route('cpanel.admin.profile')}}" method="post" enctype="multipart/form-data">
            @csrf()
            <input type="hidden" name="ref_user" value="{{ $user->id }}"/>

            <div class="form-group">
                <label>Photo</label>
                <input name="profile_photo" type="file" class="form-control" placeholder="Select Photo" value="{{ old('profile_photo',$photo) }}"/>
            </div>
            <div class="form-group">
            <label>First Name</label>
            <input type="text" class="form-control" name="first_name" value="{{ old('first_name',$first_name) }}"/>
            </div>
            <div class="form-group">
            <label>Last Name</label>
            <input type="text" class="form-control" name="last_name" value="{{ old('last_name',$last_name) }}"/>
            </div>
            <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" value="{{ $user->email }}" />
            </div>


                <label>Gender</label>

            <?php
                $old_gender = old("gender",$gender);
            ?>
            <select class="form-control" name="gender">
                <option {{($old_gender == "male")?"selected":""}} value="male">Male</option>
                <option {{($old_gender == "female")?"selected":""}} value="female">Female</option>
                <option {{($old_gender == "other")?"selected":""}} value="other">Other</option>
            </select>
            </div>
            <br>
            <button type="submit" class="btn btn-info">Submit</button>
        </form>
    </div>
@stop
