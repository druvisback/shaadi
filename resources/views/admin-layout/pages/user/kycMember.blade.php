@extends('admin-layout.app')

@section('body-content')

    <script>
        function onClickDelete(e) {
            if(!confirm('Are you want to discard KYC approval ?')){
                e.preventDefault();
                return false;
            }
        }
    </script>

    <script>
        function onClickActive(e) {
            if(!confirm('Are you want to approve this user ?')){
                e.preventDefault();
                return false;
            }
        }
    </script>


    <div class="row">

        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <!-- /.box -->

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">User Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Proof Of Identity</th>
                                    <th scope="col">Proof Of Address</th>
                                    <th scope="col">Profile Link</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $kycs = \App\model\Kyc::all()->where('is_check',false);
                                if($kycs->isNotEmpty()):
                                $count = 0;
                                foreach($kycs as $kyc):
                                $count++;
                                $p = \App\model\Profile::find($kyc->ref_profile_id);
                                $u = \App\User::find($p->ref_user);

                                $poi_type = strtoupper(str_replace('_',' ',$kyc->type_of_poi));
                                $poa_type = strtoupper(str_replace('_',' ',$kyc->type_of_poa));

                                $poi_photo_url = \App\Http\Controllers\ExtraController::getKYCPhoto_URL($p, $kyc->poi_file);
                                $poa_photo_url = \App\Http\Controllers\ExtraController::getKYCPhoto_URL($p, $kyc->poa_file);
                                ?>

                                <tr>
                                    <th scope="row">{{$count}}</th>
                                    <td>{{ $u->email }}</td>
                                    <td>{{ ($p)?$p->first_name.' '.$p->last_name:"--" }}</td>
                                    <td>{{ $poi_type}}(<a target="_blank" class="btn btn-sm btn-link" href="{{ $poi_photo_url }}">Photo Link</a>)</td>
                                    <td>{{ $poa_type }}(<a target="_blank" class="btn btn-sm btn-link" href="{{ $poa_photo_url }}">Photo Link</a>)</td>
                                    <td>
                                        @if($p && $p->is_complete_profile)
                                            <a target="_blank" href="{{route('member.view-profile', $p->id)}}">Click Here</a>
                                        @else
                                            Incomplete Profile
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="{{route('cpanel.admin.aproved-kyc-user',$kyc->id)}}"  class="btn btn-secondary">Approved</a>
                                            <a  onclick="onClickDelete(event)" href="{{route('cpanel.admin.disaproved-kyc-user',$kyc->id)}}" type="button" class="btn btn-secondary">Disapproved</a>
                                            <a href="{{route('cpanel.admin.compose-mail',$u->id)}}" class="btn btn-secondary">Message</a>
                                            {{--                                                                    <a href="{{route('',$u->id)}}" class="btn btn-secondary">Message</a>--}}

                                        </div>
                                    </td>

                                </tr>
                                <?php endforeach;
                                else:
                                ?>
                                <tr><td colspan="7">
                                        <div class="alert alert-danger" role="alert">
                                            No user pended on system for KYC verification
                                        </div>
                                    </td></tr>
                                <?php
                                endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>





    </div>
@stop
