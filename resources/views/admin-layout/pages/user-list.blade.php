@extends('admin-layout.app')

@section('body-content')

    <script>
        function onClickDelete(e) {
            if(!confirm('Are you want to delete ?')){
                e.preventDefault();
                return false;
            }
        }
    </script>

    <div class="row">

        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <!-- /.box -->

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">User Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Photo</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Gender</th>
                                    <th scope="col">User Role</th>
                                    <th scope="col">Profile Link</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $user_profiles = \App\model\Profile::all();
                                $count = 0;
                                foreach($user_profiles as $p):
                                $count++;
                                $u = \App\User::find($p->ref_user);
                                if($u && $u->type != "admin"):
                                //dd($profile);
                                ?>
                                <tr>
                                    <th scope="row">{{$count}}</th>
                                    <td><img src="{{ \App\Http\Controllers\ExtraController::getProfilePhotoURL($p) }}" class="img-responsive" width="60px" height="60px"/></td>
                                    <td>{{ ($u)?$u->email:"--" }}</td>
                                    <td>{{ ($p)?$p->first_name.' '.$p->last_name:"--" }}</td>
                                    <td>{{ ($p)?ucfirst($p->gender):"--" }}</td>
                                    <?php
                                    $role = $u->type;
                                    $txt_role = "--";
                                    switch ($role):
                                        case 'admin':
                                            $txt_role = "Admin";
                                            break;
                                        case 'member':
                                            $txt_role = "Member";
                                            break;
                                        case 'subscriber':
                                            $txt_role = "Subscriber";
                                            break;
                                    endswitch;
                                    ?>
                                    <td>{{ $txt_role }}</td>
                                    <td>
                                        <?php //dd($p->is_complete_profile); ?>
                                        @if($p && $p->is_complete_profile)
                                            <a target="_blank" href="{{route('member.view-profile', $p->id)}}">Click Here</a>
                                        @else
                                            Incomplete Profile
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="{{route('cpanel.admin.compose-mail',$u->id)}}" class="btn btn-secondary">Message</a>
                                            {{--                        <a href="{{route('',$u->id)}}" class="btn btn-secondary">Message</a>--}}
                                            <a  onclick="onClickDelete(event)" href="{{route('cpanel.admin.delete-user',$u->id)}}" type="button" class="btn btn-secondary">Delete</a>
                                        </div>
                                    </td>

                                </tr>
                                <?php endif; endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>





    </div>
@stop
