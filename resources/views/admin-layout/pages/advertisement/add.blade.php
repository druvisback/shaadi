@extends('admin-layout.app')

@section('body-content')

    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.save-add')}}" method="post" enctype="multipart/form-data">
            @csrf
            @if(isset($add_id))
                <?php $add =  \App\model\Advertisement::find($add_id); ?>
                <input type="hidden" name="id" value="{{$add_id}}"/>
                <div class="form-group">
                    <label for="exampleInputPassword1">Title</label>
                    <input name="title" type="text" class="form-control" placeholder="title" value="{{old('title',$faq->title)}}"/>
                </div><div class="form-group">
                    <label for="exampleInputPassword1">Image/Video</label>
                    <input name="photo" type="file" class="form-control" placeholder="Submit Image Or Video" value="{{old('Img',$faq->Img)}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Desscription</label>
                    <input name="dess" type="text" class="form-control" placeholder="Desscription Of Add" value="{{old('dess',$add->dess)}}"/>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Extarnal Link</label>
                    <input name="e_link" type="text" class="form-control" placeholder="Link Of The Add" value="{{old('e_link',$add->e_link)}}"/>
                </div>
                @else
                <div class="form-group">
                    <label for="exampleInputPassword1">Title</label>
                    <input name="title" type="text" class="form-control" placeholder="title" value="{{old('title')}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Image/video</label>
                    <input name="photo" type="file" class="form-control" placeholder="Submit Image Or Video" value="{{old('img')}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Desscription</label>
                    <input name="dess" type="text" class="form-control" placeholder="answer" value="{{old('dess')}}"/>
                </div>


                <div class="form-group">
                    <label for="exampleInputPassword1">Extarnal Link</label>
                    <input name="e_link" type="text" class="form-control" placeholder="Link Of The Add" value="{{old('e_link')}}"/>
                </div>

            @endif
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>

@endsection
