@extends('admin-layout.app')

@section('body-content')

    <div class="row" style="padding: 15px;">
    <form class="form-horizontal" action="{{route('cpanel.admin.update-add')}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$addById->id}}"/>
        <div class="form-group">
            <label class="col-md-3 control-label">Title</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="title" value="{{$addById->title}}" placeholder="please type title" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Desscription</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="dess" value="{{$addById->dess}}" placeholder="please type your question" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Extarnal Link</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="e_link" value="{{$addById->e_link}}" placeholder="please type your answer" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12" style="text-align: center">
                <button type="submit" class="btn btn-sm btn-success">Submit</button>
            </div>
        </div>

    </form>
</div>
</div>
<!-- end panel -->
</div>

@endsection
