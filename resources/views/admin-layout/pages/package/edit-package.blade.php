@extends('admin-layout.app')

@section('body-content')
<div class="row" style="padding: 15px;">
    <form action="{{route('cpanel.admin.edit-package',$package->id)}}" method="post">
        @csrf
        <div class="profile-indiv-info">
            <h4 class="divider-3">Edit Package</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="profile-content">
                        <div class="profile-listing">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Package Name</label>
                                <input name="name" type="text" class="form-control" placeholder="Enter Package Name" value="{{old('name',$package->name)}}"/>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Package Description</label>
                                <textarea name="desc" class="form-control">{{old('desc',$package->desc)}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="profile-content">
                        <div class="profile-listing">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Capabilities</label>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Capability</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Daily Total Profile View Limits (-1 for Unlimited)</td>
                                        <td><input name="caps_daily_total_view_limit" class="form-check-input" value="{{old('caps_daily_total_view_limit',$package->caps_daily_total_view_limit)}}"/></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Can see member photos</td>
                                        <td><input name="caps_see_photos" class="form-check-input" type="checkbox" {{(!!old('caps_see_photos',$package->caps_see_photos))?'checked':''}}/></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Can send message</td>
                                        <td><input name="caps_send_msgs" class="form-check-input" type="checkbox" {{(!!old('caps_send_msgs',$package->caps_send_msgs))?'checked':''}}/></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="profile-content">
                        <div class="profile-listing">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Total Months</label>
                                <input name="months" type="number" class="form-control" placeholder="Enter total months" value="{{old('months',$package->months)}}"/>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Price Per Month</label>
                                <input name="sale_price" type="text" class="form-control" placeholder="Enter Package Price" value="{{old('sale_price',$package->sale_price)}}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>


@stop
