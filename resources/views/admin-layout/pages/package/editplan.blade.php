@extends('admin-layout.app')

@section('body-content')

    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.update-plan')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="profile-indiv-info">
                <h4 class="divider-3">About Plan</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="profile-listing">
                                <h5>Plan Basic Info</h5>

                                <input type="hidden" name="id" value="{{$planById->id}}"/>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Plan Name</label>
                                            <input name="plan_name" type="text" class="form-control" placeholder="Enter Plan Name" value="{{$planById->plan_name}}"/>
                                    </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Plane Description</label>
                                <input name="p_des" type="text" class="form-control" placeholder="Enter Plan Price" value="{{$planById->p_des}}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="profile-listing">
                                <h5>Plan Limitation</h5>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Daily Message Limit</label>
                            <input name="d_mess" type="number" class="form-control" placeholder="Enter Daily Message Limit" value="{{$planById->d_mess}}"/>
                        </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Daily Profile Visit Limit</label>
                            <input name="d_prof" type="number" class="form-control" placeholder="Enter Daily Message Limit" value="{{$planById->d_prof}}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-content">
                            <div class="profile-listing">
                                <h5>Plan Validation & Value</h5>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">price</label>
                                    <input name="price" type="text" class="form-control" placeholder="Enter Plan Price" value="{{$planById->price}}"/>
                                </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Month</label>
                            <input name="month" type="text" class="form-control" placeholder="EnterNo Of Month" value="{{$planById->month}}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>


@endsection


