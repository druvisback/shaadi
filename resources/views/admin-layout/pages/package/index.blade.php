
@extends('admin-layout.app')

@section('body-content')


    <script>
        function onClickDelete(e) {
            if(!confirm('Are you want to delete ?')){
                e.preventDefault();
                return false;
            }
        }
    </script>

    <div class="row" style="padding: 15px;">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <!-- /.box -->

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">All Packages</h3>
                            <div>
                                <a class="btn btn-info" href="{{route('cpanel.admin.add-package')}}">Add Package</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">


                            <table id="example1" class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Months</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=0?>
                                @foreach($packages as $package)
                                    <?php $i++;
                                    if($i%2==0){
                                        $class='even gradeC';
                                    }else{
                                        $class='odd gradeX';
                                    }
                                    ?>
                                    <tr class="{{$class}}">
                                        <td>{{$i}}</td>
                                        <td>{{$package->name}}</td>
                                        <td>{{$package->desc}}</td>
                                        <td>{{$package->sale_price}}</td>
                                        <td>{{$package->months}}</td>
                                        <td>
                                            <a href="{{route('cpanel.admin.edit-package', $package->id)}}" class="btn btn-secondary">Edit</a>
                                            <a  onclick="onClickDelete(event)" href="{{route('cpanel.admin.delete-package', $package->id)}}" type="button" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>


    </div>
@stop






















