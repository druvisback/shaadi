@extends('admin-layout.app')

@section('body-content')
    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.edit-cast',['religion_id' => $religion->id, 'cast_id' => $cast->id])}}" method="post" >
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">Edit Cast Name '{{$cast->name}}' for Religion '{{$religion->name}}'</label>
                <input name="name" type="text" class="form-control" placeholder="Enter cast name" value="{{old('name',$cast->name)}}"/>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
@endsection
