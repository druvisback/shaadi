@extends('admin-layout.app')

@section('body-content')

<div class="row" style="padding: 15px;">
    <form action="{{route('cpanel.admin.update-religion', $religion->id)}}" method="post" >
        @csrf
        <div class="form-group">
            <label for="exampleInputPassword1">Religion Name</label>
            <input name="name" type="text" class="form-control" placeholder="Enter religion name" value="{{old('name',$religion->name)}}"/>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Update</button>
        </div>
    </form>
        <div class="container">
            <h2>Related Casts List</h2>
            <a href="{{route('cpanel.admin.add-cast',$religion->id)}}" class="btn btn-primary cast-new">Add New Cast</a>
            <table class="table casts_table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($casts as $cast): ?>
                <tr class="cast-row">
                    <td>
                        <b>{{$cast->name}}</b>
                    </td>
                    <td>
                        <a href="{{route('cpanel.admin.edit-cast',['religion_id' => $religion->id, 'cast_id' => $cast->id])}}" class="btn btn-primary">Edit Cast</a>
                        <a onclick="onClickDelete(event)" href="{{route('cpanel.admin.delete-cast',['religion_id' => $religion->id, 'cast_id' => $cast->id])}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>


</div>
@endsection
