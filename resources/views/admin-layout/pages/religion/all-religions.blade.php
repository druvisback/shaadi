@extends('admin-layout.app')

@section('body-content')

<div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-info" href="{{route('cpanel.admin.add-religion')}}">Add Religion</a>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-striped table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Total Casts</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $data = \App\model\Religion::all();
                    if($data->isNotEmpty()):
                    $count = 0;
                    foreach($data as $item):
                    $related_casts_count = \App\model\Cast::where('ref_religion',$item->id)->count();
                    $count++;
                ?>
                <tr>
                    <th scope="row">{{$count}}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $related_casts_count }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{ route('cpanel.admin.edit-religion', $item->id) }}" class="btn btn-primary">Edit</a>
                            <a onclick="onClickDelete(event)" href="{{ route('cpanel.admin.delete-religion', $item->id) }}" type="button" class="btn btn-danger">Delete</a>
                        </div>
                    </td>
                </tr>
                <?php endforeach;
                else:
                ?>
                <tr><td colspan="7">
                        <div class="alert alert-danger" role="alert">
                            No Religion exist on system, please add new
                        </div>
                    </td></tr>
                <?php
                endif; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@stop
