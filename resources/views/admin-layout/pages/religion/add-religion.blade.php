@extends('admin-layout.app')

@section('body-content')

    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.add-religion')}}" method="post" >
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">Religion Name</label>
                <input name="name" type="text" class="form-control" placeholder="Enter religion name" value="{{old('name','')}}"/>
                <input type="hidden" value="" name="casts" class="casts_input"/>
            </div>
            <div class="container">
                <h2>Related Casts List</h2>
                <button class="btn btn-primary cast-new">Add New Cast</button>
                <table class="table casts_table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="cast-row">
                        <td>
                            <input class="form-control religion_cast" placeholder="Enter cast name"/>
                        </td>
                        <td>
                            <button class="btn btn-danger">Delete</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
    <script>
        let $btn_submit = $("button[type=submit]");
        let $all_rows = null;
        let $all_rows_delete = null;
        function reDeclaredAvailableRows(){
            if($all_rows_delete){
                $all_rows_delete.off("click");
            }

            $all_rows = $casts_table.find('tr.cast-row');
            $all_rows_delete = $all_rows.find("button.btn.btn-danger");
            $all_rows_delete.on("click", function(e){
                e.preventDefault();
                let index = $all_rows_delete.index(this);
                $all_rows_delete.off("click");
                $all_rows[index].remove();
                reDeclaredAvailableRows();
            });
        }

        let $hidden_input = $('input.casts_input');
        let $btn_cast_new = $("button.cast-new");
        let $casts_table = $('table.table.casts_table tbody');
        let $template_cast_row = $casts_table.find('tr.cast-row').clone();
        $all_rows = $template_cast_row;
        reDeclaredAvailableRows();
        $btn_cast_new.on("click",function(e){
            e.preventDefault();
            $casts_table.append($template_cast_row.clone());
            reDeclaredAvailableRows();
        });

        $btn_submit.on("click",function(e){
            if($all_rows.length > 0){
                let final_json_data = [];
                let have_error = false;
                $.each($all_rows, function(index, dom){
                    let val = $.trim($(dom).find('input').val());
                    if(!have_error && val.length <= 0){
                        have_error = true;
                    }
                    final_json_data.push(val);
                });
                if(have_error){
                    alert("Cast name field should not be blanked.");
                    e.preventDefault();
                    return false;
                }else{
                    $hidden_input.val(JSON.stringify(final_json_data));
                }
            }else{
                alert('You need to enter at-least one cast for this religion.');
                e.preventDefault();
                return false;
            }
        });
    </script>
@endsection
