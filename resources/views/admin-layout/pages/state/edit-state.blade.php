@extends('admin-layout.app')

@section('body-content')
    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.edit-state',['country_id' => $country->id, 'state_id' => $state->id])}}" method="post" >
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">Edit state Name '{{$state->name}}' for Country '{{$country->name}}'</label>
                <input name="name" type="text" class="form-control" placeholder="Enter state name" value="{{old('name',$state->name)}}"/>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Update</button>
            </div>
        </form>
    </div>
    <div class="container">
        <h2>Related State List</h2>
        <a href="{{route('cpanel.admin.add-city',$state->id)}}" class="btn btn-primary cast-new">Add New City</a>
        <table class="table casts_table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Total city</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($cities as $city):
            ?>
            <tr class="cast-row">
                <td>
                    <b>{{$city->name}}</b>
                </td>
                <td>
                    <a href="{{route('cpanel.admin.edit-city',['state_id' => $state->id, 'city_id' => $city->id])}}" class="btn btn-primary">Edit City</a>
                    <a onclick="onClickDelete(event)" href="{{route('cpanel.admin.delete-city',['state_id' => $state->id, 'city_id' => $city->id])}}" class="btn btn-danger">Delete</a>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    </div>
@endsection
