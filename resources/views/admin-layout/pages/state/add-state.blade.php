@extends('admin-layout.app')

@section('body-content')
    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.add-state',$country->id)}}" method="post" >
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">New State Name for Country '{{$country->name}}'</label>
                <input name="name" type="text" class="form-control" placeholder="Enter state name" value="{{old('name','')}}"/>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
@endsection
