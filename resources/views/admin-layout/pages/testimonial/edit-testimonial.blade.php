@extends('admin-layout.app')

@section('body-content')
    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.update-testimonial')}}" method="post" enctype="multipart/form-data">
            @csrf
                <input type="hidden" name="id" value="{{$testimonial->id}}"/>
                <div class="form-group">
                    <label for="exampleInputEmail1">Photo</label>
                    <input name="photo" type="file" class="form-control" placeholder="Select Photo" value="{{old('photo',$testimonial->photo)}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Name</label>
                    <input name="name" type="text" class="form-control" placeholder="Name" value="{{old('name',$testimonial->name)}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Country</label>
                    <input name="country" type="text" class="form-control" placeholder="Country" value="{{old('country',$testimonial->country)}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Job</label>
                    <input name="job" type="text" class="form-control" placeholder="Job" value="{{old('job',$testimonial->job)}}"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    <textarea name="desc" class="form-control" rows="5">{{old('desc',$testimonial->desc)}}</textarea>
                </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@stop
