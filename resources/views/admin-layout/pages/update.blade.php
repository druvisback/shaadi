@extends('admin-layout.app')

@section('body-content')
    <div class="row" style="padding: 15px;">
        <form action="{{route('cpanel.admin.update-user-k')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$profile->id}}"/>
    <div class="form-group">
        <label for="exampleInputPassword1">Active/Deactivate</label>
        <select name="status" class="form-control">

            <option value="">Select</option>
            <option value="Active" >Active</option>
            <option value="Inactive" >Deactivate</option>
        </select>
    </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Blocked/Unblocked</label>
                <select name="user_block" class="form-control">

                    <option value="">Select</option>
                    <option value="Blocked" >Blocked</option>
                    <option value="Unblocked">Unblocked</option>
                </select>
            </div>

            <div class="form-group">
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
    </form>
    </div>

@endsection
