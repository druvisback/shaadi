<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lagan Utsav</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="css/mega-menu/mega_menu.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="css/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="css/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />


    <link href="css/general.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <!-- Style customizer -->
    <!--<link rel="stylesheet" type="text/css" href="css/skins/skin-default.css" data-style="styles" />-->

</head>

<body>


<!--=================================
header -->
<header id="header" class="fancy">

    <!--=================================
   mega menu -->
    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- menu start -->
                    <nav id="menu" class="mega-menu">
                        <!-- menu list items container -->
                        <section class="menu-list-items">
                            <!-- menu logo -->
                            <ul class="menu-logo">
                                <li><a href="index.html"><img id="logo_color_img" src="images/dark-logo.png" alt="logo" /> </a></li>
                            </ul>
                            <!-- menu links -->
                            <ul class="menu-links">
                                <!-- active class -->
                                <li><a href="#"> Login </a>
                                </li>
                                <li><a href="#"> Register </a>
                                </li>
                            </ul>
                        </section>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

<!--=================================
 header -->
<!--=================================
 inner-intro-->

<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(images/bg/inner-bg-1.jpg);">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-md-12 mt-7">
                <ul class="page-breadcrumb">
                    <li><a href="index.html"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                    <li><span>FAQ</span> </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!--=================================
 banner -->


<section class="faq-page page-section-ptb">
    <div class="container">
        <div class="row justify-content-center mb-5 sm-mb-3">
            <div class="col-md-8 text-center">
                <h2 class="title title3 divider wow fadeInUp animated mb-3">Frequently Asked Questions</h2>
                <p class="lead">Have a question? Please check our knowledgebase first.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="accordion arrow light-rounded sm-mb-0">
                    <div class="acd-group acd-active"><a href="#" class="acd-heading">Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit</a>
                        <div class="acd-des">
                            <h5 class="title mb-2">Sed ut perspiciatis unde omnis iste natus?</h5>
                            <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt lorem ipsum dolor sit amet of Lorem Ipsum. auctor a ornare odio. Sed non mauris vitae erat</p>
                            <ol class="list-unstyled">
                                <li>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum</li>
                                <li>Auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. </li>
                                <li>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</li>
                                <li>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit Class.</li>
                            </ol>
                        </div>
                    </div>
                    <div class="acd-group"><a href="#" class="acd-heading">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor?</a>
                        <div class="acd-des">
                            <h5 class="title mb-2">There are many variations of passages alteration?</h5>
                            <p> Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise , except to obtain some advantage from it? But who has any right .</p>
                            <ol class="list-unstyled">
                                <li>Et harum quidem rerum facilis est et expedita distinctio nam libero tempore.</li>
                                <li>Except to obtain some advantage ipsum, nec sagittis sem nibh id elit. </li>
                                <li>Sed non mauris vitae amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</li>
                                <li>Sed non  mauris vitae erat consequat auctor eu in elit Class. Nam nec tellus a odio tincidunt auctor a ornare odio. .</li>
                            </ol>
                        </div>
                    </div>
                    <div class="acd-group"><a href="#" class="acd-heading">Nisi elit consequat ipsum, nec sagittis?</a>
                        <div class="acd-des">
                            <h5 class="title mb-2">It has survived not only five centuries?</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                            <ol class="list-unstyled">
                                <li>The generated Lorem Ipsum is therefore always free from repetition, injected humour.</li>
                                <li>You need to be sure there isn't anything embarrassing in the middle of text.</li>
                                <li>Except to obtain some advantage from it? But who has any right .</li>
                                <li>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit Class.</li>
                            </ol>
                        </div>
                    </div>
                    <div class="acd-group"><a href="#" class="acd-heading">Sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris?</a>
                        <div class="acd-des">
                            <h5 class="title mb-2">The standard Lorem Ipsum passage?</h5>
                            <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt Lorem ipsum dolor sit amet of Lorem Ipsum auctor a ornare odio. Sed non mauris vitae erat</p>
                            <ol class="list-unstyled">
                                <li> It uses a dictionary of over 200 Latin words, combined with a handful of model sentence</li>
                                <li>Morbi accumsan ipsum velit consequat ipsum, nec sagittis sem nibh id elit. </li>
                                <li>Sed non  mauris vitae erat consequat vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</li>
                                <li>The generated Lorem Ipsum is therefore always free from repetition, injected humour.</li>
                            </ol>
                        </div>
                    </div>
                    <div class="acd-group"><a href="#" class="acd-heading">Morbi accumsan ipsum velit. Nam nec tellus a odio?</a>
                        <div class="acd-des">
                            <h5 class="title mb-2">Rremaining essentially unchanged.?</h5>
                            <p> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <ol class="list-unstyled">
                                <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.</li>
                                <li>But I must explain to you how all this mistaken idea of denouncing pleasure.</li>
                                <li>Voluptatem accusantium nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</li>
                                <li>Perspiciatis tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit Class.</li>
                            </ol>
                        </div>
                    </div>
                    <div class="acd-group"><a href="#" class="acd-heading">Tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit?</a>
                        <div class="acd-des">
                            <h5 class="title mb-2">Sed ut perspiciatis unde omnis iste natus error?</h5>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptate nesciunt.</p>
                            <ol class="list-unstyled">
                                <li>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus</li>
                                <li>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus. </li>
                                <li>DMorbi accumsan ipsum velit. uis sed odio sit amet nibh vulputate cursus a sit amet mauris. </li>
                                <li>Consequat auctor eu in elit Class nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat .</li>
                            </ol>
                        </div>
                    </div>
                    <div class="acd-group"><a href="#" class="acd-heading">Class aptent taciti sociosqu?</a>
                        <div class="acd-des">
                            <h5 class="title mb-2">Lorem ipsum dolor sit amet of Lorem Ipsum?</h5>
                            <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain are bound to ensue; to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These </p>
                            <ol class="list-unstyled">
                                <li>The wise man therefore always holds in these matters to this principle of selection</li>
                                <li>Every pleasure is to be welcomed and every pain avoided. </li>
                                <li>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</li>
                                <li>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, but because occasionally circumstances .</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





<!--=================================
footer -->
<footer class="text-white text-center">
    <div class="footer-widget sm-mt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img src="images/dark-logo.png" class="img-fluid" alt="logo" id="footer-logo">
                </div>
                <div class="col-md-3">
                    <h6>Main Menu</h6>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Premium Plans</a></li>
                        <li><a href="#">Happy Stories</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>

                </div>
                <div class="col-md-3">
                    <h6>Quick search</h6>
                    <ul>
                        <li><a href="#">All Members</a></li>
                        <li><a href="#">Premium Members</a></li>
                        <li><a href="#">Free Members</a></li>
                    </ul>

                </div>
                <div class="col-md-3">
                    <h6>useful links</h6>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>

                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <p class="text-white">© 2019  - LaganUtsav | All Right Reserved </p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!--=================================
footer -->


<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-level-up"></i></a></div>

<!--=================================
 jquery -->

<!-- jquery  -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/mega-menu/mega_menu.js"></script>
<script type="text/javascript" src="js/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    var wow = new WOW(
        {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       true,       // trigger animations on mobile devices (default is true)
            live:         true,       // act on asynchronously loaded content (default is true)
            callback:     function(box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null,    // optional scroll container selector, otherwise use window,
            resetAnimation: true,     // reset animation on end (default is true)
        }
    );
    wow.init();
</script>

</body>

</html>
