@extends('layouts.member')

@section('body-content')
<div class="page-bg">
    <div class="container">
        <div class="bg-page-white  page-section-ptb">
            <div class="row">
                <div class="col-sm-12 col-md-8">
                    <div class="contactus-section">
                        <h2>Let's Get In Touch!</h2>
                        <p>We always love to hear from our customers! We are happy to answer your questions and assist you</p>
                        <!--Review Form Start-->

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <strong>{{Session::get('success')}}</strong>
                            </div>
                        @endif

                        <form action="{{route('page.contact')}}" method="post" class="tl-review-form">
                            @csrf()
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <!--Inner Holder Start-->
                                    <div class="inner-holder">
                                        <label>Full Name <i class="fa fa-star" aria-hidden="true"></i></label>
                                        <input name="full_name" type="text" value="{{old('full_name')}}"/>
                                    </div><!--Inner Holder End-->

                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <!--Inner Holder Start-->
                                    <div class="inner-holder">
                                        <label>Email <i class="fa fa-star" aria-hidden="true"></i></label>
                                        <input type="email" name="email" value="{{old('email')}}"/>
                                    </div><!--Inner Holder End-->
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <!--Inner Holder Start-->
                                    <div class="inner-holder">
                                        <label>Comment </label>
                                        <textarea name="comment">{{old('comment')}}</textarea>
                                    </div><!--Inner Holder End-->
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <!--Inner Holder Start-->
                                    <div class="inner-holder">
                                        <button class="btn-submit" type="submit">Submit</button>
                                    </div><!--Inner Holder End-->
                                </div>
                            </div>
                        </form>
                    </div>

                </div><!--/col-->
                <div class="col-sm-12 col-md-4">
                    <div class="widget widget-contact">
                        <h3>Our Office Locations</h3>
                        <ul class="about-info-listed about-info-listed_v2">
                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                9311 Sherman Ave. Groton,  CT 06340
                            </li>
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <a href="tel:+00123456789">+00 123 456 789</a>
                            </li>
                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <a href="mailto:info@myvoice.com">info@demo.com</a>
                            </li>
                            <li>
                                <i class="fa fa-skype" aria-hidden="true"></i>
                                <a href="tel:+00123456789">+00 123 456 987</a>
                            </li>
                            <li>
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                <a href="#">www.mydemo.com</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
