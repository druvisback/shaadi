@extends('layouts.member')

@section('body-content')
<div class="top_bg_v2">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-sm-12 col-md-12">
                <div class="match_guarantee">
                    <h2>Find the Perfect plan for you with <span>Match Guarantee</span> </h2>
                    <span data-test-selector="tooltip" id="match_guarantee_tooltip"><span class="sc-gisBJw fryNYY"></span></span>
                </div>
                <div class="match_guarantee_divider"></div>
                <div class="offer_wrapper">
                    <h3>Upgrade to any of our Premium Plans and we guarantee you will find a match!</h3>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="pricingTable">
                            <div class="pricingTable-header">
                                    <span class="heading">
                                        <h3>Standard</h3>
                                    </span>
                                <span class="price-value"><span class="currency">$</span>10<span class="mo"> month</span></span>
                            </div>

                            <div class="pricingContent">
                                <ul>
                                    <li>50GB Disk Space</li>
                                    <li>50 Email Accounts</li>
                                    <li>50GB Monthly Bandwidth</li>
                                    <li>50 Domains</li>
                                    <li>Unlimited Subdomains</li>
                                </ul>
                            </div><!-- /  CONTENT BOX-->

                            <div class="pricingTable-sign-up">
                                <a href="#" class="btn btn-block btn-default">sign up</a>
                            </div><!-- BUTTON BOX-->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricingTable">
                            <div class="pricingTable-header">
                                    <span class="heading">
                                        <h3>Standard</h3>
                                    </span>
                                <span class="price-value"><span class="currency">$</span>10<span class="mo"> month</span></span>
                            </div>

                            <div class="pricingContent">
                                <ul>
                                    <li>50GB Disk Space</li>
                                    <li>50 Email Accounts</li>
                                    <li>50GB Monthly Bandwidth</li>
                                    <li>50 Domains</li>
                                    <li>Unlimited Subdomains</li>
                                </ul>
                            </div><!-- /  CONTENT BOX-->

                            <div class="pricingTable-sign-up">
                                <a href="#" class="btn btn-block btn-default">sign up</a>
                            </div><!-- BUTTON BOX-->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricingTable">
                            <div class="pricingTable-header">
                                    <span class="heading">
                                        <h3>Standard</h3>
                                    </span>
                                <span class="price-value"><span class="currency">$</span>10<span class="mo"> month</span></span>
                            </div>

                            <div class="pricingContent">
                                <ul>
                                    <li>50GB Disk Space</li>
                                    <li>50 Email Accounts</li>
                                    <li>50GB Monthly Bandwidth</li>
                                    <li>50 Domains</li>
                                    <li>Unlimited Subdomains</li>
                                </ul>
                            </div><!-- /  CONTENT BOX-->

                            <div class="pricingTable-sign-up">
                                <a href="#" class="btn btn-block btn-default">sign up</a>
                            </div><!-- BUTTON BOX-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  <!--/.col-->
</div>
@stop
