@extends('pages.layout')

@section('page-content')
<div class="row featurette">
    <div class="col-md-12">
        <h2 class="featurette-heading" style="padding: 15px 0px;">Happy <span class="text-muted">Story.</span></h2>
        <p class="lead">
            {{$story_content}}
        </p>
    </div>
</div>
@stop
