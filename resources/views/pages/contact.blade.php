@extends('layouts.simple')

@section('content')
<div class="container" style="margin: 61px;">
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="row mb-3">
                <div class="col-md-12 text-center">
                    <h4 class="title divider-3 text-capitalize text-center mb-5">Get in Touch</h4>
                    <p class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <div class="address-block fill">
                        <h3 class="title text-capitalize">Address :</h3>
                        <address>
                            T317 Timber Oak Drive <br/>
                            Sundown, TX 79372 <br/>
                            Sundown  - 12345
                        </address>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="address-block fill">
                        <h3 class="title text-capitalize">Email Address:</h3>
                        <a href="#">info@laganutsav.com</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="address-block fill">
                        <h3 class="title text-capitalize">Phone :</h3>
                        <span>Landline : <a href="tel:+911234567890">+91-123-456-7890</a></span> <span>Mobile : <a href="tel:+912345678900">+91-234-567-8900</a></span> </div>
                </div>
                <div class="col-md-6">
                    <div class="address-block fill">
                        <h3 class="title text-capitalize">Social Media :</h3>
                        <div class="social-icons color-hover">
                            <ul>
                                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="social-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 mb-5 sm-mb-3">
            <div class="row mb-3">
                <div class="col-md-12 text-center">
                    <h4 class="title divider-3 text-capitalize text-center mb-3">contact form</h4>
                </div>
            </div>
            <div class="defoult-form">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>{{Session::get('success')}}</strong>
                    </div>
                @endif
                <form id="contactform" method="post" action="{{route('page.contact')}}">
                    @csrf()
                    <div class="form-group">
                        <input name="full_name" class="form-control" type="text" placeholder="Name" value="{{old('full_name')}}"/>

                    </div>
                    <div class="form-group">

                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{old('email')}}"/>

                    </div>

                    <div class="form-group">
                        <div class="input-group">

                            <textarea class="form-control input-message" placeholder="Comment*" rows="5" name="comment">{{old('comment')}}</textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <button id="submit" name="submit" type="submit" value="Send" class="button btn-lg btn-theme full-rounded animated right-icn"><span>Submit Now<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-md-12 contact-iframe">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d448181.16374162206!2d76.81306442366603!3d28.647279935570428!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfd5b347eb62d%3A0x37205b715389640!2sDelhi!5e0!3m2!1sen!2sin!4v1559303597545!5m2!1sen!2sin" frameborder="0" style="border:0; width: 100%;height: 360px;" allowfullscreen></iframe>
        </div>
    </div>
@stop
