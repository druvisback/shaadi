@extends('layouts.simple')

@section('content')
    <div class="container" style="margin: 61px;">
        <div class="row justify-content-center mb-5 sm-mb-3">
            <div class="col-md-8 text-center">
                <h2 class="title title3 divider wow fadeInUp animated mb-3">Frequently Asked Questions</h2>
                <p class="lead">Have a question? Please check our knowledgebase first.</p>
            </div>
        </div>


        @foreach($faqs as $faq)

        <div class="row">
            <div class="col-md-12">
                <div class="accordion arrow light-rounded sm-mb-0">
                    <div class="acd-group acd-active"><a href="#" class="acd-heading">{{$faq->title}}</a>
                        <div class="acd-des">
                            <h5 class="title mb-2">{{$faq->question}}</h5>
                            <p>{{$faq->answer}}</p>

                        </div>
                    </div>


            </div>
        </div>
    </div>


@endforeach
    </div>
@stop

