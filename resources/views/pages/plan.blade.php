@extends('layouts.member')

@section('body-content')

<style>

    /**** pricing 2 *******/
    .pricing-section2{
        background: #c7c7c7;
        padding: 50px 0;
    }
    .pricing-section2 ul {
        width: 1000px;
        margin: auto;
    }
    .pricing-section2 ul li {
        display: table-cell;
        width: 20%;
    }
    .pricing-section2 ul li:nth-child(1) {
        width: 25%
    }
    .pricing-section2 ul li .card-title {
        margin: 0.5rem 0;
        font-size: 1.3rem;
        letter-spacing: .1rem;
        font-weight: bold;
        color: #ee0c6b;
        padding-top: 35px;
    }
    .pricing-section2 ul li .card-price {
        font-size: 2.6rem;
        margin: 0;
        font-weight: 600;
    }
    .pricing-section2 ul li .card .card-body {
        border: 1px solid #f1ecec;
        padding: 0;
    }
    .pricing-section2 ul li .card .card-body ul.list {
        width: auto;
        margin: 0;
    }
    .pricing-section2 ul li .card .card-body ul.list li {
        display: grid;
        text-align: center;
        width: auto;
    }
    .pricing-section2 ul li:nth-child(1) .card .card-body {
        padding: 1.5px 0;
    }
    .pricing-section2 ul li:nth-child(1) .card {
        border-radius: 20px 0 0 20px;
    }
    .pricing-section2 ul li:nth-child(4) .card {
        border-radius: 0 20px 20px 0;
    }
    .pricing-section2 ul li:nth-child(3) .card {
        border-radius: 20px 20px 0 0;
    }
    .pricing-section2 ul li:nth-child(3) .card .card-body {
        padding: 25px 0 0;
    }
    .pricing-section2 ul li .card .card-body ul.list li:nth-child(odd){
        background: #f7f7f7;
    }
    .pricing-section2 ul li .card .card-body ul.list {
        padding: 40px 0 0;
    }
    .pricing-section2 ul li .card .card-body ul.list li {
        padding: 20px;
    }
    .pricing-section2 ul li .card .card-body img {
        width: 25px;
        margin: auto;
    }
    .pricing-section2 ul li:nth-child(1) .card-title {
        color: #0c86ee;
    }
    .pricing-section2 ul li:nth-child(1) .card-price {
        color: #0c86ee;
    }

    @media (max-width: 991px){
        .pricing-section2 ul {
            width: auto;
        }
    }
    @media (min-width: 768px) and (max-width: 991px){
        .pricing-section2 ul li:nth-child(1) {
            width: 34%;
        }
        .pricing-section2 ul li .card-price {
            font-size: 1.8rem;
        }
        .pricing-section2 ul li .card-title {
            padding-top: 26px;
        }
        .pricing-section2 ul li .card .card-body ul.list {
            padding: 32px 0 0;
        }
        .pricing-section2 ul li .card .card-body ul.list li {
            font-size: 12px;
        }
    }
    @media (max-width: 767px){
        .pricing-section2 ul li {
            display: contents;
        }
        .pricing-section2 ul li .card{
            border-radius: 20px!important;
        }
    }

</style>

<section>



    <div class="top_bg_v2">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-sm-12 col-md-12">
                    <div class="match_guarantee">
                        <h2>Find the Perfect plan for you with <span>Match Guarantee</span> </h2>
                        <span data-test-selector="tooltip" id="match_guarantee_tooltip"><span class="sc-gisBJw fryNYY"></span></span>
                    </div>
                    <div class="match_guarantee_divider"></div>
                    <div class="offer_wrapper">
                        <h3>Upgrade to any of our Premium Plans and we guarantee you will find a match!</h3>
                    </div>


                    <div class="pricing-section2">

        <div class="container-fluid">

        <form action="{{route('UserSubscription')}}"  method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="hidden" name="user_id" value="{{$user_id}}"/>
    <ul>


        @foreach($plans as $plan)
            <li>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-center">{{$plan->plan_name}}</h5>
                        <h6 class="card-price text-center">{{$plan->price}}</h6>
                        <ul class="list">
                            <li>
                                Plan Description  {{$plan->p_des}}
                            </li>

                            <li>
                                {{$plan->month}} months
                            </li>

                            <li>
                                Daily Message Limit {{$plan->d_mess}}
                            </li>
                            <li>
                                Daily Profile Visit Limit {{$plan->d_prof}}
                            </li>

                            <li>
                               Plan Price : {{$plan->price}}
                            </li>
                            <li>
                                <input type="radio" value="{{$plan->id}}" name="plan" >Select Plan
                            </li>

                        </ul>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
    <div class="form-group">
        <div class="col-md-12" style="text-align: center">
            <button type="submit" class="btn btn-sm btn-success">Submit</button>
        </div>
    </div>
</form>

</div>
</div>
</div>
</div>
</div>
</div>
</div>

    </section>

@stop
