@extends('pages.layout')

@section('page-content')
<div class="row featurette">
    <div class="col-md-12">
        <h2 class="featurette-heading" style="padding: 15px 0px;">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
        <p class="lead">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus neque imperdiet nisi porta commodo. Suspendisse pharetra, augue laoreet ultrices pulvinar, enim felis viverra elit, et egestas ex erat sed massa. Maecenas sed mattis enim, vel dapibus ante. Etiam auctor ut enim volutpat malesuada. Aliquam erat volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi accumsan, justo nec porttitor luctus, mauris elit sodales dolor, vitae consectetur urna elit eget nulla. Vestibulum id viverra nisl. Ut egestas scelerisque neque, quis malesuada dui. Nullam est velit, vulputate a nibh a, pulvinar viverra ex. Nam eget finibus magna. Praesent placerat, augue quis fermentum volutpat, sem quam consequat turpis, eu iaculis libero odio non neque.

            Vestibulum feugiat ante nec tortor faucibus, eu sagittis massa accumsan. Maecenas turpis felis, pellentesque ut ornare ac, auctor ut est. Ut non libero aliquam nulla scelerisque varius. Proin porta odio eget ipsum tristique, pellentesque viverra lectus dictum. Curabitur vitae magna ultricies, sagittis eros vitae, facilisis elit. Proin felis nunc, suscipit in neque at, tincidunt bibendum mi. Sed in porta est, sed auctor nunc. Mauris a lectus nec erat interdum sodales nec ut mi. Vestibulum eu risus mattis lorem fringilla pharetra a sed sem. Duis pharetra imperdiet nulla.

            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur tellus lorem, scelerisque nec quam sed, pulvinar facilisis quam. Suspendisse ultrices nec nulla ac semper. Donec faucibus lorem vitae arcu cursus euismod. Vivamus pulvinar nisl ac pellentesque rutrum. Fusce urna dui, vulputate sed sapien nec, euismod molestie massa. Proin congue sollicitudin sem eu scelerisque.

            Curabitur pulvinar nibh eget ligula posuere, id pulvinar felis pellentesque. Suspendisse in eros sit amet enim vulputate facilisis sit amet eu tortor. Proin a mattis urna, non varius leo. Nulla fermentum et ligula quis commodo. Praesent vitae viverra turpis. Sed dapibus, ipsum in tempor efficitur, enim risus pellentesque neque, a vulputate nunc lorem a justo. Aliquam placerat justo a convallis consectetur. Nulla eleifend ut justo a placerat. Sed at leo viverra, facilisis tellus a, euismod orci. Ut nec gravida diam, ut ullamcorper risus. Quisque ut imperdiet quam, quis aliquam diam. Aliquam sodales leo quis elit dignissim, in viverra turpis maximus. Etiam semper neque sit amet tellus suscipit malesuada. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas sapien leo, interdum eu felis ac, malesuada posuere odio.

            Cras laoreet nisi nec consectetur consequat. Vestibulum luctus ligula sapien, ut euismod lacus porttitor ac. Nam ex felis, eleifend eget tellus viverra, sagittis interdum libero. Ut rhoncus eget ipsum in sodales. In pretium fermentum libero, laoreet vulputate orci porttitor in. Praesent efficitur metus eget augue dignissim vehicula. Suspendisse et egestas odio, a gravida felis. In quis nibh vitae libero facilisis pulvinar. Nulla facilisi. Curabitur non odio eu lorem pellentesque mattis eu nec dolor. Vivamus laoreet lacus leo, quis iaculis nulla consequat a. Pellentesque scelerisque tortor cursus diam feugiat, posuere imperdiet magna auctor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In hac habitasse platea dictumst. Suspendisse potenti.
        </p>
    </div>
</div>
@stop
