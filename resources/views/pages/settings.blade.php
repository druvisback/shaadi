@extends('layouts.member-simple')

@section('body-content')

<section class="page-section-ptb dashboard">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="account-settings">
                    <h4 class="divider-3">Account Settings</h4>

                    <div class="settings-block">
                        <div class="setting-single">
                            <h5>hide or mute password for 5 to infinite days</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi doloremque laudantium natus necessitatibus optio quisquam. Asperiores, molestias obcaecati.
                            </p>
                            <span>
                                <button type="button" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
                                     <div class="handle"></div>
                                </button>
                            </span>
                        </div>


                        <?php $user = Auth::user() ?>

                 <div class="setting-single">
                     <h5>Delete Account</h5>
                     <p>
                         Delete Your Acount Permanently...
                     </p>
                     <span>
                          <a href="#!" class="btn-edit" id="edit-password" style="display: inline-block;">Edit</a>
                         <a href="#!" class="btn-edit" id="cancel-password" style="display: none;"><i class="fa fa-times"></i></a>
                     </span>
                     <form action="{{route('cpanel.member.delete-profile',$user->password)}}"class="hidden-block" id="form-settings" style="display: none;">
                         <div class="form-group">
                             <input type="password" name="password" class="form-control" placeholder="Password">
                             <button class="button btn-theme full-rounded">Delete</button>
                         </div>
                     </form>
                 </div>



                        <div class="setting-single">
                            <h5>Password change with old password</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi doloremque laudantium natus necessitatibus optio quisquam. Asperiores, molestias obcaecati.
                            </p>
                            <span>
                                 <a href="#!" class="btn-edit" id="edit-password" style="display: inline-block;">Edit</a>
                                <a href="#!" class="btn-edit" id="cancel-password" style="display: none;"><i class="fa fa-times"></i></a>
                            </span>
                            <form class="hidden-block" action="{{route('change-password')}}" method="post" id="form-settings" style="display: none;">
                              @csrf
                                <div class="form-group">
                                    <input type="password" class="form-control" name="old_pass" placeholder="Old Password">
                                    <input type="password" class="form-control" value="{{ old('new_password','') }}"  name="new_password" placeholder="New Password">
                                    <input type="password" class="form-control" value="{{ old('cnf_password','') }}" name="cnf_password" placeholder="Confirm New Password">
                                    <button class="button btn-theme full-rounded">Save</button>
                                </div>
                            </form>
                        </div>



                        <div class="setting-single">
                            <h5>number point</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi doloremque laudantium natus necessitatibus optio quisquam. Asperiores, molestias obcaecati.
                            </p>
                            <span>
                                <button type="button" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
                                     <div class="handle"></div>
                                </button>
                            </span>
                        </div>

                        
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

@stop
