<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script>
    $(document).ready(function(){
        document.getElementById("paypalForm").submit();
    });
</script>
<?php

$en = \Illuminate\Support\Facades\Crypt::encrypt($order_id);


?>
<form name="paypalForm" id="paypalForm" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="cmd" value="_xclick-subscriptions">
    <input type="hidden" name="item_name" value="{{$order_id}}">
    <input type="hidden" name="business" value="info-facilitator@gowebbi.com">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" value="2" name="rm">
    <input type="hidden" name="return" value="{{route('recurring_paypal',$order_id)}}">
    <input type="hidden" name="cancel_return" value="">
    <input type="hidden" name="a3" value="{{$amount}}">
    <input type="hidden" name="p3" value="1">

    <input type="hidden" name="t3" value="M">
    <input type="hidden" name="src" value="1">
    <input type="hidden" name="sra" value="1">
    <input type="hidden" name="srt" value="{{$month}}>
    <input type="hidden" name="item_number" value="">
    <input type="hidden" name="notify_url" value="">
    <input type="hidden" name="quantity" value="1">
    <input type="hidden" name="invoice" value="{{$order_id.'|'.rand(9,9999)}}">
    <input type="hidden" name="cbt" value="Please click here to confirm your Plan">
    <input type="hidden" name="country" value="GB">
</form>
