@extends('layouts.member')

@section('body-content')
<style>
    .page.card{
        width: 100%;
        height: 200px;
        background: url('{{$head_img_url}}') center no-repeat;
        background-size: cover;
        justify-content: center;
        align-items: center;
        display: flex;
    }
    .card p.page-title{
        color: white;
        font-size: 39px;
    }
</style>
<section class="dashboard">
    <div class="page card">
        <p class="page-title">{{$page_title}}</p>
    </div>
    <div class="container" style="padding: 23px 0px;">
        <div class="row justify-content-center">
            @yield('page-content')
        </div>
    </div>

</section>
@stop
