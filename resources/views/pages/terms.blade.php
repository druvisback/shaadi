@extends('pages.layout')

@section('page-content')
<div class="row featurette">
  @foreach($terms as $s)
  <div class="col-md-12">
      <h2 class="featurette-heading" style="padding: 15px 0px;">{{$s->heading}}</h2>
      <p class="lead">
        {{$s->contain}}
      </p>
  </div>
  @endforeach
</div>
@stop
