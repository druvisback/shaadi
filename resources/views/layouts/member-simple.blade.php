@include('layouts.header-member-simple')

<?php
if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET) && array_key_exists('member_validation', $_GET)):
    $member_validation = $_GET['member_validation'];
    if(!$member_validation['success']):
?>
<div class="alert alert-danger" role="alert">
    {!! $member_validation['message'] !!}
</div>
<?php endif; endif; ?>


@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{!! $error !!}</li>
        @endforeach
    </ul>
</div>
@elseif(\Session::has('success'))
<div class="alert alert-success">
    <ul>
        <li>{!! \Session::get('success') !!}</li>
    </ul>
</div>
@elseif(\Session::has('error'))
<div class="alert alert-danger">
    <ul>
        <li>{!! \Session::get('error') !!}</li>
    </ul>
</div>
@elseif(isset($error))
<div class="alert alert-danger">
    <ul>
        <li>{!! $error !!}</li>
    </ul>
</div>
@endif

@yield('body-content')


@include('layouts.footer')
