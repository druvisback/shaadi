@extends('layouts.app')

@section('body-content')
<div class="login-form" style="background: url({{ ExtraController::assetURL('images/login-full-bg.png') }}) center no-repeat; background-size: contain;">
    <div class="container">
        @if(!$errors->isEmpty())
            <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <ul>
                    <li>{{ $error }}</li>
                </ul>
            @endforeach
            </div>
        @endif
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @yield('content')
    </div>
</div>
@endsection
