@include('layouts.common-head')
<body>

<header id="header" class="fancy">
    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav id="menu" class="mega-menu">
                        <section class="menu-list-items">
                            <ul class="menu-logo">
                                <li><a href="{{ route('front-page') }}"><img id="logo_color_img" src="{{ ExtraController::assetURL('images/dark-logo.png') }}" alt="logo" /> </a></li>
                            </ul>
                            <ul class="menu-links">
                                @guest
                                    <li class="nav-item{{ Request::is('/*') ? ' active' : '' }}">
                                        <a href="{{ route('front-page') }}">{{ __('Search') }}</a>
                                    </li>
                                    @if (Route::has('login'))
                                    <li class="nav-item{{ Request::is('login*') ? ' active' : '' }}">
                                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                    @endif
                                    @if (Route::has('register'))
                                    <li class="nav-item{{ Request::is('register*') ? ' active' : '' }}">
                                        <a href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                    @endif
                                @else
                                <li class="nav-item">
                                    <a href="{{ route('cpanel') }}">{{ __('Dashboard') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                @endguest
                            </ul>
                        </section>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
