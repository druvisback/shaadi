@include('layouts.common-head')

<body>

<link href="{{ ExtraController::assetURL('css/member-style.css') }}" rel="stylesheet" type="text/css" />
<header id="header" class="fancy">

    <!--=================================
   mega menu -->
    <div class="menu header-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- menu start -->
                    <nav id="menu" class="mega-menu">
                        <!-- menu list items container -->
                        <section class="menu-list-items">
                            <!-- menu logo -->
                            <ul class="menu-logo">
                                <li><a href="{{ route('front-page') }}"><img id="logo_color_img" src="{{ ExtraController::assetURL('images/dark-logo.png') }}" alt="logo" /> </a></li>
                            </ul>
                            <!-- menu links -->
                            <ul class="menu-links">
                                <li class="{{ Request::is('/*') ? 'active' : '' }}"><a href="{{route('front-page')}}"> Home </a></li>
                                @if(Auth::check())
                                    <li class="{{ Request::is('dashboard/member*') && !Request::is('dashboard/member/privacy-settings*') ? 'active' : '' }}"><a href="{{route('cpanel')}}"> Dashboard </a></li>
                                @else
                                <li><a href="{{route('register')}}">Became a Member</a></li>
                                @endif
                                <li class="{{ Request::is('search*') ? 'active' : '' }}"><a href="{{route('search')}}"> Matches & Search </a></li>
                                @if(Auth::check())
                                <li><a href="{{route('UserPlanRegistration',5)}}"> Upgrade Now </a></li>
                                @endif
                                <li class="{{ Request::is('help*') ? 'active' : '' }}"><a href="{{route('page.help')}}"> Help </a></li>
                                @if(Auth::check())
                                <!--<li class="{{ Request::is('/logout*') ? 'active' : '' }}"><a href="{{route('logout')}}"> Logout </a></li>-->
                                <li class="{{ Request::is('dashboard/my-account*') ? 'active' : '' }}">
                                    <a href="#"> My Account <i class="fa fa-angle-down fa-indicator"></i></a>
                                    <ul class="drop-down-multilevel right-menu">
                                        <li class="{{ Request::is('dashboard/my-account/current-package*') ? 'active' : '' }}"><a href="{{ route('cpanel.account.package') }}">Current Package</a></li>
                                        <li class="{{ Request::is('dashboard/my-account/privacy*') ? 'active' : '' }}"><a href="{{route('cpanel.account.privacy')}}">Account settings</a></li>
                                        <li><a href="#">Interaction</a></li>
                                        <!--<li><a href="#">Email/SMS Alert</a></li>
                                        <li><a href="#">Privacy option</a></li>-->
                                        <li><a href="{{route('logout')}}">Logout</a></li>
                                    </ul>
                                </li>
                                @endif
                            </ul>
                        </section>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <?php if(!(isset($bottom_header) && $bottom_header == false)): ?>
    <div class="bottom-header">
        @if(Request::is('member/matches*'))
        <ul class="menu-bottom-links">
            <li class="{{ Request::is('member/matches*') ? 'active' : '' }}"><a href="{{ route('cpanel.member.profile-setup') }}"> Matches </a></li>
            <li class=""><a href="#"> Searches </a></li>
            <li class=""><a href="#"> Recently Viewed Profiles </a></li>sss
        </ul>
        @elseif(Request::is('dashboard/member*'))
        <ul class="menu-bottom-links">
            <li class="{{ Request::is('dashboard/member/profile*') ? 'active' : '' }}"><a href="{{ route('cpanel.member.profile-setup') }}"> Profile </a></li>
            <li class="{{ Request::is('dashboard/member/photos*') ? 'active' : '' }}"><a href="{{ route('cpanel.member.photo-setup') }}"> Photos </a></li>
            <li class="{{ Request::is('dashboard/member/preference*') ? 'active' : '' }}"><a href="{{ route('cpanel.member.preference-setup') }}"> Match Preferences </a></li>
            <?php
            $user = \Illuminate\Support\Facades\Auth::user();
            $profile = \App\model\Profile::where('ref_user', $user->id)->first();
            $kyc = \App\model\Kyc::where('ref_profile_id', $profile->id)->first();

            ?>
            @if(!($kyc && $kyc->is_check && $kyc->is_valid))
                <li class="{{ Request::is('dashboard/member/submit-docs*') ? 'active' : '' }}"><a href="{{ route('cpanel.member.submit-docs') }}">Profile Verification & Validation (KYC)</a></li>
            @else
            <li class="bg-success {{ Request::is('dashboard/member/submit-docs*') ? 'active' : '' }}">
                Profile Verified
            </li>
            @endif
        </ul>
        @endif
    </div>
    <?php endif; ?>
</header>
