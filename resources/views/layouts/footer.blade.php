

<!--=================================
footer -->
<footer class="text-white text-center">
    <div class="footer-widget sm-mt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img src="{{ ExtraController::assetURL('images/dark-logo.png') }}" class="img-fluid" alt="logo" id="footer-logo">
                </div>
                <div class="col-md-3">
                    <h6>Main Menu</h6>
                    <ul>
                        <li class="{{ Request::is('/*') ? 'active' : '' }}"><a href="{{ route('front-page') }}">Home</a></li>
                        <li><a href="{{ route('page.premium-plans') }}">Premium Plans</a></li>
                        <li><a href="{{ route('page.happy-story') }}">Happy Stories</a></li>
                        <li><a href="{{ route('page.contact') }}">Contact us</a></li>
                    </ul>

                </div>
                <div class="col-md-3">
                    <h6>Quick search</h6>
                    <ul>
                        <li><a href="{{ route('search') }}">Quick Search</a></li>
                        <li><a href="#">Premium Members</a></li>
                        <li><a href="#">Free Members</a></li>
                    </ul>

                </div>
                <div class="col-md-3">
                    <h6>useful links</h6>
                    <ul>
                        <li><a href="{{ route('page.faqs') }}">FAQ</a></li>
                        <li><a href="{{ route('page.terms') }}">Terms & Conditions</a></li>
                        <li><a href="{{ route('page.policy') }}">Privacy Policy</a></li>
                    </ul>

                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <p class="text-white">© 2019  - LaganUtsav | All Right Reserved </p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!--=================================
footer -->


<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-level-up"></i></a></div>

<!--=================================
 jquery -->

<!-- jquery  -->

<script type="text/javascript" src="{{ ExtraController::assetURL('js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ ExtraController::assetURL('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ ExtraController::assetURL('js/jquery.appear.js') }}"></script>
<script type="text/javascript" src="{{ ExtraController::assetURL('js/mega-menu/mega_menu.js') }}"></script>
<script type="text/javascript" src="{{ ExtraController::assetURL('js/owl-carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ ExtraController::assetURL('js/custom.js') }}"></script>
<script type="text/javascript" src="{{ ExtraController::assetURL('js/wow.min.js') }}"></script>
<script>
    var wow = new WOW(
        {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       true,       // trigger animations on mobile devices (default is true)
            live:         true,       // act on asynchronously loaded content (default is true)
            callback:     function(box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null,    // optional scroll container selector, otherwise use window,
            resetAnimation: true,     // reset animation on end (default is true)
        }
    );
    wow.init();
</script>

</body>

</html>
