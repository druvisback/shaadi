@include('layouts.header')


<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url({{ ExtraController::assetURL('images/bg/inner-bg-1.jpg') }});">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-md-12 mt-7">
                <ul class="page-breadcrumb">
                    <li><a href="/"><i class="fa fa-home"></i>Home</a> <i class="fa fa-angle-double-right"></i></li>
                    <li><span>{{(isset($page_title))?$page_title:'No Page'}}</span> </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="faq-page">
<div class="container-fluid" style="padding: 0px;">
    @if(!$errors->isEmpty())
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $error)
        <ul>
            <li>{{ $error }}</li>
        </ul>
        @endforeach
    </div>
    @endif
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    @yield('content')
</div>
</section>
@include('layouts.footer')
