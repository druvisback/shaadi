@include('layouts.header')
<!--=================================
 inner-intro-->

<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url({{ ExtraController::assetURL('images/bg/inner-bg-1.jpg') }});">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-md-12 mt-7">
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('front-page') }}"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                    <li><span>Login</span> </li>
                </ul>
            </div>
        </div>
    </div>
</section>


@yield('body-content')


@include('layouts.footer')
