<!DOCTYPE html>
<html lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ ExtraController::assetURL('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ ExtraController::assetURL('css/mega-menu/mega_menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ ExtraController::assetURL('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ ExtraController::assetURL('css/flaticon.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ ExtraController::assetURL('css/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ ExtraController::assetURL('css/animate.css') }}" rel="stylesheet" type="text/css" />


    <link href="{{ ExtraController::assetURL('css/general.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ ExtraController::assetURL('css/style.css') }}" rel="stylesheet" type="text/css" />

    <!-- Style customizer -->
    <!--<link rel="stylesheet" type="text/css" href="css/skins/skin-default.css" data-style="styles" />-->
    <script type="text/javascript" src="{{ ExtraController::assetURL('js/jquery.min.js') }}"></script>
</head>
