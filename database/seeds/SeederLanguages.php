<?php

use Illuminate\Database\Seeder;

class SeederLanguages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('languages')->insert([
            ['name' => 'Bengali'],
            ['name' => 'Hindi'],
            ['name' => 'English'],
            ['name' => 'Tamil']
        ]);
    }
}
