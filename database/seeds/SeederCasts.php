<?php

use Illuminate\Database\Seeder;

class SeederCasts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('casts')->insert([
            ['name' => 'Sample 1', 'ref_religion' => 1],
            ['name' => 'Sample 2', 'ref_religion' => 1],
            ['name' => 'Sample 3', 'ref_religion' => 1],
            ['name' => 'Sample 4', 'ref_religion' => 2],
            ['name' => 'Sample 5', 'ref_religion' => 2]
        ]);
    }
}
