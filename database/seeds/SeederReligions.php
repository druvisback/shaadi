<?php

use Illuminate\Database\Seeder;

class SeederReligions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('religions')->insert([
            ['name' => 'Hindu'],
            ['name' => 'Muslim'],
            ['name' => 'Sikh'],
            ['name' => 'Buddhist'],
            ['name' => 'Jain']
        ]);
    }
}
