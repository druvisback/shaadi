<?php

use Illuminate\Database\Seeder;

class SeederCountries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('countries')->insert([
            ['name' => 'India'],
            ['name' => 'Nepal'],
            ['name' => 'Bangladesh']
        ]);
    }
}
