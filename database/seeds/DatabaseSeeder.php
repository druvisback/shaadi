<?php

use Illuminate\Database\Seeder;
use \SeederCountries;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){
        $this->call([
            SeederCountries::class
        ]);
        /*$this->call(SeederLanguages::class);
        $this->call(SeederReligions::class);
        $this->call(SeederCasts::class);*/
    }
}
