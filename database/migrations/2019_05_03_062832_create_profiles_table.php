<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');

            //Ref Ids...
            $table->bigInteger('ref_user')->unsigned();
            $table->bigInteger('ref_religion')->unsigned()->nullable();
            $table->bigInteger('ref_cast')->unsigned()->nullable();
            $table->bigInteger('ref_country')->unsigned()->nullable();
            $table->bigInteger('ref_language')->unsigned()->nullable();
            $table->bigInteger('ref_current_order')->unsigned()->nullable();

            $table->string('first_name');
            $table->string('last_name');
            $table->enum('gender',['male','female','other']);
            $table->enum('profile_for',['me','father','mother','son','daughter','sister', 'brother','relative']);

            //Profile Validations ...
            $table->boolean('is_accept_terms')->default(false);
            $table->boolean('is_complete_profile')->default(false);
            $table->boolean('is_allow_to_search')->default(false);
            $table->boolean('is_block')->default(true);
            $table->boolean('is_mail_verified')->default(false);
            $table->string('err_message')->default("Incomplete Profile");
            $table->enum('status', array('Active', 'Inactive'))->default('Inactive')->index();
            $table->enum('user_block', array('Blocked', 'Unblocked'))->default('Unblocked')->index();
            $table->timestamps();
        });

        Schema::table('profiles', function($table) {
            $table->foreign('ref_user')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('ref_religion')
                ->references('id')->on('religions')
                ->onDelete('cascade');
            $table->foreign('ref_cast')
                ->references('id')->on('casts')
                ->onDelete('cascade');
            $table->foreign('ref_country')
                ->references('id')->on('countries')
                ->onDelete('cascade');
            $table->foreign('ref_language')
                ->references('id')->on('languages')
                ->onDelete('cascade');
            $table->foreign('ref_current_order')
                ->references('id')->on('orders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('profiles');
    }
}
