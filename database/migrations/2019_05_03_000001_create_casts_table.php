<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            //Ref Ids...
            $table->bigInteger('ref_religion')->unsigned();

            $table->timestamps();
        });

        Schema::table('casts', function($table) {
            $table->foreign('ref_religion')
                ->references('id')->on('religions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('casts', function($table) {

        });
        Schema::dropIfExists('casts');
    }
}
