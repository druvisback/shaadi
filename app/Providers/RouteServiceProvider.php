<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider{

    protected $namespace = 'App\Http\Controllers';

    public function boot(){
        parent::boot();
    }

    public function map(){
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    protected function mapWebRoutes(){
        //Admin Routes...
        Route::middleware(['web','auth','omg-user:admin'])
             ->namespace($this->namespace)
             ->name('admin')
             ->prefix('admin')
             ->group(base_path('routes/web/admin-routes.php'));

        //Member Routes...
        Route::middleware('web')
            ->namespace($this->namespace)
            ->name('member')
            ->prefix('member')
            ->group(base_path('routes/web/member-routes.php'));

        //Developer Routes...
        Route::middleware('web')
            ->namespace($this->namespace)
            ->name('developer')
            ->prefix('developer')
            ->group(base_path('routes/web/developer-routes.php'));

        //Routes...
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web/routes.php'));
    }

    protected function mapApiRoutes(){
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
