<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Kyc extends Model{
    protected $table = "kycs";
    protected $guarded = [];
}
