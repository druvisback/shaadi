<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class MatchPreference extends Model{
    protected $guarded = ['id'];
}
