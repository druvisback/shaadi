<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model{
    protected $table = 'custom_fields';
    protected $guarded = [];

    public function setValueAttribute($val){
        $this->attributes['value'] = serialize([$val]);
    }

    public function getValueAttribute($val){
        $response = false;
        if($val):
            $data = unserialize($val);
            if(is_array($data) && !empty($data)):
                $response = $data[0];
            endif;
        endif;
        return $response;
    }
}
