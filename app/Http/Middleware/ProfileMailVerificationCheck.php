<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Member\MemberController;
use App\model\Profile;
use Closure;
use Illuminate\Support\Facades\Auth;

class ProfileMailVerificationCheck{

    public function checkUserType($uType){
        $current_user = Auth::user();
        return ($current_user->type) == $uType;
    }

    public function doAbort($code = 404){
        return abort($code);
    }

    public function handle($request, Closure $next){
        $to_CurrentResponse = $next($request);
        $response = $to_CurrentResponse;

        if($this->checkUserType('member')):
            $member_user = Auth::user();
            $member_profile = Profile::where('ref_user', $member_user->id)->first();
            if($member_user && $member_profile):
                if($member_profile && !$member_profile->is_mail_verified):
                    $mail_verify_timestamp = $member_user->email_verified_at;
                    if($mail_verify_timestamp):
                        $member_profile->is_mail_verified = true;
                        $member_profile->save();
                    endif;
                endif;
            else:
                $member_user->delete();
            endif;
        endif;

        if($response):
            return $response;
        else:
            return $this->doAbort();
        endif;
    }
}
