<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Member\MemberController;
use Closure;
use Illuminate\Support\Facades\Auth;

class OMGCheckUser{

    public function checkUserType($uType){
        $current_user = Auth::user();
        return ($current_user->type) == $uType;
    }

    public function doAbort($code = 404){
        return abort($code);
    }

    public function handle($request, Closure $next, $action = false){
        $to_CurrentResponse = $next($request);
        $to_AdminDashboard = redirect(route('cpanel.admin.dashboard'));
        $to_MemberDashboard = redirect(route('cpanel.member.check'));

        $response = false;

        switch($action):
            case 'admin':
                if($this->checkUserType('admin')):
                    $response = $to_CurrentResponse;
                endif;
                break;
            case 'member':
                if($this->checkUserType('member')):
                    $response = $to_CurrentResponse;
                endif;
                break;
            case 'check':
                if($this->checkUserType('admin')):
                    $response = $to_AdminDashboard;
                elseif($this->checkUserType('member')):
                    $response = $to_MemberDashboard;
                endif;
                break;
            case 'check-member':
                $checkMember = MemberController::checkProfileCompletion();

                if(!$checkMember['success']):
                    $data = ['member_validation' => $checkMember];
                    if($checkMember['redirect']):
                        return redirect()->route($checkMember['redirect'], $data);
                    else:
                        return $next($request);
                    endif;
                else:
                    $response = redirect()->route($checkMember['redirect']);
                endif;
                break;
        endswitch;


        if($response):
            return $response;
        else:
            return $this->doAbort();
        endif;
    }
}
