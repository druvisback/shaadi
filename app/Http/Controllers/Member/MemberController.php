<?php

namespace App\Http\Controllers\Member;

use App\model\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller{

    public static function checkProfileCompletion(){
        $currentUser = Auth::user();
        $currentProfile = Profile::where('ref_user',$currentUser->id)->first();

        $error_message = "";
        $redirect_to = "cpanel.member.profile-setup";
        $success = false;
        if(!$currentProfile->is_accept_terms):
            $error_message = "Terms is not accepted, please `contact admin.";
        elseif(!$currentProfile->is_mail_verified):
            $redirect_to = "cpanel.member.profile-setup";
            $error_message = "Mail not verified, please verify your mail. <a href='".route('verification.resend')."' class='btn btn-sm btn-link'>Resend</a>";
        elseif(!$currentProfile->is_kyc_submitted):
            $redirect_to = "cpanel.member.submit-docs";
            $error_message = "You need to submit your KYC-(Know Your Customer) details to go verified & validated your profile.";
        elseif(!$currentProfile->is_complete_match_preference):
            $redirect_to = "cpanel.member.preference-setup";
            $error_message = "Please complete Match Preference Setup.";
        elseif(!$currentProfile->is_complete_profile):
            $redirect_to = "cpanel.member.profile-setup";
            $error_message = "Please complete Profile Setup.";
        elseif(!$currentProfile->is_in_search_result):
            $redirect_to = "cpanel.member.profile-setup";
            $error_message = "You are not allow to be in search result by other user, either please complete your incomplete profile or contact our support system.";
        elseif($currentProfile->is_block):
            $redirect_to = "login";
            $error_message = "You are blocked by us, please contact us for further clarification.";
        else:
            $success = true;
        endif;
        return ['success'=>$success, 'message' => $error_message, 'redirect' => $redirect_to];
    }


}
