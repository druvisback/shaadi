<?php

namespace App\Http\Controllers\Member\Pages;

use App\model\Kyc;
use App\model\MatchPreference;
use App\model\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class KYCController extends Controller{
    public function index(Request $request){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $kyc = Kyc::where('ref_profile_id', $profile->id)->first();

        $view = view('member-layout.kycs');

        if($kyc && !$kyc->is_check):
            $view->with('error', 'Please wait until your previous submission not checked by our team...');
        endif;


        return $view;
    }
    public function submit(Request $request){
        $current_timestamp = Carbon::now()->timestamp;

        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $err_msgs = [
            'type_of_poa' => 'Must need to select POA doc type',
            'poa_file' => 'Must need to select POA doc file',
            'type_of_poi' => 'Must need to select POI doc type',
            'poi_file' => 'Must need to select POI doc file',
        ];

        $validator = Validator::make($request->all(), [
            'type_of_poa' => 'required|string',
            'poa_file' => 'required',
            'type_of_poi' => 'required|string',
            'poi_file' => 'required'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $kyc = Kyc::where('ref_profile_id', $profile->id)->first();

            if($kyc && !$kyc->is_check):
                return Redirect::back()->with('error', 'Submission canceled, Please wait until your previous submission not checked by our team...')
                    ->withInput();
            endif;

            if($profile):
                $kyc_data = ($validator->validated());

                $target = 'public/user_gallery/'.$user->id.'/kyc/';
                Storage::deleteDirectory($target);
                $file_poa = $request->file('poa_file');
                $file_poi = $request->file('poi_file');
                $file_ext_poa = $file_poa->extension();
                $file_ext_poi = $file_poi->extension();

                $file_name_poa = "img_".$user->id."_poa_".$current_timestamp.".".$file_ext_poa;
                $file_name_poi = "img_".$user->id."_poi_".$current_timestamp.".".$file_ext_poi;
                $request->file('poa_file')->storeAs(
                    $target, $file_name_poa
                );
                $request->file('poi_file')->storeAs(
                    $target, $file_name_poi
                );

                $kyc_data['ref_profile_id'] = $profile->id;
                $kyc_data['poa_file'] = $file_name_poa;
                $kyc_data['poi_file'] = $file_name_poi;
                $kyc_data['is_check'] = false;
                $kyc_data['is_valid'] = false;
                if($kyc):
                    $kyc->update($kyc_data);
                else:
                    Kyc::create($kyc_data);
                endif;

                $profile->is_kyc_submitted = true;
                $profile->save();
            endif;
            return redirect()->route('cpanel.member')->with('success', 'KYC Successfully submitted, please wait until verification by our team.');
        endif;
    }
}
