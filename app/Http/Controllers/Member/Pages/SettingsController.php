<?php

namespace App\Http\Controllers\Member\Pages;

use App\model\MatchPreference;
use App\model\Profile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller{
    public function index(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $match_pref = MatchPreference::where('ref_profile', $profile->id)->first();
        return view('pages.settings',compact('user', 'profile', 'match_pref'));
    }
}
