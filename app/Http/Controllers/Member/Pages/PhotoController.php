<?php

namespace App\Http\Controllers\Member\Pages;

use App\model\MatchPreference;
use App\model\Profile;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PhotoController extends Controller
{
    public function index(){
        //dd("okkk");
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $match_pref = MatchPreference::where('ref_profile', $profile->id)->first();
        return view('member-layout.photos');
    }

    public function uploadPhoto(Request $request){
        $file = $request->file('new_photo');
        if($file):

            $file_ext = $file->extension();
            $current_timestamp = Carbon::now()->timestamp;
            $uid = $request->user()->id;



            $user = Auth::user();
            $profile_query = Profile::where('ref_user', $user->id);
            if($profile_query->count() > 0):
                $file_name = "img_".$uid."_".$current_timestamp.".".$file_ext;
                $request->file('new_photo')->storeAs(
                    'public/user_gallery/'.$user->id.'/', $file_name
                );

                $profile = $profile_query->first();
                $gallery = $profile->gallery_photos;
                $arr_gallery = [];
                if(isset($gallery)):
                    $arr_gallery = unserialize($gallery);
                endif;
                array_push($arr_gallery, $file_name);
                $profile->gallery_photos = serialize($arr_gallery);
                $profile->save();
            endif;
            //gallery_photos
            //return $path;
            return redirect()->back()->with('success','Photo Uploaded!');
            //dd($path);
        endif;
        return redirect()->back()->with('error','Invalid File!');
    }

    public static function deletePhoto($file_path){
        $user = Auth::user();
        $profile_query = Profile::where('ref_user', $user->id);

        $actual_file_path = 'public/user_gallery/'.$user->id.'/'.$file_path;
        if(Storage::exists($actual_file_path)):
            Storage::delete($actual_file_path);
            $user = Auth::user();
            $profile_query = Profile::where('ref_user', $user->id);
            if($profile_query->count() > 0):
                $profile = $profile_query->first();
                $gallery = $profile->gallery_photos;
                $arr_gallery = [];
                if(isset($gallery)):
                    $arr_gallery = unserialize($gallery);
                    $arr_gallery = array_diff($arr_gallery,[$file_path]);
                endif;
                $profile->gallery_photos = serialize($arr_gallery);
                $profile->save();
            endif;
            return redirect()->back()->with('success', 'Photo deleted successfully.');
        endif;
        return redirect()->back()->with('error', 'Photo can\'t deleted');
    }

    public static function getPhotoURLs(){
        $response = ['success' => false, 'data' => []];
        $user = Auth::user();
        $profile_query = Profile::where('ref_user', $user->id);
        if($profile_query->count() > 0):
            $profile = $profile_query->first();
            $gallery = $profile->gallery_photos;
            $arr_gallery = [];
            if(isset($gallery)):
                $arr_gallery = unserialize($gallery);
                if(!empty($arr_gallery)):
                    $response['success'] = true;
                    foreach($arr_gallery as $file_path):
                        $file_url = asset('storage/user_gallery/'.$file_path);
                        array_push($response['data'],['url' => $file_url, 'path' => $file_path]);
                    endforeach;
                endif;
            endif;
        endif;
        return $response;
    }
}
