<?php

namespace App\Http\Controllers\Member\Pages;

use App\model\MatchPreference;
use App\model\Profile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller{
    public function index(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $match_pref = MatchPreference::where('ref_profile', $profile->id)->first();
        return view('member-layout.profile',compact('user', 'profile', 'match_pref'));
    }

    public function updateProfile(Request $request){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        //$match_pref = MatchPreference::where('ref_profile', $profile->id)->first();

        $err_msgs = [
            'dob.before' => '<b>Date of Birth (Age)</b> should be greater than equals 18',
            'last_education_institude_name' => 'Should Be Enter The Institute Name',
            'last_education_name' => 'Should Be Enter The Last Education Name',
            'last_education_passing_year' => 'Should Be Enter The Passing Year',

            'marital_status.required' => '<b>Marital Status</b> need to select.',
            'marital_status.in' => '<b>Marital Status</b> should be select from Unmarried, Divorced & Widow.',

            'height_cm.integer' => '<b>Height</b> should be a numerical value.',
            'height_cm.max' => '<b>Height</b> should not be greater than 203 centimeters (6 ft 8 inch).',
            'height_cm.min' => '<b>Height</b> should not be greater than 120 centimeters.',

            'weight_kg.min' => '<b>Weight</b> should not be less than 40 kilos.',
            'weight_kg.max' => '<b>Weight</b> should not be greater than 150 kilos.',

            'body_type.in' => '<b>Body Type</b> should not select from Athlete, Average and Slim.',

            'family_father_status.min' => '<b>Father Status</b> should minimum 20 characters.',
            'family_father_status.max' => '<b>Father Status</b> should maximum upto 255 characters.',

            'family_mother_status.min' => '<b>Mother Status</b> should minimum 20 characters.',
            'family_mother_status.max' => '<b>Mother Status</b> should maximum upto 255 characters.',

            'family_no_of_sister.min' => '<b>No of Sister</b> should minimum 0.',
            'family_no_of_sister.max' => '<b>No of Brother</b> should maximum upto 10.',

            'family_no_of_brother.min' => '<b>Mother Status</b> should minimum 0.',
            'family_no_of_brother.max' => '<b>Mother Status</b> should maximum upto 10.',

            'family_description.min' => '<b>Family Description</b> should minimum 20 characters.',
            'family_description.max' => '<b>Family Description</b> should maximum upto 255 characters.',
        ];

        $eligible_last_dob = date("Y-m-d", strtotime("-18 years", time()));

        $validator = Validator::make($request->all(), [
            'dob' => 'required|date|date_format:Y-m-d|before:'.$eligible_last_dob,
            'last_education_institude_name' => 'required',
            'last_education_name' => 'required',
            'last_education_passing_year' => 'required',
            'marital_status' => 'required|in:unmarried,divorced,widower',
            'height_cm' => 'required|integer|max:203|min:100',
            'body_type' => 'required|in:athlete,average,slim',
            'weight_kg' => 'required|integer|min:30|max:150',
            'ref_country_grew_up_in' => 'required|integer',
            'ref_country' => 'required|integer',
            'ref_state' => 'required|integer',
            'ref_city' => 'required|integer',
            'diet' => 'required|in:veg,nonveg,both',
            'drink' => 'required|in:occasionally,regular,no,heavy',
            'smoke' => 'required|in:occasionally,regular,no,heavy',
            'sun_sign' => 'required|in:virgo,leo,libra,scorpio,cancer,gemini,taurus,aries,pisces,aquarius,capricorn,sagittarius',
            'blood_group' => 'required|in:a-,a+,b-,b+,ab-,ab+,o+,o-',
            'have_disability' => ['required', 'min:1', 'string', 'max:1'],
            'health_description' => ['required', 'min:0', 'string', 'max:255'],
            'self_description' => ['required', 'min:0', 'string', 'max:255'],
            'family_father_status' => 'required|min:0|string|max:255',
            'family_mother_status' => 'required|min:0|string|max:255',
            'family_no_of_sister' => 'required|integer|min:0|max:10',
            'family_no_of_brother' => 'required|integer|min:0|max:10',
            'family_description' => 'required|string|max:255',
            'mobile_no' => 'required|integer|min:10',
            'business_email' => 'required|email',
            'contact_person' => 'required|string|max:255',
            'relationship_with_member' => 'required|string|in:self,father,mother,brother,sister,friend','relative',
            'time_to_call' => 'required|string|max:255',
            'have_privacy_dp' =>'required|boolean',
            'have_privacy_basic_lifestyle' =>'required|boolean',
            'have_privacy_family_details' =>'required|boolean',
            'have_privacy_contact_details' =>'required|boolean',
            'have_privacy_location_details' =>'required|boolean',
            'have_privacy_gallery_photo' =>'required|boolean',
            'have_privacy_educational_details' =>'required|boolean'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $profile_data = ($validator->validated());
            if($request->hasFile('profile_photo')):
                $profile_photo = $request->file('profile_photo');

                $file_ext = $profile_photo->extension();
                $current_timestamp = Carbon::now()->timestamp;

                $profile_photo_file_name = "img_profile_".$user->id."_".$current_timestamp.".".$file_ext;
                $profile_photo->storeAs(
                    'public/profile/', $profile_photo_file_name
                );
                $profile_data['profile_photo'] = $profile_photo_file_name;
            endif;

            $profile->update($profile_data);
            $profile->is_complete_profile = true;
            $profile->is_in_search_result = true;
            $profile->save();
            return redirect()->route('cpanel.member.profile-setup')->with('success', 'Profile Successfully Updated');
            //return redirect()->route('cpanel.member.profile-setup')->with("success",'Profile Successfully Updated');
        endif;
    }

    public function viewProfile($pid){
        $profile = Profile::where('id',$pid)->first();
        if($profile && $profile->is_complete_profile):
            return view('member-layout.view-profile', compact('profile'));
        else:
            return abort(403,'Profile not completed...');
        endif;
    }
    public function deleteProfile($pass){

        $user = Auth::user();
        if (Hash::check($pass, $user->password)) {
            $user->delete();
            return redirect('/')->with('success','Your Account Deleted successfully...');
        }else{
            return redirect()->route('cpanel.member.profile-setup')->with('error','Wrong Password, please try again or recover your password...');
        }
    }

    public function updateChangePassword(Request $request)
    {
        $user = Auth::user();
        $old_pass = $request->get('old_pass', true);
        $pass1 = $request->get('new_password', false);
        $pass2 = $request->get('cnf_password', false);
        $id = Auth::user()->id;
        $pass = User::where('id', $id)->value('password');
        if (Hash::check($old_pass, $pass)) {
            if ($pass1 == $pass2) {
                $password = Hash::make($pass1);
                $changePass = User::where('id', $id)->update([
                    'password' => $password,
                ]);
                if ($changePass == true) {
                    return redirect()->back()->with('success', "Password Updated Sucessfully !!!");
                }
            } else {
                return redirect()->back()->with('error', "New Password and Confirm Password are Not Matched !!!");
            }
        } else {
            return redirect()->back()->with('error', "Old Password Not Matched !!!");
        }
    }


}
