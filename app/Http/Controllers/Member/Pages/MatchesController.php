<?php

namespace App\Http\Controllers\Member\Pages;

use App\model\MatchPreference;
use App\model\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MatchesController extends Controller{
    public function index(){
        $currentUID = Auth::user()->id;
        $profile = Profile::where('ref_user', $currentUID)->first();
        $match_pref = MatchPreference::where('ref_profile',$profile->id)->first();
        $admin_uid = User::where('type','admin')->first()->id;
        $match_profiles = Profile::where('gender',$match_pref->looking_for)
            ->where('ref_religion',$match_pref->ref_religion)
            ->where('ref_user', '!=', $admin_uid)
                ->get();
        return view('member-layout.matches',compact('user', 'profile', 'match_pref','match_profiles'));
    }
    public function search(Request $request){
        dd($request->all());
        $currentUID = Auth::user()->id;
        $profile = Profile::where('ref_user', $currentUID)->first();
        $match_pref = MatchPreference::where('ref_profile',$profile->id)->first();
        $admin_uid = User::where('type','admin')->first()->id;
        $match_profiles = Profile::where('gender',$match_pref->looking_for)
            ->where('ref_religion',$match_pref->ref_religion)
            ->where('ref_user', '!=', $admin_uid)
            ->get();
        return view('member-layout.matches',compact('user', 'profile', 'match_pref','match_profiles'));
    }
}
