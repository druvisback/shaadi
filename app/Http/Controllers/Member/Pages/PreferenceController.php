<?php

namespace App\Http\Controllers\Member\Pages;

use App\model\Profile;
use Illuminate\Http\Request;
use App\model\MatchPreference;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PreferenceController extends Controller{
    public function index(){
        $currentUID = Auth::user()->id;
        $profile = Profile::where('ref_user', $currentUID)->first();
        $match_preference = MatchPreference::where('ref_profile',$profile->id)->first();
        return view('member-layout.match-preference')
            ->with('preference_data',$match_preference);
    }

    public function save(Request $request){

        //dd($request->all());

        $min_age = $request->get('from_age','');
        $max_age = $request->get('to_age','');

        $min_height = $request->get('from_height_cm','');
        $max_height = $request->get('to_height_cm','');


        $err_msgs = [
            'from_age.min' => '<b>Minimum Age</b> should be greater than equals 18',
            'from_age.max' => '<b>Minimum Age</b> should not be greater than <b>Maximum Age</b>',
            'to_age.min' => '<b>Maximum Age</b> should not be less than <b>Minimum Age</b>',
            'from_height_cm.max' => '<b>Minimum Height</b> should not be greater than <b>Maximum Height</b>',
            'to_height_cm.min' => '<b>Maximum Height</b> should not be less than <b>Minimum Height</b>',
            'ref_religion.required' => '<b>Religion</b> need to select.',
            'ref_cast.required' => '<b>Cast</b> need to select.',
            'ref_language.required' => '<b>Mother Tongue</b> need to select.',
            'ref_country.required' => '<b>Country</b> need to select.',
            'ref_state.required' => '<b>State</b> need to select.',
            'ref_city.required' => '<b>City</b> need to select.',
            'looking_for.required' => '<b>Looking For</b> need to select.',
            'marital_status.required' => '<b>Marital Status</b> need to select.'
        ];

        $validator = Validator::make($request->all(), [
            'from_age' => 'required_with:to_age|integer|min:18|max:'. ($max_age-1),
            'to_age' => 'required_with:from_age|integer|min:'. ($min_age+1) .'|max:120',
            'from_height_cm' => 'required_with:to_height_cm|integer|min:100|max:'. ($max_height-1),
            'to_height_cm' => 'required_with:from_height_cm|integer|max:200|min:'. ($min_height+1),
            'marital_status' => ['required', 'min:1', 'string', 'max:255'],
            'ref_religion' => ['required', 'min:1', 'string', 'max:255'],
            'ref_cast' => ['required', 'min:1', 'string', 'max:255'],
            'ref_language' => ['required', 'min:1', 'string', 'max:255'],
            'ref_country' => ['required', 'min:1', 'string', 'max:255'],
            'ref_state' => ['required', 'min:1', 'string', 'max:255'],
            'ref_city' => ['required', 'min:1', 'string', 'max:255'],
            'looking_for' => ['required', 'min:1', 'string', 'max:255'],
        ], $err_msgs);

        $validator->validate();
        //dd($validatedData);

        if($validator->fails()):
        return Redirect::back()->withErrors($validator)
            ->withInput();
        else:
            $currentUser = Auth::user();
            $uid = $currentUser->id;
            $profile = Profile::where('ref_user',$uid)->first();
            $matchPrefQuery = MatchPreference::where('ref_profile',$profile->id);

            $preference_data = ($validator->validated());

            if($matchPrefQuery->count() > 0):
                $matchPref = $matchPrefQuery->first();
                $matchPref->update($preference_data);
            else:
                $preference_data = array_merge($preference_data,[
                    'ref_profile' => $profile->id
                ]);
                $matchPreference = MatchPreference::create($preference_data);
                if($matchPreference):
                    $profile->is_complete_match_preference = true;
                    $profile->save();
                endif;
            endif;
            return redirect()->route('cpanel.member.profile-setup')->with('success', 'Preference Successfully Updated');
            //return redirect(route('cpanel.member.profile-setup'));
        endif;
    }
}
