<?php

namespace App\Http\Controllers\Member\Pages;

use App\model\Kyc;
use App\model\MatchPreference;
use App\model\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MyPackageController extends Controller{
    public function index(Request $request){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        return view('member-layout.package');
    }
}
