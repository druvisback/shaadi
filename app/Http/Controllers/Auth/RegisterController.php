<?php

namespace App\Http\Controllers\Auth;

use App\model\Profile;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller{

    use RegistersUsers;

    protected $redirectTo = '/dashboard';

    public function __construct(){
        $this->middleware('guest');
    }

    protected function validator(array $data){
        $messages = [
            'password.required' => 'Password is required.',
            'password.min' => 'Password should be Min. 6 chars.',
            'referral_profile_id.exists' => 'Referral profile ID not exist',
            'password.regex'    => 'Password should be (Min. 6 characters long and must contain a lower case and must contain a upper case and must contain a special char).'
        ];

        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'profile_for' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'min:1', 'string', 'max:255'],
            'religion' => ['required', 'min:1', 'string', 'max:255'],
            'cast' => ['required', 'min:1', 'string', 'max:255'],
            'language' => ['required', 'min:1', 'string', 'max:255'],
            'country' => ['required', 'min:1', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            /*'password' => ['required', 'min:6', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/'],*/
            'password' => [
            'required',
            'string',
            'min:6',             // must be at least 8 characters in length
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[-_!@$!%*#?&]/', // must contain a special character
        ],
            'terms_and_condition' => ['required', 'string', 'max:255'],
            'referral_profile_id' => ["exists:profiles,profile_id"]
        ],$messages);
    }

    public static function generateProfileID($id, $is_male = true,$prefix = "LU"){
        $gender = ($is_male)?'M':'F';
        $pid = $prefix.$gender.$id;
        return $pid;
    }

    protected function create(array $data){
        $referral_profile_id = $data['referral_profile_id'];

        //dd($data);
        $type = 'member';

        $admin_found_query = User::where('type','admin');
        if($admin_found_query->count() <= 0):
            $type = "admin";
        endif;

        $user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'type' => $type,
        ]);

        if($user):
            $profile_id = RegisterController::generateProfileID($user->id,strtolower($data['gender']));

            $data = [
                'ref_user' => $user->id,
                'profile_id' => $profile_id,
                'ref_religion' =>  $data['religion'],
                'ref_cast' =>  $data['cast'],
                'ref_country' =>  $data['country'],
                'ref_language' =>  $data['language'],
                'first_name' =>  $data['first_name'],
                'last_name' =>  $data['last_name'],
                'gender' =>  $data['gender'],
                'profile_for' =>  $data['profile_for'],
                'is_accept_terms' =>  true,
                'is_block' => false,
                'err_message' => 'Please verify your email first.',
            ];

            if($referral_profile_id):
                $data['referral_profile_id'] = $referral_profile_id;
            endif;

            Profile::create($data);
        endif;
        return $user;
    }
}
