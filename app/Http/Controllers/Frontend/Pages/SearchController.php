<?php

namespace App\Http\Controllers\Frontend\Pages;

use App\model\MatchPreference;
use App\model\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller{
    public function viewQuickSearch(Request $request){
        $use_preference = false;

        $data = null;
        $profiles = null;
        $admin_uid = User::where('type','admin')->first()->id;


            $data = $request->only([
                'gender', 'from_age', 'to_age', 'ref_religion', 'ref_cast', 'ref_language', 'ref_country', 'ref_country_grew_up_in', 'marital_status'
            ]);

            $gender = $data["gender"]??"any";
            $from_age = $data["from_age"]??"18";
            $to_age = $data["to_age"]??"30";
            $ref_religion = $data["ref_religion"]??"any";
            $ref_cast = $data["ref_cast"]??"any";
            $ref_language = $data["ref_language"]??"any";

            $ref_country = isset($data["ref_country"])?$data["ref_country"]:'any';
            $ref_country_grew_up_in = isset($data["ref_country_grew_up_in"])?$data["ref_country_grew_up_in"]:'any';
            $marital_status = isset($data["marital_status"])?$data["marital_status"]:'any';


            $profiles_query = Profile::where('ref_user', '!=', $admin_uid)->
            where('is_account_deactivate', false)
            ->where('is_complete_profile',true);

            //User Preference...
            if(empty($data) && Auth::check()):
                $current_user = Auth::user();
                $profiles_query = $profiles_query->where('ref_user','!=',$current_user->id);

                $current_profile = Profile::where('ref_user', $current_user->id)->where('is_complete_match_preference', true)->first();
                if($current_profile):
                    $match_pref = MatchPreference::where('ref_profile', $current_profile->id)->first();
                    if($match_pref):
                        $use_preference = true;
                        $from_age = $match_pref->from_age;
                        $to_age = $match_pref->to_age;

                        /*$pref_from_height_cm = $current_profile->from_height_cm;
                        $pref_to_height_cm = $current_profile->to_height_cm;*/

                        $marital_status = $match_pref->marital_status;
                        $gender = $match_pref->looking_for;

                    endif;
                endif;
            endif;


            //Gender Filtration...
            if($gender != "any"):
                $profiles_query = $profiles_query->where('gender',$gender);
            endif;

            //Language Filtration...
            if($ref_language != "any"):
                $profiles_query = $profiles_query->where('ref_language',$ref_language);
            endif;

            //Religion Filtration...
            if($ref_religion != "any"):
                //For Religion
                $profiles_query = $profiles_query->where('ref_religion',$ref_religion);
            endif;

            //Country Filtration...
            if($ref_country != "any"):
                $profiles_query = $profiles_query->where('ref_country',$ref_country);
            endif;

            //Country Filtration...
            if($ref_country_grew_up_in != "any"):
                $profiles_query = $profiles_query->where('ref_country_grew_up_in',$ref_country_grew_up_in);
            endif;

            //Marital Filtration...
            if($marital_status != "any"):
                $profiles_query = $profiles_query->where('marital_status',$marital_status);
            endif;

            //Cast Filtration...
            if($ref_cast != "any"):
                //For Cast
                $profiles_query = $profiles_query->where('ref_cast',$ref_cast);
            endif;

            //Age Filtration ....
            if($from_age == "any"):
                $from_age = 18;
            endif;
            if($to_age == "any"):
                $to_age = 100;
            endif;


            if($from_age < $to_age):
                if($profiles_query->count() > 0):
                    $profiles = $profiles_query->get();
                    $filter_matches_ids = [];
                    foreach($profiles as $p):
                        if($p->dob):
                            $age = Carbon::make($p->dob)->age;
                            if($age >= $from_age && $age <= $to_age):
                                array_push($filter_matches_ids, $p->id);
                            endif;
                        else:
                            array_push($filter_matches_ids, $p->id);
                        endif;
                    endforeach;
                    $profiles_query = Profile::whereIn('id', $filter_matches_ids);
                endif;
            endif;



         //Get Result...
            $profiles = $profiles_query->get();




        $filter_data = [
            "gender" => $gender,
            "from_age" => $from_age,
            "to_age" => $to_age,
            "ref_religion" => $ref_religion,
            "ref_cast" => $ref_cast,
            "ref_language" => $ref_language,
            "ref_country" => $ref_country,
            'ref_country_grew_up_in' => $ref_country_grew_up_in,
            'marital_status' => $marital_status
        ];

        return view('quick-search', compact('profiles', 'filter_data','use_preference'));
    }
    public function doQuickSearch(Request $request){
        $data = null;
        $profiles = null;
        $admin_uid = User::where('type','admin')->first()->id;

        if(Auth::check()):

        else:
            $data = $request->only([
                'gender', 'from_age', 'to_age', 'ref_religion', 'ref_cast', 'ref_language'
            ]);
            $gender = $data->gender;
            $from_age = $data->from_age;
            $to_age = $data->to_age;
            $ref_religion = $data->ref_religion;
            $ref_cast = $data->ref_cast;
            $ref_language = $data->ref_language;

            $profiles_query = Profile::where('ref_user', '!=', $admin_uid);

            $profiles = $profiles_query->get();
        endif;
        return redirect()->back()->withInput()->with('profiles',$profiles);
    }


    public function index(Request $request){
        $from_age = $request->get('from_age', false);
        $to_age = $request->get('to_age', false);
        $gender = $request->get('gender', false);

        $data = false;
        $match_profiles = false;
        $match_pref = false;
        $profile = false;
        $predefined_filter = $request->all();
        $admin_uid = User::where('type','admin')->first()->id;
        if(Auth::check()):
            $currentUID = Auth::user()->id;
            $profile = Profile::where('ref_user', $currentUID)->
            first();
            $match_pref = MatchPreference::where('ref_profile',$profile->id)->first();
            $match_profiles = Profile::where('gender',$match_pref->looking_for)
                ->where('ref_religion',$match_pref->ref_religion)
                ->where('ref_user', '!=', $admin_uid)
                    ->get();
        else:
            //From Home Page Guest Redirect...

            $match_profiles_query = Profile::where('gender',$gender)
                ->where('ref_user', '!=', $admin_uid);

                //Age Filter...
                if($match_profiles_query->count() > 0):
                    $match_profiles = $match_profiles_query->get();
                    $filter_matches_ids = [];
                    foreach($match_profiles as $p):
                        $age = Carbon::make($p->dob)->age;
                        if($age >= $from_age && $age <= $to_age):
                            array_push($filter_matches_ids, $p->id);
                        endif;
                    endforeach;
                    $match_profiles = Profile::findMany($filter_matches_ids);
                endif;
        endif;
        $data = [
            'profile' => $profile,
            'match_pref' => $match_pref,
            'match_profiles' => $match_profiles
        ];
        return view('searches',compact('data','match_profiles','predefined_filter'));
    }
}
