<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\CustomFieldController;
use App\model\Faq;
use App\model\Story;
use App\model\Policy;
use App\model\Condition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller{
    public function viewHelp(){
        return view('pages.help')
            ->with('head_img_url','//cdn.newretirement.com/retirement/wp-content/uploads/2018/02/iStock-875218208.jpg')
            ->with('page_title','Help');
    }
    public function viewContact(){
        return view('pages.contact')
            ->with('head_img_url','//cdn.newretirement.com/retirement/wp-content/uploads/2018/02/iStock-875218208.jpg')
            ->with('page_title','Contact Us');
    }

    public function viewPremiumPlans(){
        return view('pages.premium-plans')
            ->with('head_img_url','//cdn.newretirement.com/retirement/wp-content/uploads/2018/02/iStock-875218208.jpg')
            ->with('page_title','Premium Plans');
    }
    public function viewHappyStory(){
        $story_content = CustomFieldController::getValue('page_story_content')??'';
        return view('pages.happy-stories',compact('stories'))
            ->with('head_img_url','//cdn.newretirement.com/retirement/wp-content/uploads/2018/02/iStock-875218208.jpg')
            ->with('page_title','Happy Story')
            ->with('story_content',$story_content);
    }

    public function viewAllMembers(){
        return view('pages.help')
            ->with('head_img_url','//cdn.newretirement.com/retirement/wp-content/uploads/2018/02/iStock-875218208.jpg')
            ->with('page_title','All Members');
    }

    public function viewFaqs(){
        $faqs=Faq::get();
        return view('pages.faqs',compact('faqs'))
            ->with('head_img_url','//cdn.newretirement.com/retirement/wp-content/uploads/2018/02/iStock-875218208.jpg')
            ->with('page_title','FAQs');
    }
    public function viewTerms(){
      $terms= Condition::get();
        return view('pages.terms',compact('terms'))
            ->with('head_img_url','//cdn.newretirement.com/retirement/wp-content/uploads/2018/02/iStock-875218208.jpg')
            ->with('page_title','Terms and Conditions');
    }
    public function viewPolicy(){
      $policies= Policy::get();
        return view('pages.policy',compact('policies'))
            ->with('head_img_url','//cdn.newretirement.com/retirement/wp-content/uploads/2018/02/iStock-875218208.jpg')
            ->with('page_title','Privacy Policy');
    }
}
