<?php

namespace App\Http\Controllers;

use App\model\Profile;
use App\model\Testimonial;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ExtraController extends Controller{

    public static $is_demo = false;

    public static function isDemo(){
        return ExtraController::$is_demo;
    }

    public static function getKYCPhoto_URL(Profile $profile, $path){
        $user_id = $profile->ref_user;
        $image_path = 'storage/user_gallery/'.$user_id.'/kyc/'.$path;
        $image_url = self::assetURL($image_path);
        return $image_url;
    }

    public static function getProfilePhotoURL($profile){
        $demo_link_prefix = 'http://webinch.com/developed/shaadi/storage/app/public/profile/';
        $prefix = 'public/';
        $photo_url = false;
        if(!$profile):
            if(ExtraController::$is_demo):
                $photo_url = asset($prefix.'images/trans-icon.png');
            else:
                $photo_url = asset('images/trans-icon.png');
            endif;
        else:
            $gender = $profile->gender;

            $path = 'profile/'.$profile->profile_photo;
            $photo_url = Storage::url($path);
            if($profile->profile_photo):
                if(ExtraController::$is_demo):
                    $photo_url = $demo_link_prefix.$profile->profile_photo;
                else:
                    if(!($path && strlen($path))):
                        switch($gender):
                            case 'male':
                                $photo_url =  self::assetURL('images/male-icon.png');
                                break;
                            case 'female':
                                $photo_url = self::assetURL('images/female-icon.png');
                                break;
                            default:
                                $photo_url = self::assetURL('images/trans-icon.png');
                                break;
                        endswitch;
                    endif;
                endif;
            else:
                switch($gender):
                    case 'male':
                        $photo_url =  self::assetURL('images/male-icon.png');
                        break;
                    case 'female':
                        $photo_url = self::assetURL('images/female-icon.png');
                        break;
                    default:
                        $photo_url = self::assetURL('images/trans-icon.png');
                        break;
                endswitch;
            endif;
        endif;

        return $photo_url;
    }
    public static function getGalleryPhotoURL($img_path, $profile_id){
        $profile = Profile::find($profile_id);
        $demo_link_prefix = 'http://webinch.com/developed/shaadi/storage/app/public/user_gallery/'.$profile->ref_user.'/';


        if(ExtraController::$is_demo):
            $photo_url = $demo_link_prefix.$img_path;
        else:
            $photo_url = Storage::url('public/user_gallery/'.$profile->ref_user.'/'.$img_path);
        endif;
        return $photo_url;
    }

    public static function testimonialPhotoURL(Testimonial $testimonial){
        $demo_link_prefix = 'http://webinch.com/developed/shaadi/storage/app/public/admin/';

        if(ExtraController::$is_demo):
            $path = $demo_link_prefix.$testimonial->photo_url;
            $photo_url = $path;
        else:
            $path = 'admin/'.$testimonial->photo_url;
            $photo_url = Storage::url($path);
        endif;

        return $photo_url;
    }

    public static function assetURL($path){
        if(ExtraController::$is_demo):
            $path = "public/".$path;
        endif;
        return asset($path);
    }

    public static function photoURL($file_name){
        $image_path = 'storage/uploads/'.$file_name;
        $image_url = self::assetURL($image_path);
        return $image_url;
    }
}
