<?php

namespace App\Http\Controllers;

use App\model\Profile;
use Illuminate\Http\Request;

class HomeController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $profiles = Profile::where('gender','male')->where('is_complete_profile', true)->take(10)->get();
        $found_love = [];
        foreach($profiles as $profile):
            $pair = [
                'female' => null,
                'male' => $profile,
            ];
            $related_female = Profile::where('gender','female')->
            where('is_complete_profile', true)->
            where('ref_country',$profile->ref_country)->
            where('ref_state',$profile->ref_state)->
            where('ref_city', $profile->ref_city)->
            where('ref_language', $profile->ref_language)
                ->first();
            $pair['female'] = $related_female;

            if($related_female):
                array_push($found_love, $pair);
            endif;
        endforeach;
        return view('welcome', compact(['found_love']));
    }
}
