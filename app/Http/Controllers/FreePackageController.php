<?php

namespace App\Http\Controllers;

class FreePackageController extends Controller{
    public static function saveData($data){
        return CustomFieldController::saveField('package_free_config', $data);
    }
    public static function getData(){
        return CustomFieldController::getValue('package_free_config');
    }
}
