<?php

namespace App\Http\Controllers;

use App\model\Order;
use App\model\Profile;
use App\User;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller{
    public static function getLatestOrder_ByUserID($user_id){
        $response = false;
        $user = User::find($user_id);
        if($user):
            $lastestOrder = Order::where('ref_user_id', $user->id)->first();
            if($lastestOrder):
                $response = $lastestOrder;
            endif;
        endif;
        return $response;
    }
    public static function getLatestOrder_ByUser(User $user){
        return self::getLatestOrder_ByUserID($user->id);
    }
    public static function getLatestOrder_ByProfile(Profile $profile){
        return self::getLatestOrder_ByUserID($profile->ref_user);
    }

    private static function checkLatestOrderStatus_currentUser(){
        $user = Auth::user();
        $latestOrder =  self::getLatestOrder_ByUser($user);
        $response = false;
        if($latestOrder):
            dd($response);
        endif;
        return $response;
    }
}
