<?php

namespace App\Http\Controllers;

use App\model\Profile;
use App\PackagePlan;
use App\PlanMapped;
use Illuminate\Http\Request;

class PaymentController extends Controller{
    public function UserPlanRegistration($user_id){
        $plans=PackagePlan::get();
        return view('pages.plan',compact('plans','user_id'));
    }
    public function UserSubscription(Request $request){
        $plan_id=$request->get('plan');
        $plan=PackagePlan::where('id',$plan_id)->first();
        $month=$plan->month;
        $d=strtotime("+$month months");
        $date = date("Y-m-d", $d);
        $payment=PlanMapped::create([
            'user_id'=>$request->user_id,
            'plan_id'=>$plan_id,
            'month'=>$month,
            'date'=>$date,
            'status'=>'Pending',
            'price'=>$plan->price,
        ]);
        return view('pages.payment',['order_id'=>$payment->id,'user_id'=>$payment->user_id,'month'=>$payment->month,'amount'=>$payment->price]);
    }

    public function recurring_paypalIpn(Request $request,$order_id)
    {
        if ($request->st == 'Completed') {
            PlanMapped::where('id',$order_id)->update([
                'status' => 'Completed',
                'transaction_id' => $request->tx,
            ]);

            $payment=PlanMapped::where('id',$order_id)->first();

            PlanMapped::where('user_id',$payment->user_id)->update([
                'plan_id' =>$payment->plan_id,
                'expire_date' =>$payment->date,
                'payment_status' =>'Completed',
            ]);
            return redirect(route('search'))->with('success','Payment Completed.....');

        }

    }

}
