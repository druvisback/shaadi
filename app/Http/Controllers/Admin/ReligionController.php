<?php

namespace App\Http\Controllers\Admin;

use App\model\Cast;
use App\model\Profile;
use App\model\Religion;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ReligionController extends Controller{
    public function viewAll(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.religion.all-religions',compact(['user','profile']));
    }

    public function viewAdd(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.religion.add-religion',compact(['user','profile']));
    }
    public function createReligion(Request $request){
        $err_msgs = [
            'name.required' => 'Name is required.',
            'casts.required' => 'Casts is required.'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'casts' => 'required'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $data = ($request->only(['name']));
            $casts = $request->get('casts');
            $arrCasts = json_decode($casts);
            $religion = Religion::create($data);
            foreach($arrCasts as $cast):
                Cast::create([
                    'name' => $cast,
                    'ref_religion' => $religion->id
                ]);
            endforeach;
            return redirect()->back()->with('success', 'Religion & Related Casts added successfully');
        endif;
    }

    public function delete($religion_id){
        $religion = Religion::find($religion_id);
        if($religion->count()){
            $related_casts = Cast::where('ref_religion', $religion_id);
            $total_casts = $related_casts->count();
            $name = $religion->name;
            $related_casts->delete();
            $religion->delete();
            return redirect()->back()->with('success', 'Religion '.$name.'[#'.$religion_id.'] with ('.$total_casts.' Related Casts) deleted successfully...');
        }
        return redirect()->back()->with('error', 'Invalid Religion...');
    }
    public function viewEdit($religion_id){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $religion = Religion::find($religion_id);
        if(!$religion->count()){
            return redirect()->back()->with('error', 'Invalid Religion...');
        }
        $casts = Cast::where('ref_religion', $religion_id)->get();
        return view('admin-layout.pages.religion.edit-religion',compact(['user','profile','religion', 'casts']));
    }

    public function updateReligion($religion_id, Request $request){
        $religion = Religion::find($religion_id);

        $err_msgs = [
            'name.required' => 'Name is required.'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $data = ($request->only(['name']));
            $religion->update($data);
            return redirect()->back()->with('success', 'Religion update successfully');
        endif;
    }
}
