<?php

namespace App\Http\Controllers\Admin;

use App\AjaxResponse;
use App\Http\Controllers\Controller;
use App\model\Cast;
use App\model\City;
use App\model\Profile;
use App\model\Religion;
use App\model\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    public function getAllCitiesByStateID(Request $request){
        $req_id = $request->get('id', false);
        $reqQuery = City::where('ref_state',$req_id);
        $response = new AjaxResponse();
        if($reqQuery->count() > 0){
            $response->setSuccess($reqQuery->get());
        }
        return $response->getJsonResponse();
    }
    public function viewAdd($state_id){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $state = State::find($state_id);
        if(!$state){
            return redirect()->back()->with('error', 'Invalid State...');
        }

        return view('admin-layout.pages.city.add-city',compact(['user','profile','state']));
    }
    public function addCity($state_id, Request $request){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $state = State::find($state_id);
        if(!$state){
            return redirect()->back()->with('success', 'Invalid state...');
        }

        $err_msgs = [
            'name.required' => 'Name is required.'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $name = $request->get('name');
            City::create([
                'name' => $name,
                'ref_state' => $state->id
            ]);
            return redirect()->back()->with('success', 'City added successfully');
        endif;
    }
    public function cityEdit($state_id, $city_id, Request $request){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $state = State::find($state_id);
        $city = City::where('ref_state',$state_id)->where('id',$city_id)->first();
        return view('admin-layout.pages.city.edit-city',compact(['user','profile','state','city']));
    }
    public function cityUpdate($state_id, $city_id, Request $request){
        $state = State::find($state_id);
        if($state){
            $city = City::where('ref_state', $state_id)->where('id', $city_id)->first();
            if($city) {
                $err_msgs = [
                    'name.required' => 'Name is required.'
                ];

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], $err_msgs);

                $validator->validate();

                if($validator->fails()):
                    return Redirect::back()->withErrors($validator)
                        ->withInput();
                else:
                    $data = ($request->only(['name']));
                    $city->update($data);
                    return redirect()->route('cpanel.admin.edit-state',$state_id)->with('success', 'Successfully updated cast #'.$city_id.'-'.$city->name.'...');
                endif;
            }
        }

        return redirect()->back()->with('error', 'Invalid Data');
    }
    public function deleteCity($state_id, $city_id){
        $state = State::find($state_id);
        if($state){
            $city = City::where('ref_state', $state_id)->where('id', $city_id)->first();
            if($city) {
                $city->delete();
                return redirect()->back()->with('success', 'Successfully deleted city #'.$city_id.'-'.$city->name.'...');
            }
        }

        return redirect()->back()->with('error', 'Invalid Data');
    }
}
