<?php

namespace App\Http\Controllers\Admin;

use App\model\Contact;
use App\model\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller{
    public function viewContactList(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        return view('admin-layout.pages.contact.contacts', compact(['profile']));
    }

    public function newContact(Request $request){
        $err_msgs = [
            'full_name.require' => '<b>Full Name</b> Please enter name correctly',
            'email.require' => '<b>Email</b> Please enter email correctly',
            'comment.require' => '<b>Comment</b> Please enter comment correctly',
        ];

        $validator = Validator::make($request->all(), [
            'full_name' => 'required|min:6|string|max:255',
            'email' => 'required|email',
            'comment' => 'required|min:6|string|max:255'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $data = ($validator->validated());
            Contact::create($data);
            return redirect()->back()->with('success', 'Mail Successfully Send...');
        endif;
    }
}
