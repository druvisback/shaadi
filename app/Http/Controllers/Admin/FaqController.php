<?php

namespace App\Http\Controllers\Admin;

use App\model\Faq;
use App\model\Profile;
use App\model\Testimonial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $faqs = Faq::orderBy('id','desc')->get();
        return view('admin-layout.pages.faq.index', compact(['profile','faqs']));

        
    }

    public function addfaqview(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.faq.addfaq', compact(['profile']));

    }



    public function savefaq(Request $request){
        $err_msgs =[
            'title.required' => 'Enter Your  title',
            'question.required' => 'Enter Your  question',
            'answer.required' => 'Enter Your answer',
        ];
        $only_requested_data = $request->only([
            'title','question', 'answer']);

        $validator = Validator::make($only_requested_data, [
            'title' => 'required',
            'question' => 'required',
            'answer' => 'required',

        ], $err_msgs);
        $validator->validate();

        if($request->has('id')):
            $faq_id = $request->get('id');

            $faq = Faq::find($faq_id);
            $faq->save($only_requested_data);
            return redirect()->back()->with('success','Faq successfully updated!');
        else:
            Faq::create($only_requested_data);
            return redirect()->back()->with('success','Faq successfully created!');
        endif;

        return redirect()->back()->withInput()->with('error','Please select photo!');

    }

    public function vieweditfaq($id){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $faqById = Faq::where('id', $id)->first();
        return view('admin-layout.pages.faq.editfaq', compact(['profile','faqById']));

    }

    public function updateFaq(Request $request)
    {
        //'question' => 'required',
          //  'answer' => 'required',


        $msg = [
            'title.required' => 'PLEASE TYPE YOR Title',
            'question.required' => 'PLEASE TYPE YOR QUESTION',
            'answer.required' => 'PLEASE TYPE YOUR ANSWER',

        ];
        $this->validate($request, [
            'title' => 'required',
            'question' => 'required',
            'answer' => 'required',
        ], $msg);

        $id = $request->get('id');
        $question = $request->get('question');
        $answer = $request->get('answer');

        Faq:: where('id',$id)->update([
            'title' =>  $question,
            'question' =>  $question,
            'answer' => $answer,

        ]);

        return redirect()->back()->with('success', 'Faq Updated Successfully !!!');

    }


    public function deletefaq($id){
        Faq:: where('id', $id)->delete();

        return redirect()->back()->with('success','Faq Successfully Deleted!');
    }




    public function viewEditTestimonial($id){
        $user = Auth::user();
        $testimonial = Testimonial::where('id', $id)->first();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.testimonial.edit-testimonial', compact(['profile','testimonial']));
    }

    public function updateTestimonial(Request $request)
    {

        $msg = [
            'name.required' => 'Please enter name',
            'desc.required' => 'Please enter description',
            'photo.required' => 'Please select photo',
            'country.required' => 'Please enter country name',
            'job.required' => 'Please enter job',

        ];
        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'country' => 'required',
            'job' => 'required',
        ], $msg);

        $id = $request->get('id');
        $name = $request->get('name');
        $desc = $request->get('desc');
        $country = $request->get('country');
        $job = $request->get('job');

        if($request->hasFile('photo')):
            $photo = $request->file('photo');
            $file_ext = $photo->extension();
            $current_timestamp = Carbon::now()->timestamp;

            $photo_file_name = "img_testimonial_".Auth::user()->id."_".$current_timestamp.".".$file_ext;
            $photo->storeAs(
                'public/admin/', $photo_file_name
            );
            $only_requested_data['photo_url'] = $photo_file_name;
        endif;

        Testimonial:: where('id',$id)->update([
            'name' =>  $name,
            'desc' =>  $desc,
            'country' => $country,
            'job' => $job,

        ]);

        return redirect()->back()->with('success', 'Testimonial Updated Successfully !!!');

    }




}
