<?php

namespace App\Http\Controllers\Admin;

use App\Mail\AuthMail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MailController extends Controller{
    public static $subject = "Lagan Utsav - ";

    public static function sendNotification_AccountCreationWithCredential(User $user, $new_password){
        $subject = MailController::$subject ."New User Registration";
        $message = "Your Login URL is: ".route('login')."<br>Email is : ".$user->email."<br>And<br>Password is : ".$new_password;
        new AuthMail($subject,$message);
    }
}
