<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CustomFieldController;
use App\model\Address;
use App\model\Contact;
use App\model\Faq;
use App\model\Story;
use App\model\Policy;
use App\model\Profile;
use App\model\Condition;
use App\model\Testimonial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AllpageeditController extends Controller
{


    public function viewTearms(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        return view('admin-layout.pages.terms-conditions.all', compact(['profile']));

    }

    public function addTearms(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.terms-conditions.add', compact(['profile']));

    }



    public function saveTearms(Request $request){


        $err_msgs =[
            'heading.required' => 'Enter Heading',
            'contain.required' => 'Enter Contain',

        ];


        $this->validate($request,[
            'heading' => 'required',
            'contain' => 'required',


        ], $err_msgs);
        $heading = $request->get('heading');
          $contain = $request->get('contain');


      $result=Condition::create([
              'heading' => $heading,
              'contain' => $contain,



          ]);
          $route_redirect = redirect()->back();
      if($result):
          return $route_redirect->with('success','Terms & Condition Added Successfully');
      else:
          return $route_redirect->withInputs()->with('error','Can\'t Added!');
      endif;


  }

    public function editTearms($id){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $tById = Condition::where('id', $id)->first();
        return view('admin-layout.pages.terms-conditions.edit', compact(['profile','tById']));

    }

    public function updateTearms(Request $request)
    {
        //'question' => 'required',
          //  'answer' => 'required',


        $msg = [
            'heading.required' => 'Enter Heading',
            'contain.required' => 'Enter Contain',

        ];
        $this->validate($request, [
          'heading' => 'required',
          'contain' => 'required',

], $msg);

        $id = $request->get('id');
        $heading = $request->get('heading');
        $contain = $request->get('contain');

        Condition:: where('id',$id)->update([
            'heading' =>  $heading,
            'contain' =>  $contain,


        ]);

        return redirect()->back()->with('success', 'Tearms & Condition Updated Successfully !!!');

    }

    public function deleteTearms($id){
        Condition:: where('id', $id)->delete();

        return redirect()->back()->with('success','Tearms & Condition Successfully Deleted!');
    }



//Policy Starts...
public function viewPolicy(){
    $user = Auth::user();
    $profile = Profile::where('ref_user', $user->id)->first();

    return view('admin-layout.pages.policy.all', compact(['profile']));

}

public function addPolicy(){
    $user = Auth::user();
    $profile = Profile::where('ref_user', $user->id)->first();
    return view('admin-layout.pages.policy.add', compact(['profile']));

}

public function savePolicy(Request $request){


    $err_msgs =[
        'heading.required' => 'Enter Heading',
        'contain.required' => 'Enter Contain',

    ];


    $this->validate($request,[
        'heading' => 'required',
        'contain' => 'required',


    ], $err_msgs);
    $heading = $request->get('heading');
      $contain = $request->get('contain');


  $result=Policy::create([
          'heading' => $heading,
          'contain' => $contain,



      ]);
      $route_redirect = redirect()->back();
  if($result):
      return $route_redirect->with('success','Privacy & Policy Added Successfully');
  else:
      return $route_redirect->withInputs()->with('error','Can\'t Added!');
  endif;


}

public function editPolicy($id){
    $user = Auth::user();
    $profile = Profile::where('ref_user', $user->id)->first();
    $tById = Policy::where('id', $id)->first();
    return view('admin-layout.pages.policy.edit', compact(['profile','tById']));

}

public function updatePolicy(Request $request)
{
    //'question' => 'required',
      //  'answer' => 'required',


    $msg = [
        'heading.required' => 'Enter Heading',
        'contain.required' => 'Enter Contain',

    ];
    $this->validate($request, [
      'heading' => 'required',
      'contain' => 'required',

], $msg);

    $id = $request->get('id');
    $heading = $request->get('heading');
    $contain = $request->get('contain');

    Policy:: where('id',$id)->update([
        'heading' =>  $heading,
        'contain' =>  $contain,


    ]);

    return redirect()->back()->with('success', 'Privacy & Policy Updated Successfully !!!');

}

public function deletePolicy($id){
    Policy:: where('id', $id)->delete();

    return redirect()->back()->with('success','Tearms & Condition Successfully Deleted!');
}



//story starts
public function viewStory(){
    $user = Auth::user();
    $profile = Profile::where('ref_user', $user->id)->first();
    $story_content = CustomFieldController::getValue('page_story_content')??'';
    return view('admin-layout.pages.happy-story.all', compact(['profile','story_content']));

}

public function addStory(){
    $user = Auth::user();
    $profile = Profile::where('ref_user', $user->id)->first();
    return view('admin-layout.pages.happy-story.add', compact(['profile']));
}



public function saveStory(Request $request){
    $err_msgs =[
        'content.required' => 'Enter Heading',
    ];

    $validator = Validator::make($request->all(),[
        'content' => 'required'
    ], $err_msgs);

    if($validator->fails()):
        return redirect()->back()->withErrors($validator->errors());
    else:
        $data = $validator->validate();
        $result = CustomFieldController::saveField('page_story_content',$data['content']);
        if($result):
            return redirect()->back()->with('success','Saved Story!');
        else:
            return redirect()->back()->with('error','Can\'t saved, try again!');
        endif;
    endif;
}

public function editStory($id){
    $user = Auth::user();
    $profile = Profile::where('ref_user', $user->id)->first();
    $tById = Story::where('id', $id)->first();
    return view('admin-layout.pages.happy-story.edit', compact(['profile','tById']));

}

public function updateStory(Request $request)
{
    //'question' => 'required',
      //  'answer' => 'required',


    $msg = [
        'heading.required' => 'Enter Heading',
        'contain.required' => 'Enter Contain',

    ];
    $this->validate($request, [
      'heading' => 'required',
      'contain' => 'required',

], $msg);

    $id = $request->get('id');
    $heading = $request->get('heading');
    $contain = $request->get('contain');

    Story:: where('id',$id)->update([
        'heading' =>  $heading,
        'contain' =>  $contain,


    ]);

    return redirect()->back()->with('success', 'Success story Updated Successfully !!!');

}


public function deleteStory($id){
    Story:: where('id', $id)->delete();

    return redirect()->back()->with('success','Success story Successfully Deleted!');
}





}
