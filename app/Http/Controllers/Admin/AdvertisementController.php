<?php

namespace App\Http\Controllers\Admin;

use App\model\Address;
use App\model\Contact;
use App\model\Faq;
use App\model\Profile;
use App\model\Testimonial;
use App\model\Advertisement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AdvertisementController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(){
      $user = Auth::user();
      $profile = Profile::where('ref_user', $user->id)->first();
      $faqs = Advertisement::orderBy('id','desc')->get();
      return view('admin-layout.pages.advertisement.all', compact(['profile','faqs']));
  }

  public function addaddview(){
      $user = Auth::user();
      $profile = Profile::where('ref_user', $user->id)->first();
      return view('admin-layout.pages.advertisement.add', compact(['profile']));
  }



  public function saveadd(Request $request){
    $msg =[
          'title.required' => 'Enter  title',
          'dess.required' => 'Enter Desscription  ',
          'e_link.required' => 'Enter Extarnal Link',
      ];
      $validator = Validator::make($request->all(),[
          'title' => 'required',
          'dess' => 'required',
          'e_link' => 'required',

          'photo' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',

      ], $msg);

      if(!$validator->fails()):
          $user = Auth::user();

          $data = $validator->validate();

          if($request->hasFile('photo')):
              $profile_photo = $request->file('photo');

              $file_ext = $profile_photo->extension();
              $current_timestamp = Carbon::now()->timestamp;

              $profile_photo_file_name = "img_".$user->id."_".$current_timestamp.".".$file_ext;
              $profile_photo->storeAs(
                  'public/uploads/', $profile_photo_file_name
              );
              $data['photo'] = $profile_photo_file_name;
          endif;

          $product = Advertisement::create($data);
          if($product):
              return redirect()->back()->with('success', 'Advertisement[#'.$product->id.'] added successfully');
          else:
              return redirect()->back()->with('error', 'Advertisement can\'t created, please try again!');
          endif;
      else:
          return redirect()->back()->withErrors($validator->errors())->withInput();
      endif;
  }

  public function vieweditadd($id){
      $user = Auth::user();
      $profile = Profile::where('ref_user', $user->id)->first();
      $addById = Advertisement::where('id', $id)->first();
      return view('admin-layout.pages.advertisement.edit', compact(['profile','addById']));

  }

  public function updateadd(Request $request)
  {


      $msg = [
         'title.required' => 'Enter  title',
        'dess.required' => 'Enter Desscription  ',
        'e_link.required' => 'Enter Extarnal Link',

      ];
      $this->validate($request, [
        'title' => 'required',
        'dess' => 'required',
        'e_link' => 'required',
      ], $msg);

      $id = $request->get('id');
      $title = $request->get('title');
      $dess = $request->get('dess');
      $e_link = $request->get('e_link');

      Advertisement:: where('id',$id)->update([
          'title' =>  $title,
          'title' =>  $title,
          'dess' => $dess,
          'e_link' => $e_link,

      ]);

      return redirect()->back()->with('success', 'Advertisement Updated Successfully !!!');

  }


  public function deleteadd($id){
      Advertisement:: where('id', $id)->delete();

      return redirect()->back()->with('success','Advertisement Successfully Deleted!');
  }



}
