<?php

namespace App\Http\Controllers\Admin;

use App\model\Profile;

use App\model\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PackageController extends Controller{

    public function index(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $packages = Package::orderBy('id','desc')->get();
        return view('admin-layout.pages.package.index',compact(['profile','packages']));
    }

    public function viewAddPackage(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.package.add-package',compact(['profile']));
    }

    public function addPackage(Request $request){

        $msg = [
            'name.required' => 'Enter Your Plan Name',
            'desc.required' => 'Enter Your Plan Description',
            'sale_price.required' => 'Enter Plan Price',
            'months.required' => 'Enter No Of Months',
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required | string',
            'desc' => 'required | string',
            'sale_price' => 'required | integer',
            'months' => 'required | integer',
            'caps_daily_total_view_limit' => 'integer',
            'caps_see_photos' => 'string',
            'caps_send_msgs' => 'string'
        ], $msg);

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)->withInput();
        else:
            $data = $validator->validate();
            $data['caps_daily_total_view_limit'] = (!!array_key_exists('caps_daily_total_view_limit', $data))?$data['caps_daily_total_view_limit']:-1;
            $data['caps_daily_total_view_limit'] = (int)$data['caps_daily_total_view_limit'];
            $data['caps_see_photos'] = (!!array_key_exists('caps_see_photos', $data));
            $data['caps_send_msgs'] = (!!array_key_exists('caps_send_msgs', $data));

            Package::create($data);
            return Redirect::back()->with('success', 'New package created!');
        endif;
        //dd($validator->validated());

        //return view('admin-layout.pages.package.add-package',compact(['profile']));
    }

    /*public function savePlan(Request $request){
        $msg = [
            'plan_name.required' => 'Enter Your Plan Name',
            'p_des.required' => 'Enter Your Plan Description',
            'd_mess.required' => 'Enter Daily Message Limit',
            'd_prof.required' => 'Enter Daily Message Limit',
            'price.required' => 'Enter Plan Price',
            'month.required' => 'Enter No Of Month',
        ];
        $this->validate($request, [
            'plan_name' => 'required',
            'p_des' => 'required',
            'd_mess' => 'required',
            'd_prof' => 'required',
            'price' => 'required',
            'month' => 'required',
        ], $msg);

        $plan_name = $request->get('plan_name');
        $p_des = $request->get('p_des');
        $d_mess = $request->get('d_mess');
        $d_prof = $request->get('d_prof');
        $price = $request->get('price');
        $month = $request->get('month');

        Package::create([
            'plan_name' => $plan_name,
            'p_des' => $p_des,
            'd_mess' => $d_mess,
            'd_prof' => $d_prof,
            'price' => $price,
            'month' => $month,
        ]);
        return redirect()->back()->with('success', 'Plan Added Successfully !!!');
    }

    public function editPlan($id)
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $planById = Package::where('id', $id)->first();
        return view('admin-layout.pages.plan.editplan',compact(['profile','planById']));


    }

    public function updatePlan(Request $request)
    {
        $msg = [
            'plan_name.required' => 'Enter Your Plan Name',
            'p_des.required' => 'Enter Your Plan Description',
            'd_mess.required' => 'Enter Daily Message Limit',
            'd_prof.required' => 'Enter Daily Message Limit',
            'price.required' => 'Enter Plan Price',
            'month.required' => 'Enter No Of Month',
        ];
        $this->validate($request, [
            'plan_name' => 'required',
            'p_des' => 'required',
            'd_mess' => 'required',
            'd_prof' => 'required',
            'price' => 'required',
            'month' => 'required',
        ], $msg);

        $id = $request->get('id');
        $plan_name = $request->get('plan_name');
        $p_des = $request->get('p_des');
        $d_mess = $request->get('d_mess');
        $d_prof = $request->get('d_prof');
        $price = $request->get('price');
        $month = $request->get('month');

        Package:: where('id',$id)->update([
            'plan_name' => $plan_name,
            'p_des' => $p_des,
            'd_mess' => $d_mess,
            'd_prof' => $d_prof,
            'price' => $price,
            'month' => $month,
        ]);

        return redirect()->back()->with('success', 'Plan Updated Successfully !!!');

    }

    public function delPlan($id)
    {
        Package:: where('id', $id)->delete();
        return redirect()->back()->with('success', 'Plan Deleted Successfully !!!');
    }*/

}
