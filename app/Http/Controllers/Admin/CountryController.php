<?php
namespace App\Http\Controllers\Admin;

use App\AjaxResponse;
use App\Http\Controllers\Controller;
use App\model\Cast;
use App\model\City;
use App\model\Country;
use App\model\Profile;
use App\model\Religion;
use App\model\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{
    public function viewAll(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.country.all-country',compact(['user','profile']));
    }

    public function viewEdit($country_id){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $country = Country::find($country_id);
        if(!$country->count()){
            return redirect()->back()->with('error', 'Invalid Country...');
        }
        $states = State::where('ref_country', $country_id)->get();

        return view('admin-layout.pages.country.edit-country',compact(['user','profile','country', 'states']));
    }
    public function updateCountry($country_id, Request $request){
        $country = Country::find($country_id);

        $err_msgs = [
            'name.required' => 'Name is required.'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $data = ($request->only(['name']));
            $country->update($data);
            return redirect()->back()->with('success', 'Country update successfully');
        endif;
    }

    public function viewAdd(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.country.add-country',compact(['user','profile']));
    }
    public function createCountry(Request $request){
        $err_msgs = [
            'name.required' => 'Name is required.',
            'states.required' => 'States is required.'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'states' => 'required'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $data = ($request->only(['name']));
            $states = $request->get('states');
            $arrStates = json_decode($states);
            $Country = Country::create($data);
            foreach($arrStates as $state):
                State::create([
                    'name' => $state,
                    'ref_Country' => $Country->id
                ]);
            endforeach;
            return redirect()->back()->with('success', 'Country & Related States added successfully');
        endif;
    }

    public function delete($country_id){
        $country = Country::find($country_id);
        if($country->count()){
            $related_states = State::where('ref_country', $country_id);
            $total_states = $related_states->count();
            $name = $country->name;
            $related_states->delete();
            $country->delete();
            return redirect()->back()->with('success', 'Country '.$name.'[#'.$country_id.'] with ('.$total_states.' Related States) deleted successfully...');
        }
        return redirect()->back()->with('error', 'Invalid Country...');
    }

}
