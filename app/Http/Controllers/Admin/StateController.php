<?php

namespace App\Http\Controllers\Admin;

use App\AjaxResponse;
use App\Http\Controllers\Controller;
use App\model\City;
use App\model\Country;
use App\model\Profile;
use App\model\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class StateController extends Controller
{
    public function getAllStatesByCountryID(Request $request){
        $req_id = $request->get('id', false);
        $reqQuery = State::where('ref_country',$req_id);
        $response = new AjaxResponse();
        if($reqQuery->count() > 0){
            $response->setSuccess($reqQuery->get());
        }
        return $response->getJsonResponse();
    }
    public function viewAdd($country_id){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $country = Country::find($country_id);
        if(!$country){
            return redirect()->back()->with('error', 'Invalid Country...');
        }

        return view('admin-layout.pages.state.add-state',compact(['user','profile','country']));
    }

    public function addState($country_id, Request $request){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $country = Country::find($country_id);
        if(!$country){
            return redirect()->back()->with('success', 'Invalid Country...');
        }

        $err_msgs = [
            'name.required' => 'Name is required.'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $name = $request->get('name');
            State::create([
                'name' => $name,
                'ref_country' => $country->id
            ]);
            return redirect()->route('cpanel.admin.edit-country', $country_id)->with('success', 'State added successfully');
        endif;
    }
    public function stateEdit($country_id, $state_id, Request $request){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $country = Country::find($country_id);
        $state = State::where('ref_country',$country_id)->where('id',$state_id)->first();
        $cities = City::where('ref_state', $state_id)->get();
        return view('admin-layout.pages.state.edit-state',compact(['user','profile','country','state','cities']));
    }
    public function stateUpdate($country_id, $state_id, Request $request){
        $country = Country::find($country_id);
        if($country){
            $state = State::where('ref_country', $country_id)->where('id', $state_id)->first();
            if($state) {
                $err_msgs = [
                    'name.required' => 'Name is required.'
                ];

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], $err_msgs);

                $validator->validate();

                if($validator->fails()):
                    return Redirect::back()->withErrors($validator)
                        ->withInput();
                else:
                    $data = ($request->only(['name']));
                    $state->update($data);
                    return redirect()->route('cpanel.admin.edit-country',$country_id)->with('success', 'Successfully updated State #'.$state_id.'-'.$state->name.'...');
                endif;
            }
        }

        return redirect()->back()->with('error', 'Invalid Data');
    }
    public function deleteState($country_id, $state_id){
        $country = Country::find($country_id);
        if($country){
            $state = State::where('ref_country', $country_id)->where('id', $state_id)->first();
            if($state) {
                $state->delete();
                return redirect()->back()->with('success', 'Successfully deleted state #'.$state_id.'-'.$state->name.'...');
            }
        }

        return redirect()->back()->with('error', 'Invalid Data');
    }
    public function viewAll(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.states.all-states',compact(['user','profile']));
    }
}
