<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Mail\AdminToUser;
use App\model\Kyc;
use App\model\Profile;
use App\model\Contact;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.dashboard', compact(['user', 'profile']));
    }

    public function userList()
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.user-list', compact(['user', 'profile']));
    }
    public function MaleuserList()
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.user.male-user-list', compact(['user', 'profile']));
    }


    public function NewuserList()
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.user.new-user-list', compact(['user', 'profile']));
    }

    public function activeNewUser($id)
    {
        Profile::where('id',$id)->update([
            'is_account_deactivate' => 0 ,
        ]);
        return redirect()->back()->with('success', 'Activate Successfully !!!');
    }


    public function FemaleuserList()
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.user.female-user-list', compact(['user', 'profile']));
    }

    public function KycuserList(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.user.kycMember', compact(['user', 'profile']));
    }
    public function aproveKyc($id){
       Kyc::where('id',$id)->update([
            'is_valid' => true,
           'is_check' => true
        ]);
        return redirect()->back()->with('success', 'Approved Successfully !!!');
    }
    public function disaproveKyc($id)
    {
        Kyc::where('id',$id)->update([
            'is_valid' => false,
            'is_check' => true,
        ]);
        return redirect()->back()->with('success', 'Disapproved Successfully !!!');
    }

    public function block($uid){
            $user = Profile::where('id', $uid)->first();
            if($user):
                $user->is_block = true;
                if($user->save()):
                    return redirect()->back()->with('success','User Successfully Blocked!');
                else:
                    return redirect()->back()->with('error','Can\'t Blocked User Anyway, Please try again!');
                endif;
            endif;
            return redirect()->back()->with('error','User Not Found!');
        }
        public function unblock($uid){
            $user = Profile::where('id', $uid)->first();
            if($user):
                $user->is_block = false;
                if($user->save()):
                    return redirect()->back()->with('success','User Successfully Un-blocked!');
                else:
                    return redirect()->back()->with('error','Can\'t Un-blocked User Anyway, Please try again!');
                endif;
            endif;
            return redirect()->back()->with('error','User Not Found!');
        }


    public function deactivateClient($client)
    {
        foreach ($client as $clientId)
            Client::findOrNew($clientId)->update(['status' => "0"]);
    }

    public function viewComposeMail($uid)
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $target_user = User::find($uid);
        return view('admin-layout.pages.compose-mail', compact(['user', 'profile', 'target_user']));
    }

    public function viewmessage($uid)
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $target_user = Contact::find($uid);
        return view('admin-layout.pages.compose-mail', compact(['user', 'profile', 'target_user']));
    }

    public function deleteUser($uid)
    {

        $target_user = User::find($uid);
        if ($target_user):
            $target_profile = Profile::where("ref_user", $target_user->id)->first();

            if ($target_profile):
                $target_profile->delete();
            endif;
            $target_user->delete();
        endif;
        return redirect()->route('cpanel.admin.user-list')->with('success', 'User Successfully Deleted!');
    }

    public function doComposeMail(Request $request)
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $target_email = $request->get('to');
        $subject = $request->get('subject');
        $message = $request->get('message');

        $result = Mail::to($target_email)->send(new AdminToUser($subject, $message));

        return redirect()->route('cpanel.admin.user-list')->with('success', 'Successfully Sent!');
    }

        public function doComposeMessage(Request $request)
        {
            $user = Auth::user();
            $profile = Profile::where('ref_user', $user->id)->first();

            $target_email = $request->get('to');
            $subject = $request->get('subject');
            $message = $request->get('message');

            $result = Mail::to($target_email)->send(new AdminToUser($subject, $message));

            return redirect()->route('cpanel.admin.contact-list')->with('success', 'Successfully Sent!');
        }

    public function showProfile(Request $request)
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $have_error = (!!$profile);

        $view = view('admin-layout.pages.profile', compact(['user', 'profile']));

        if (!$have_error):
            $view->with('error', "No profile found, please update your profile information.");
        endif;

        return $view;
    }

    public function updateProfile(Request $request)
    {
                $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        $data = $request->only([
            'first_name',
            'last_name',
            'gender',
            'ref_user'
        ]);

        $email = $request->get('email');


        $mail_user = User::where('email', $email)->where('email','!=',$user->email)->first();
        if($mail_user):
            $mail_profile = Profile::where('ref_user', $mail_user->id)->first();
            return redirect()->back()->withInput()->with("error", "Mail already taken by another user (".$mail_profile->first_name." ".$mail_profile->last_name.") of Lagan Utsav!");
        elseif($request->hasFile('profile_photo')):
            $profile_photo = $request->file('profile_photo');

            $file_ext = $profile_photo->extension();
            $current_timestamp = Carbon::now()->timestamp;

            $profile_photo_file_name = "img_profile_".$user->id."_".$current_timestamp.".".$file_ext;
            $profile_photo->storeAs(
                'public/profile/', $profile_photo_file_name
            );

            $data['profile_photo']=$profile_photo_file_name;
        endif;




        $is_updated = false;



        if ($profile):
            $is_updated = $profile->update($data);
        else:
            $is_updated = Profile::create($data);
        endif;


         $is_updated = $user->update([
             'email' => $email
         ]);


        if ($is_updated):
            return redirect()->back()->with("success", "Profile Updated Successfully!");
        endif;

        return redirect()->back()->withInput()->with("error", "Invalid Data!");
    }

    public function showChangePassword()
    {
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        return view('admin-layout.pages.change-password', compact(['user', 'profile']));
    }

    public function updateChangePassword(Request $request)
    {
        $user = Auth::user();
        $old_pass = $request->get('old_pass', true);
        $pass1 = $request->get('new_password', false);
        $pass2 = $request->get('cnf_password', false);
        $id = Auth::user()->id;
        $pass = User::where('id', $id)->value('password');
        if (Hash::check($old_pass, $pass)) {
            if ($pass1 == $pass2) {
                $password = Hash::make($pass1);
                $changePass = User::where('id', $id)->update([
                    'password' => $password,
                ]);
                if ($changePass == true) {
                    return redirect()->back()->with('success', "Password Updated Sucessfully !!!");
                }
            } else {
                return redirect()->back()->with('error', "New Password and Confirm Password are Not Matched !!!");
            }
        } else {
            return redirect()->back()->with('error', "Old Password Not Matched !!!");
        }
    }

    public function updateUser()
    {

        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        return view('admin-layout.pages.update', compact(['user', 'profile']));

    }

    public function updateNewuser(Request $request)
    {

        $msg = [
            'status.required' => 'Please select active or deactivated',
            'user_block.required' => 'Please select user blocked or unblocked',

        ];
        $this->validate($request, [
            'status' => 'required',
            'user_block' => 'required',
        ], $msg);

        $id = $request->get('id');
        $status = $request->get('status');
        $user_block = $request->get('user_block');


        Profile:: where('id',$id)->update([
            'status' =>  $status,
            'user_block' =>  $user_block,

        ]);

        return redirect()->back()->with('success', 'User Updated Successfully !!!');

    }




}
