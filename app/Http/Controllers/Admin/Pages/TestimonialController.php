<?php

namespace App\Http\Controllers\Admin\Pages;

use App\model\Profile;
use App\model\Testimonial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class TestimonialController extends Model{
    public function viewTestimonials(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.testimonial.testimonials', compact(['profile']));
    }

    public function viewAddTestimonial(){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();
        return view('admin-layout.pages.testimonial.add-testimonial', compact(['profile']));
    }


    public function updateTestimonial(Request $request){
        $err_msgs = [
            'name.required' => 'Please enter name',
            'desc.required' => 'Please enter description',
            'photo.required' => 'Please select photo',
            'country.required' => 'Please enter country name',
            'job.required' => 'Please enter job',
        ];

        $only_requested_data = $request->only([
            'name', 'desc', 'country', 'job'
        ]);

        $validator = Validator::make($only_requested_data, [
            'name' => 'required',
            'desc' => 'required',
            'country' => 'required',
            'job' => 'required',
        ], $err_msgs);
        $validator->validate();
        if($request->hasFile('photo')):
            $photo = $request->file('photo');
            $file_ext = $photo->extension();
            $current_timestamp = Carbon::now()->timestamp;

            $photo_file_name = "img_testimonial_".Auth::user()->id."_".$current_timestamp.".".$file_ext;
            $photo->storeAs(
                'public/admin/', $photo_file_name
            );
            $only_requested_data['photo_url'] = $photo_file_name;
        endif;

        if($request->has('id')):
            $testimonial_id = $request->get('id');

            $testimonial = Testimonial::find($testimonial_id);
            $testimonial->save($only_requested_data);
            return redirect()->back()->with('success','Testimonial successfully updated!');
        else:
            Testimonial::create($only_requested_data);
            return redirect()->back()->with('success','Testimonial successfully created!');
        endif;

        return redirect()->back()->withInput()->with('error','Please select photo!');
    }


    public function deleteTestimonial($id){
        Testimonial:: where('id', $id)->delete();

        return redirect()->back()->with('success','Testimonial Successfully Deleted!');
    }
}
