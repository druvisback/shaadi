<?php

namespace App\Http\Controllers\Admin;

use App\AjaxResponse;
use App\Http\Controllers\Controller;
use App\model\Cast;
use App\model\Profile;
use App\model\Religion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CastController extends Controller{
    public function getAllCastsByReligionID(Request $request){
        $religionID = $request->get('id', false);
        $castsQuery = Cast::where('ref_religion',$religionID);
        $response = new AjaxResponse();
        if($castsQuery->count() > 0){
            $response->setSuccess($castsQuery->get());
        }
        return $response->getJsonResponse();
    }

    public function viewAdd($religion_id){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $religion = Religion::find($religion_id);
        if(!$religion){
            return redirect()->back()->with('error', 'Invalid Religion...');
        }

        return view('admin-layout.pages.cast.add-cast',compact(['user','profile','religion']));
    }
    public function addCast($religion_id, Request $request){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $religion = Religion::find($religion_id);
        if(!$religion){
            return redirect()->back()->with('success', 'Invalid Religion...');
        }

        $err_msgs = [
            'name.required' => 'Name is required.'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], $err_msgs);

        $validator->validate();

        if($validator->fails()):
            return Redirect::back()->withErrors($validator)
                ->withInput();
        else:
            $name = $request->get('name');
            Cast::create([
                'name' => $name,
                'ref_religion' => $religion->id
            ]);
            return redirect()->route('cpanel.admin.edit-religion', $religion_id)->with('success', 'Cast added successfully');
        endif;
    }
    public function castEdit($religion_id, $cast_id, Request $request){
        $user = Auth::user();
        $profile = Profile::where('ref_user', $user->id)->first();

        $religion = Religion::find($religion_id);
        $cast = Cast::where('ref_religion',$religion_id)->where('id',$cast_id)->first();
        return view('admin-layout.pages.cast.edit-cast',compact(['user','profile','religion','cast']));
    }
    public function castUpdate($religion_id, $cast_id, Request $request){
        $religion = Religion::find($religion_id);
        if($religion){
            $cast = Cast::where('ref_religion', $religion_id)->where('id', $cast_id)->first();
            if($cast) {
                $err_msgs = [
                    'name.required' => 'Name is required.'
                ];

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], $err_msgs);

                $validator->validate();

                if($validator->fails()):
                    return Redirect::back()->withErrors($validator)
                        ->withInput();
                else:
                    $data = ($request->only(['name']));
                    $cast->update($data);
                    return redirect()->route('cpanel.admin.edit-religion',$religion_id)->with('success', 'Successfully updated cast #'.$cast_id.'-'.$cast->name.'...');
                endif;
            }
        }

        return redirect()->back()->with('error', 'Invalid Data');
    }
    public function deleteCast($religion_id, $cast_id){
        $religion = Religion::find($religion_id);
        if($religion){
            $cast = Cast::where('ref_religion', $religion_id)->where('id', $cast_id)->first();
            if($cast) {
                $cast->delete();
                return redirect()->back()->with('success', 'Successfully deleted cast #'.$cast_id.'-'.$cast->name.'...');
            }
        }

        return redirect()->back()->with('error', 'Invalid Data');
    }
}
