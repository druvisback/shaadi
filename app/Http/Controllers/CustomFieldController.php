<?php

namespace App\Http\Controllers;

use App\model\CustomField;

class CustomFieldController extends Controller{
    public static function saveField($slug, $value){
        $acf = self::getField($slug);
        if($acf):
            $acf->value = $value;
            $result = $acf->save();
        else:
            $result = CustomField::create([
                'slug' => $slug,
                'value' => $value
            ]);
        endif;

        return !!$result;
    }

    public static function getValue($slug){
        $acf = CustomField::where('slug', $slug)->first();
        return (!!$acf)?$acf->value:false;
    }

    public static function getField($slug){
        $acf = CustomField::where('slug', $slug)->first();
        return (!!$acf)?$acf:false;
    }
}
