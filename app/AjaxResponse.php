<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AjaxResponse extends Model{
    private $status_code, $status_message, $reload, $redirect_to, $data, $is_success;

    public function __construct($data = null, $status_message = "Invalid Data.", $is_success = false, $status_code = "400", $reload = false, $redirect_to = false){
        $this->status_code = $status_code;
        $this->status_message = $status_message;
        $this->reload = $reload;
        $this->redirect_to = $redirect_to;
        $this->data = $data;
        $this->is_success = $is_success;
    }





    public function setSuccess($data,$message = false, $reload = false, $redirect_to = false){
        $this->status_code = "200";
        $this->status_message = $message;
        $this->reload = $reload;
        $this->redirect_to = $redirect_to;
        $this->data = $data;
        $this->is_success = true;
    }

    public function setError400($message = "Invalid Data.", $reload = false, $redirect_to = false){
        $this->status_code = "400";
        $this->status_message = $message;
        $this->reload = $reload;
        $this->redirect_to = $redirect_to;
        $this->data = null;
        $this->is_success = false;
    }


    public function getJsonResponse(){
        $response = [
            'success' => $this->is_success,
            'status_code' => $this->status_code,
            'status_message' => $this->status_message,
            'data' => $this->data,
            'reload' => $this->reload,
            'redirect_to' => $this->redirect_to,
        ];
        return $response;
    }

    /**
     * @param string $status_code
     */
    public function setStatusCode(string $status_code): void
    {
        $this->status_code = $status_code;
    }

    /**
     * @param mixed $status_message
     */
    public function setStatusMessage($status_message): void
    {
        $this->status_message = $status_message;
    }

    /**
     * @param bool $reload
     */
    public function setReload(bool $reload): void
    {
        $this->reload = $reload;
    }

    /**
     * @param bool $redirect_to
     */
    public function setRedirectTo(bool $redirect_to): void
    {
        $this->redirect_to = $redirect_to;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @param bool $is_success
     */
    public function setIsSuccess(bool $is_success): void
    {
        $this->is_success = $is_success;
    }

}
