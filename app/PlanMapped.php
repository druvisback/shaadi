<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanMapped extends Model
{
    protected $table = 'user_plan_mapped';
    public $timestamps = true;
    protected $fillable = array('user_id', 'plan_id','month','date','price','transaction_id','status');
    protected $visible = array('user_id', 'plan_id','month', 'date','price','transaction_id','status');
}
