-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 22, 2019 at 06:49 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lara_shaadi`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `heading` text NOT NULL,
  `address` text NOT NULL,
  `admin_email_address` varchar(191) NOT NULL,
  `landline` int(20) NOT NULL,
  `mobile` int(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE `advertisements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) NOT NULL,
  `photo` varchar(191) NOT NULL,
  `dess` varchar(191) NOT NULL,
  `e_link` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advertisements`
--

INSERT INTO `advertisements` (`id`, `title`, `photo`, `dess`, `e_link`, `created_at`, `updated_at`) VALUES
(2, 'kmiusdmviut', 'img_7_1561191061.png', 'jjibmijmbji', 'jjjnjhn', '2019-06-22 02:41:01', '2019-06-22 02:41:01');

-- --------------------------------------------------------

--
-- Table structure for table `casts`
--

CREATE TABLE `casts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_religion` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `casts`
--

INSERT INTO `casts` (`id`, `name`, `ref_religion`, `created_at`, `updated_at`) VALUES
(1, 'Brahmins', 1, NULL, NULL),
(2, 'Kshatryas', 1, NULL, NULL),
(3, 'Vaisyas', 1, NULL, NULL),
(4, 'Rajput', 2, NULL, NULL),
(5, 'Sheikh', 2, NULL, NULL),
(6, 'Sudras ', 1, NULL, NULL),
(7, 'Dalits', 1, NULL, NULL),
(8, 'Mughal', 2, NULL, NULL),
(9, 'Pathan', 2, NULL, NULL),
(10, 'Darzi', 2, NULL, NULL),
(11, 'Dhobi', 2, NULL, NULL),
(12, 'None', 11, '2019-05-26 08:32:55', '2019-05-26 08:32:55'),
(13, 'Arora', 3, '2019-05-26 08:34:24', '2019-05-26 08:34:24'),
(14, 'Ramgarhia', 3, '2019-05-26 08:34:47', '2019-05-26 08:34:47'),
(15, 'Majhabi', 3, '2019-05-26 08:35:00', '2019-05-26 08:35:00'),
(16, 'Jat', 3, '2019-05-26 08:35:13', '2019-05-26 08:35:13'),
(17, 'Digamber', 5, '2019-05-26 18:54:19', '2019-05-26 18:54:19'),
(18, 'Shwetamber', 5, '2019-05-26 18:55:02', '2019-05-26 18:55:02'),
(19, 'Vania', 5, '2019-05-26 18:55:14', '2019-05-26 18:55:14'),
(20, 'Others', 5, '2019-05-26 18:55:27', '2019-05-26 18:55:27'),
(21, 'Catholic', 6, '2019-05-27 16:51:14', '2019-05-27 16:51:59'),
(22, 'Roman Catholic', 6, '2019-05-27 16:52:15', '2019-05-27 16:52:15'),
(23, 'Jacobite Protestant', 6, '2019-05-27 16:53:02', '2019-05-27 16:53:02'),
(24, 'CSI', 6, '2019-05-27 16:53:09', '2019-05-27 16:53:09'),
(25, 'Born Again', 6, '2019-05-27 16:53:17', '2019-05-27 16:53:17'),
(26, 'Pentecost', 6, '2019-05-27 16:53:31', '2019-05-27 16:53:31'),
(27, 'Pentecost', 6, '2019-05-27 16:53:31', '2019-05-27 16:53:31'),
(28, 'Marthema', 6, '2019-05-27 16:53:50', '2019-05-27 16:53:50'),
(29, 'Christian Nadar', 6, '2019-05-27 16:54:07', '2019-05-27 16:54:07'),
(30, 'Syrian Catholics', 6, '2019-05-27 16:54:22', '2019-05-27 16:54:22'),
(31, 'Syrian Orthodox', 6, '2019-05-27 16:54:35', '2019-05-27 16:54:35'),
(32, 'Latin Catholic', 6, '2019-05-27 16:54:48', '2019-05-27 16:54:56'),
(33, 'Orthodox', 6, '2019-05-27 16:55:06', '2019-05-27 16:55:06'),
(34, 'Syro Malabar', 6, '2019-05-27 16:55:28', '2019-05-27 16:55:28');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_country` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_state` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `ref_country`, `ref_state`, `created_at`, `updated_at`) VALUES
(1, 'Malda', 1, 1, NULL, NULL),
(2, 'Kolkata', 1, 1, NULL, NULL),
(4, 'siliguri', NULL, 1, '2019-06-04 11:15:45', '2019-06-04 11:15:45');

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `heading` varchar(191) NOT NULL,
  `contain` varchar(191) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `heading`, `contain`, `created_at`, `updated_at`) VALUES
(2, 'First Featurette Heading. It\'ll Blow Your Mind.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2019-06-22 12:09:22', '2019-06-22 12:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(191) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `full_name`, `email`, `comment`, `created_at`, `updated_at`) VALUES
(1, 'uithui', 'tubai.malda.120@gmail.com', 'ionfiovniovgovgb', '2019-05-23 12:06:11', '2019-05-23 12:06:11'),
(2, 'guguijijgk', 'huhhjn@gmail.com', 'higjiorgjiojiojijiobhji', '2019-05-23 12:39:19', '2019-05-23 12:39:19'),
(3, 'grhughuhurhurio', 'uhhg@gmail.com', 'fiojegjiogjiogjigjjg', '2019-05-23 12:39:35', '2019-05-23 12:39:35'),
(4, 'ggjijkgkmllkSLkkl', 'joihjiohjioji@gmail.com', 'jfjiofiojfijijfjifjifjifjifjijifjif', '2019-05-23 12:39:48', '2019-05-23 12:39:48'),
(5, 'jyotirmoy', 'jyotirmor@gmail.com', 'i am a developer', '2019-05-23 12:40:19', '2019-05-23 12:40:19'),
(6, 'rajkumar', 'raj6045@yahoo.com', 'hello, just  checking', '2019-05-27 17:11:15', '2019-05-27 17:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'India', NULL, NULL),
(2, 'Nepal', NULL, NULL),
(3, 'Bangladesh', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` text NOT NULL,
  `value` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `slug`, `value`, `created_at`, `updated_at`) VALUES
(1, 'package_free_config', 'a:1:{i:0;a:3:{s:27:\"caps_daily_total_view_limit\";i:489;s:15:\"caps_see_photos\";b:1;s:14:\"caps_send_msgs\";b:1;}}', '2019-06-22 01:07:43', '2019-06-22 02:34:58');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(3, 'Most Trusted Free Matrimony Site?', 'Most Trusted Free Matrimony Site?', 'Welcome to iMarriages, the first matrimonial site to offer a scientific approach to finding a compatible life partner. Exclusive to iMarriages, our personality matching system will help ensure that your marriage is lasting and fulfilling. Established in 2006 and with thousands of registered members, we are the most experienced free marriage bureau operating today. Include us in your journey.', '2019-05-23 12:57:18', '2019-06-08 02:50:20'),
(4, 'Success Story?', 'Success Story?', 'My name is Padma and my husband\'s name is Tavish Swaminathan. I am now Padma Swaminathan thanks to the team at iMarriages.                                I found Tavish on your matrimony site a month after I had created my profile. We began talking and organised a meeting with our families. We married here in Chennai three months later. God bless you and thank you very much iMarriages.', '2019-05-23 13:42:33', '2019-05-31 08:33:15'),
(6, 'Exclusive Matchmaking?', 'Exclusive Matchmaking?', 'Research has established that couples who are also matched on personality have more fulfilling relationships and are more successful. An individual\'s personality cannot be judged by their age, religion or appearance. Even after a brief conversation, it is difficult to determine your compatibility with another person.                                Our exclusive personality test, developed by registered doctors, will ensure you are paired with the most suitable match.', '2019-05-23 13:53:44', '2019-05-31 08:33:28');

-- --------------------------------------------------------

--
-- Table structure for table `kycs`
--

CREATE TABLE `kycs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_profile_id` bigint(20) UNSIGNED NOT NULL,
  `poi_file` varchar(255) NOT NULL,
  `poa_file` varchar(255) NOT NULL,
  `type_of_poi` varchar(255) NOT NULL,
  `type_of_poa` varchar(255) NOT NULL,
  `is_check` tinyint(1) NOT NULL DEFAULT '0',
  `is_valid` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kycs`
--

INSERT INTO `kycs` (`id`, `ref_profile_id`, `poi_file`, `poa_file`, `type_of_poi`, `type_of_poa`, `is_check`, `is_valid`, `updated_at`, `created_at`) VALUES
(5, 69, 'img_74_poi_1560497317.png', 'img_74_poa_1560497317.png', 'adhar', 'passport', 1, 0, '2019-06-14 01:59:46', '2019-06-13 23:49:24'),
(6, 63, 'img_68_poi_1560497435.png', 'img_68_poa_1560497435.png', 'passport', 'adhar', 1, 1, '2019-06-14 02:01:14', '2019-06-14 02:00:35');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Bengali', NULL, NULL),
(2, 'Hindi', NULL, NULL),
(3, 'English', NULL, NULL),
(4, 'Tamil', NULL, NULL),
(5, 'Punjabi', NULL, NULL),
(6, 'Marathi', NULL, NULL),
(8, 'Telugu', NULL, NULL),
(9, 'Gujarati', NULL, NULL),
(10, 'Malayalam', NULL, NULL),
(11, 'Kannada', NULL, NULL),
(12, 'Bihari', NULL, NULL),
(13, 'Rajasthani', NULL, NULL),
(14, 'Oriya', NULL, NULL),
(15, 'Konkani', NULL, NULL),
(16, 'Hariyanvi', NULL, NULL),
(17, 'Himachali', NULL, NULL),
(18, 'Assamese', NULL, NULL),
(19, 'Nepali', NULL, NULL),
(20, 'Marwari', NULL, NULL),
(21, 'Sindhi', NULL, NULL),
(22, 'Urdu', NULL, NULL),
(23, 'Arunachali', NULL, NULL),
(24, 'Arabic', NULL, NULL),
(25, 'Bhojpuri', NULL, NULL),
(26, 'Maithali', NULL, NULL),
(27, 'Others', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `match_preferences`
--

CREATE TABLE `match_preferences` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_profile` bigint(20) UNSIGNED NOT NULL,
  `from_age` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_age` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_height_cm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_height_cm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marital_status` enum('any','unmarried','divorced','widower') COLLATE utf8mb4_unicode_ci NOT NULL,
  `looking_for` enum('male','female','other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_religion` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_cast` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_country` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_state` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_city` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_language` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `match_preferences`
--

INSERT INTO `match_preferences` (`id`, `ref_profile`, `from_age`, `to_age`, `from_height_cm`, `to_height_cm`, `marital_status`, `looking_for`, `ref_religion`, `ref_cast`, `ref_country`, `ref_state`, `ref_city`, `ref_language`, `created_at`, `updated_at`) VALUES
(19, 24, '20', '41', '170', '182', 'any', 'female', 1, 1, 1, 1, 2, 1, '2019-05-21 12:49:21', '2019-05-21 12:49:21'),
(20, 25, '18', '23', '170', '182', 'divorced', 'male', 1, 2, 1, 1, 1, 3, '2019-05-21 12:50:01', '2019-05-21 12:50:01'),
(21, 26, '18', '23', '170', '182', 'unmarried', 'male', 1, 3, 1, 1, 2, 12, '2019-05-21 13:14:35', '2019-05-21 13:14:35'),
(22, 27, '18', '23', '170', '182', 'widower', 'female', 1, 2, 1, 1, 2, 2, '2019-05-21 13:19:04', '2019-05-21 13:19:04'),
(23, 28, '18', '23', '170', '182', 'widower', 'male', 2, 9, 1, 1, 1, 12, '2019-05-21 13:24:39', '2019-05-21 13:24:39'),
(25, 30, '18', '23', '170', '182', 'unmarried', 'male', 2, 4, 1, 1, 1, 16, '2019-05-21 13:34:20', '2019-05-21 13:34:20'),
(26, 31, '18', '23', '170', '182', 'unmarried', 'male', 1, 1, 1, 1, 1, 11, '2019-05-21 13:38:20', '2019-05-21 13:38:20'),
(27, 32, '18', '23', '170', '182', 'unmarried', 'male', 2, 4, 1, 1, 1, 1, '2019-05-21 13:42:43', '2019-05-21 13:42:43'),
(28, 33, '18', '23', '170', '182', 'unmarried', 'female', 1, 1, 1, 1, 1, 1, '2019-05-21 13:46:20', '2019-05-21 13:46:20'),
(29, 34, '18', '23', '170', '182', 'unmarried', 'female', 1, 1, 1, 1, 1, 1, '2019-05-21 13:50:47', '2019-05-21 13:50:47'),
(30, 35, '18', '23', '170', '182', 'unmarried', 'female', 1, 1, 1, 1, 1, 1, '2019-05-21 13:54:33', '2019-05-21 13:54:33'),
(31, 36, '18', '23', '170', '182', 'unmarried', 'female', 1, 2, 1, 1, 1, 1, '2019-05-21 13:59:08', '2019-05-21 13:59:08'),
(32, 37, '18', '23', '170', '182', 'unmarried', 'female', 1, 2, 1, 1, 2, 1, '2019-05-21 14:04:48', '2019-05-21 14:04:48'),
(33, 38, '18', '23', '170', '182', 'unmarried', 'female', 2, 4, 1, 1, 2, 18, '2019-05-21 14:10:30', '2019-05-21 14:10:30'),
(34, 39, '18', '23', '170', '182', 'unmarried', 'female', 2, 4, 1, 1, 2, 1, '2019-05-21 14:13:56', '2019-05-21 14:13:56'),
(35, 40, '18', '23', '170', '182', 'unmarried', 'female', 1, 3, 1, 1, 2, 16, '2019-05-21 14:18:08', '2019-05-21 14:18:08'),
(36, 41, '18', '23', '170', '182', 'divorced', 'female', 2, 8, 1, 1, 1, 18, '2019-05-21 14:21:05', '2019-05-21 14:21:05'),
(38, 43, '18', '23', '170', '182', 'unmarried', 'female', 2, 10, 1, 1, 2, 17, '2019-05-21 14:27:44', '2019-05-21 14:27:44'),
(40, 45, '18', '23', '160', '162', 'unmarried', 'female', 1, 1, 1, 1, 2, 2, '2019-05-27 17:03:18', '2019-05-27 17:03:18'),
(41, 51, '18', '23', '170', '182', 'any', 'male', 11, 12, 1, 1, 1, 1, '2019-06-01 14:22:55', '2019-06-01 14:22:55'),
(42, 63, '18', '30', '170', '182', 'unmarried', 'male', 2, 9, 1, 1, 1, 2, '2019-06-21 07:42:10', '2019-06-21 07:42:10');

-- --------------------------------------------------------

--
-- Table structure for table `memberships`
--

CREATE TABLE `memberships` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timeline_timestamp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_visible` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `member_activities`
--

CREATE TABLE `member_activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `activity_type` enum('view_profile','view_profile_photo','chat_with','blocked_by') NOT NULL,
  `from_user` bigint(20) UNSIGNED NOT NULL,
  `to_user` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_03_000000_create_religions_table', 1),
(4, '2019_05_03_000001_create_casts_table', 1),
(5, '2019_05_03_000003_create_languages_table', 1),
(6, '2019_05_03_000004_create_countries_table', 1),
(7, '2019_05_03_000005_create_memberships_table', 1),
(8, '2019_05_03_000006_create_orders_table', 1),
(9, '2019_05_03_062832_create_profiles_table', 1),
(10, '2019_05_07_104118_create_match_preferences_table', 2),
(11, '2019_05_07_130737_create_states_table', 2),
(12, '2019_05_07_130816_create_cities_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_package_id` int(10) UNSIGNED NOT NULL,
  `ref_user_id` int(10) UNSIGNED NOT NULL,
  `start_at` datetime DEFAULT NULL,
  `expire_at` datetime DEFAULT NULL,
  `status` enum('pending','processing','applied','expired') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `transaction_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sale_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `coupons` text COLLATE utf8mb4_unicode_ci,
  `caps_daily_total_view_limit` int(11) DEFAULT '0',
  `caps_see_photos` tinyint(1) DEFAULT '0',
  `caps_send_msgs` tinyint(1) DEFAULT '0',
  `months` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `desc`, `sale_price`, `basic_discount`, `coupons`, `caps_daily_total_view_limit`, `caps_see_photos`, `caps_send_msgs`, `months`, `created_at`, `updated_at`) VALUES
(7, 'Gold Package', 'huidcndc', '554', '0', NULL, 489, 1, 1, 88, '2019-06-14 05:08:13', '2019-06-14 05:08:13');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('RAJ6045@YAHOO.COM', '$2y$10$vv7YVBaDjPyczDL2bQ8Xuuh/dE8frISmymemMb/.81xBH6x.ltXtW', '2019-05-16 07:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `policies`
--

CREATE TABLE `policies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `heading` varchar(191) NOT NULL,
  `contain` varchar(191) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `policies`
--

INSERT INTO `policies` (`id`, `heading`, `contain`, `created_at`, `updated_at`) VALUES
(1, 'First Featurette Heading. It\'ll Blow Your Mind.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2019-06-10 00:00:00', '2019-06-12 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `profile_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_profile_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_user` bigint(20) UNSIGNED NOT NULL,
  `ref_religion` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_cast` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_country` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_state` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_city` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_country_grew_up_in` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_language` bigint(20) UNSIGNED DEFAULT NULL,
  `ref_current_order` bigint(20) UNSIGNED DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female','other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_for` enum('father','mother','son','daughter','sister','brother','me','relative') COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_photos` text COLLATE utf8mb4_unicode_ci,
  `family_father_status` text COLLATE utf8mb4_unicode_ci,
  `family_mother_status` text COLLATE utf8mb4_unicode_ci,
  `family_description` text COLLATE utf8mb4_unicode_ci,
  `family_no_of_sister` int(10) UNSIGNED DEFAULT NULL,
  `family_no_of_brother` int(10) UNSIGNED DEFAULT NULL,
  `self_description` text COLLATE utf8mb4_unicode_ci,
  `health_description` text COLLATE utf8mb4_unicode_ci,
  `mobile_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relationship_with_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_to_call` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `last_education_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_education_institude_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_education_passing_year` date DEFAULT NULL,
  `marital_status` enum('unmarried','divorced','widow') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body_type` enum('athlete','average','slim') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sun_sign` enum('virgo','leo','libra','scorpio','cancer','gemini','taurus','aries','pisces','aquarius','capricorn','sagittarius') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diet` enum('veg','nonveg','both') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drink` enum('occasionally','regular','no','heavy') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smoke` enum('occasionally','regular','no','heavy') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_group` enum('a-','a+','b-','b+','ab-','ab+','o+','o-') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight_kg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `have_disability` tinyint(1) NOT NULL DEFAULT '0',
  `is_accept_terms` tinyint(1) NOT NULL DEFAULT '0',
  `is_complete_profile` tinyint(1) NOT NULL DEFAULT '0',
  `is_complete_match_preference` tinyint(1) NOT NULL DEFAULT '0',
  `is_profile_verified` tinyint(1) NOT NULL DEFAULT '0',
  `is_in_search_result` tinyint(1) NOT NULL DEFAULT '0',
  `is_block` tinyint(1) NOT NULL DEFAULT '1',
  `is_mail_verified` tinyint(1) NOT NULL DEFAULT '0',
  `is_kyc_submitted` tinyint(1) NOT NULL DEFAULT '0',
  `have_privacy_basic_lifestyle` tinyint(1) NOT NULL DEFAULT '0',
  `have_privacy_family_details` tinyint(1) NOT NULL DEFAULT '0',
  `have_privacy_contact_details` tinyint(1) NOT NULL DEFAULT '0',
  `have_privacy_location_details` tinyint(1) NOT NULL DEFAULT '0',
  `have_privacy_gallery_photo` tinyint(1) NOT NULL DEFAULT '0',
  `have_privacy_dp` tinyint(1) NOT NULL DEFAULT '0',
  `have_privacy_educational_details` tinyint(1) NOT NULL DEFAULT '0',
  `is_account_deactivate` tinyint(1) NOT NULL DEFAULT '0',
  `err_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Incomplete Profile',
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Inactive',
  `user_block` enum('Blocked','Unblocked') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Unblocked',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `profile_id`, `referral_profile_id`, `ref_user`, `ref_religion`, `ref_cast`, `ref_country`, `ref_state`, `ref_city`, `ref_country_grew_up_in`, `ref_language`, `ref_current_order`, `first_name`, `last_name`, `gender`, `profile_for`, `profile_photo`, `gallery_photos`, `family_father_status`, `family_mother_status`, `family_description`, `family_no_of_sister`, `family_no_of_brother`, `self_description`, `health_description`, `mobile_no`, `business_email`, `relationship_with_member`, `time_to_call`, `contact_person`, `dob`, `last_education_name`, `last_education_institude_name`, `last_education_passing_year`, `marital_status`, `body_type`, `sun_sign`, `diet`, `drink`, `smoke`, `blood_group`, `height_cm`, `weight_kg`, `have_disability`, `is_accept_terms`, `is_complete_profile`, `is_complete_match_preference`, `is_profile_verified`, `is_in_search_result`, `is_block`, `is_mail_verified`, `is_kyc_submitted`, `have_privacy_basic_lifestyle`, `have_privacy_family_details`, `have_privacy_contact_details`, `have_privacy_location_details`, `have_privacy_gallery_photo`, `have_privacy_dp`, `have_privacy_educational_details`, `is_account_deactivate`, `err_message`, `status`, `user_block`, `created_at`, `updated_at`) VALUES
(24, 'LU-M24', NULL, 28, 1, 2, 1, 1, 1, 1, 1, NULL, 'Vicky', 'Oberoy', 'male', 'me', 'img_profile_28_1558443883.jpeg', 'a:1:{i:0;s:22:\"img_28_1558443944.jpeg\";}', 'Perfect..............................', 'Perfect..............................', 'Perfect..............................', 8, 5, 'I\'m perfect..............................', 'I\'m perfect..............................', '8016946009', 'vicky99407@gmail.com', 'brother', 'Perfect..............................', 'Perfect..............................', '1992-12-15', '', '', NULL, 'unmarried', 'athlete', 'cancer', 'veg', 'heavy', 'heavy', 'b+', '175', '82', 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Active', 'Unblocked', '2019-05-21 12:48:28', '2019-06-22 00:22:38'),
(25, '', NULL, 29, 1, 2, 1, 1, 2, 1, 3, NULL, 'prianka', 'ghosh', 'female', 'me', 'img_profile_29_1558443213.jpeg', 'a:3:{i:0;s:22:\"img_29_1558443229.jpeg\";i:1;s:22:\"img_29_1558443236.jpeg\";i:2;s:22:\"img_29_1558443245.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 1, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '7501353298', 'prianka@gmail.com', 'sister', '10 Am To 9 Pm .......................', 'mom,dad,brothers,uncle...........', '1994-07-16', '', '', NULL, 'unmarried', 'average', 'virgo', 'both', 'no', 'no', 'b-', '175', '55', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Active', 'Unblocked', '2019-05-21 12:49:15', '2019-05-21 12:54:05'),
(26, '', NULL, 30, 1, 3, 1, 1, 1, 1, 5, NULL, 'neetu', 'bose', 'female', 'me', 'img_profile_30_1558444614.jpeg', 'a:3:{i:0;s:22:\"img_30_1558444627.jpeg\";i:1;s:22:\"img_30_1558444635.jpeg\";i:2;s:22:\"img_30_1558444641.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 1, 1, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '564654565', 'neetu@gmail.com', 'brother', '10 Am To 9 Pm .......................', 'mom,dad,brothers,uncle...........', '1992-11-20', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '185', '59', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Active', 'Unblocked', '2019-05-21 13:13:38', '2019-05-21 13:17:21'),
(27, '', NULL, 31, 1, 1, 1, 1, 1, 1, 1, NULL, 'harpreet', 'bharati', 'female', 'me', 'img_profile_31_1558444905.jpeg', 'a:3:{i:0;s:22:\"img_31_1558444920.jpeg\";i:1;s:22:\"img_31_1558444925.jpeg\";i:2;s:22:\"img_31_1558444930.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '548978965', 'harpreet@gmail.com', 'mother', '10 Am To 9 Pm .......................', 'mom,dad,brothers,uncle...........', '1995-06-21', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '122', '43', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Blocked', '2019-05-21 13:18:32', '2019-05-21 13:22:10'),
(28, '', NULL, 32, 2, 4, 1, 1, 1, 1, 1, NULL, 'prianka', 'som', 'female', 'me', 'img_profile_32_1558445275.jpeg', 'a:3:{i:0;s:22:\"img_32_1558445296.jpeg\";i:1;s:22:\"img_32_1558445301.jpeg\";i:2;s:22:\"img_32_1558445307.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '5241564845', 'prianka1@gmail.com', 'father', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1993-02-17', '', '', NULL, 'unmarried', 'average', 'virgo', 'veg', 'no', 'no', 'a+', '155', '45', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 13:24:02', '2019-05-21 13:28:27'),
(30, '', NULL, 34, 2, 8, 1, 1, 1, 1, 17, NULL, 'sonai', 'mandal', 'female', 'me', 'img_profile_34_1558445805.jpeg', 'a:3:{i:0;s:22:\"img_34_1558445819.jpeg\";i:1;s:22:\"img_34_1558445824.jpeg\";i:2;s:22:\"img_34_1558445831.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '5656456651', 'sonai@gmail.com', 'mother', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1999-07-22', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '188', '59', 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 'Please verify your email first.', 'Active', 'Unblocked', '2019-05-21 13:33:51', '2019-05-21 13:37:11'),
(31, '', NULL, 35, 1, 1, 1, 1, 2, 1, 20, NULL, 'shamali', 'haldar', 'female', 'me', 'img_profile_35_1558446043.jpeg', 'a:3:{i:0;s:22:\"img_35_1558446057.jpeg\";i:1;s:22:\"img_35_1558446065.jpeg\";i:2;s:22:\"img_35_1558446075.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '255485454', 'shamali@321', 'brother', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1991-02-13', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'both', 'no', 'no', 'a+', '165', '60', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 13:37:56', '2019-05-21 13:41:15'),
(32, '', NULL, 36, 2, 10, 1, 1, 1, 1, 17, NULL, 'mili', 'khatun', 'female', 'me', 'img_profile_36_1558446274.jpeg', 'a:3:{i:0;s:22:\"img_36_1558446283.jpeg\";i:1;s:22:\"img_36_1558446288.jpeg\";i:2;s:22:\"img_36_1558446294.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '41549869287', 'mili@gmail.com', 'mother', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1993-05-04', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '158', '45', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 13:42:18', '2019-05-21 13:44:54'),
(33, '', NULL, 37, 1, 1, 1, 1, 1, 1, 1, NULL, 'rajib', 'saha', 'female', 'daughter', 'img_profile_37_1558446514.jpeg', 'a:3:{i:0;s:22:\"img_37_1558446526.jpeg\";i:1;s:22:\"img_37_1558446533.jpeg\";i:2;s:22:\"img_37_1558446540.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '212145564', 'rajib@gmail.com', 'mother', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1989-06-07', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '178', '45', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 13:45:53', '2019-05-21 13:49:00'),
(34, '', NULL, 38, 1, 1, 1, 1, 1, 1, 15, NULL, 'ranjit', 'ghosh', 'male', 'son', 'img_profile_38_1558446763.jpeg', 'a:3:{i:0;s:22:\"img_38_1558446773.jpeg\";i:1;s:21:\"img_38_1558446785.png\";i:2;s:22:\"img_38_1558446797.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '45498489', 'ranjit@gmail.com', 'friend', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1997-10-22', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '185', '65', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 13:50:14', '2019-05-21 13:53:17'),
(35, '', NULL, 39, 1, 1, 1, 1, 2, 1, 1, NULL, 'soham', 'roy', 'male', 'me', 'img_profile_39_1558446991.jpeg', 'a:3:{i:0;s:21:\"img_39_1558447007.png\";i:1;s:22:\"img_39_1558447020.jpeg\";i:2;s:22:\"img_39_1558447027.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '4554565654', 'soham@gmail.com', 'father', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1991-05-21', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '187', '49', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 13:54:11', '2019-05-21 13:57:07'),
(36, '', NULL, 40, 1, 2, 1, 1, 2, 1, 1, NULL, 'rahul', 'das', 'male', 'me', 'img_profile_40_1558447297.png', 'a:3:{i:0;s:22:\"img_40_1558447314.jpeg\";i:1;s:21:\"img_40_1558447332.png\";i:2;s:22:\"img_40_1558447340.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '4455445451', 'rahul@gmail.com', 'mother', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1991-10-22', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '165', '50', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 13:58:49', '2019-05-21 14:02:20'),
(37, '', NULL, 41, 1, 3, 1, 1, 1, 1, 4, NULL, 'himesh', 'raj', 'male', 'me', 'img_profile_41_1558447599.jpeg', 'a:3:{i:0;s:21:\"img_41_1558447611.png\";i:1;s:22:\"img_41_1558447618.jpeg\";i:2;s:22:\"img_41_1558447627.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '54356665546', 'himesh@gmail.com', 'mother', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1990-06-21', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '178', '58', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 14:04:30', '2019-05-21 14:07:07'),
(38, '', NULL, 42, 2, 4, 1, 1, 2, 1, 2, NULL, 'tanna', 'hk', 'male', 'me', 'img_profile_42_1558447937.jpeg', 'a:2:{i:0;s:22:\"img_42_1558447950.jpeg\";i:1;s:22:\"img_42_1558447959.jpeg\";}', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '42543215454', 'tanna@gmail.com', 'father', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1989-07-07', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '188', '56', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 14:10:03', '2019-05-21 14:12:39'),
(39, '', NULL, 43, 2, 4, 1, 1, 1, 1, 1, NULL, 'param', 'sarkar', 'male', 'me', 'img_profile_43_1558448154.jpeg', NULL, 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '5455655656', 'param@gmail.com', 'brother', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1997-10-15', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '177', '45', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 14:13:38', '2019-05-21 14:15:54'),
(40, '', NULL, 44, 2, 5, 1, 1, 1, 1, 14, NULL, 'amir', 'khan', 'female', 'me', 'img_profile_44_1558448396.jpeg', NULL, 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '575455454854', 'amir@gmail', 'mother', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1993-06-17', '', '', NULL, 'divorced', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '144', '58', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 14:17:33', '2019-05-21 14:19:56'),
(41, '', NULL, 45, 2, 9, 1, 1, 2, 1, 14, NULL, 'amir', 'khan', 'male', 'me', 'img_profile_45_1558448568.jpeg', NULL, 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '65454845454546', 'amir1@gmail.com', 'mother', '10 Am to 6 Pm.........................', 'momo..............................', '1992-05-10', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '189', '85', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 14:20:41', '2019-05-21 14:22:48'),
(43, '', NULL, 47, 2, 4, 1, 1, 2, 1, 1, NULL, 'imteaj', 'sarkar', 'male', 'me', 'img_profile_47_1558448988.jpeg', NULL, 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'My parents believe that I am a bright child with good skills and that I am capable of doing well in my academics as they feel that I am an active child.', 'When raising a child one is taught values by their families that they feel are important for their child to have.', 0, 0, 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible.', 'Health information is the data related to a person’s medical history, including symptoms, diagnoses, procedures, and outcomes. Health information records include patient histories, lab results, x-rays, clinical information, and notes.', '56456548', 'imteaj@gmail.com', 'father', '10 Am to 6 Pm.........................', 'mom,dad,brothers,uncle...........', '1993-06-18', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '178', '47', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-21 14:27:10', '2019-05-21 14:29:48'),
(45, '', NULL, 49, 1, 1, 1, 1, 2, 1, 2, NULL, 'raj', 'kumar', 'male', 'me', 'img_profile_49_1558976534.jpeg', 'a:1:{i:0;s:22:\"img_49_1558976554.jpeg\";}', 'RETD...............................................................', 'HW..............................................................', 'LIBERAL..................................................', 2, 1, 'FINE....................................................................', 'GOOD..........................................................', '9971671636', 'raj6045@yahoo.com', 'self', '7..................................................', 'self................................................', '1971-11-27', '', '', NULL, 'unmarried', 'athlete', 'sagittarius', 'veg', 'no', 'no', 'ab+', '166', '66', 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-27 16:57:17', '2019-05-28 16:46:38'),
(47, '', NULL, 51, 1, 1, 1, NULL, NULL, NULL, 1, NULL, 'jyoti', 'jyu8h', 'male', 'me', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-30 12:01:28', '2019-05-30 12:01:28'),
(50, '', NULL, 55, 1, 1, 1, 1, 1, 1, 1, NULL, 'jyotirmoy', 'bhattacharjee', 'male', 'me', NULL, NULL, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 1, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '750214578', 'jyoti@gmail.com', 'father', NULL, NULL, '1992-03-13', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '172', '45', 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-05-31 07:12:54', '2019-05-31 07:23:04'),
(51, '', NULL, 56, 1, 1, 1, 1, 1, 1, 1, NULL, 'jyoti', 'bhatta', 'male', 'me', NULL, NULL, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 1, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '147414884', 'bhatta@gmail.com', 'father', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '1993-06-23', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '174', '45', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-06-01 11:53:16', '2019-06-01 14:22:55'),
(53, '', NULL, 58, 1, 1, 1, 1, 2, 1, 1, NULL, 'tubai', 'bhattacharjee', 'male', 'me', NULL, NULL, 'a', 'a', 'a', 1, 1, 'a', 'a', '216545616546541', 'tubai.malda.120@gmail.com', 'father', 'a', 'a', '1975-03-07', '', '', NULL, 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '147', '45', 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-06-04 13:32:09', '2019-06-08 01:29:52'),
(63, 'LUM68', NULL, 68, 6, 24, 1, 1, 2, 1, 1, NULL, 'Anirban', 'Sanyal', 'male', 'me', 'img_profile_68_1561122622.webp', NULL, 'Lorem ipsum dolor sit amet.....', 'Lorem ipsum dolor sit amet.....', 'Lorem ipsum dolor sit amet.....', 7, 6, 'Lorem ipsum dolor sit amet.....', 'Lorem ipsum dolor sit amet.....', '8016946009', 'druvisback@gmail.com', 'mother', 'Lorem ipsum dolor sit amet.....', 'Lorem ipsum dolor sit amet.....', '1992-12-10', '', '', NULL, 'unmarried', 'athlete', 'scorpio', 'both', 'no', 'occasionally', 'a+', '185', '82', 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-06-08 13:46:35', '2019-06-21 07:42:10'),
(69, 'LUM74', 'LU-M24', 74, 1, 3, 1, 1, 1, 1, 3, NULL, 'Anirban', 'Sanyal', 'male', 'sister', NULL, 'a:0:{}', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 1, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '89789479878', 'druvisback.info@gmail.com', 'father', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '1991-09-26', 'h.s', 'malda zilla school', '2019-06-04', 'unmarried', 'athlete', 'virgo', 'veg', 'no', 'no', 'a+', '174', '65', 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'Please verify your email first.', 'Inactive', 'Unblocked', '2019-06-13 16:13:56', '2019-06-22 01:28:10'),
(70, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Lagan', 'Utsav - Admin', 'male', 'father', 'img_profile_7_1561193610.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Incomplete Profile', 'Inactive', 'Unblocked', '2019-06-14 00:45:03', '2019-06-22 03:23:30');

-- --------------------------------------------------------

--
-- Table structure for table `religions`
--

CREATE TABLE `religions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `religions`
--

INSERT INTO `religions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Hindu', NULL, NULL),
(2, 'Muslim', NULL, NULL),
(3, 'Sikh', NULL, NULL),
(5, 'Jain', NULL, NULL),
(6, 'Christian', NULL, NULL),
(7, 'Jewish', NULL, NULL),
(8, 'Bahai', NULL, NULL),
(9, 'Spiritual', NULL, '2019-05-26 08:31:22'),
(10, 'Atheist', NULL, NULL),
(11, 'Others', '2019-05-26 08:32:55', '2019-05-26 08:32:55');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_country` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `ref_country`, `created_at`, `updated_at`) VALUES
(1, 'West Bengal', 1, NULL, '2019-06-04 11:11:28'),
(2, 'Delhi', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE `stories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `heading` varchar(191) NOT NULL,
  `contain` varchar(191) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`id`, `heading`, `contain`, `created_at`, `updated_at`) VALUES
(1, 'First Featurette Heading. It\'ll Blow Your Mind.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '2019-06-04 00:00:00', '2019-06-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `photo_url` varchar(255) DEFAULT NULL,
  `desc` text,
  `name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `photo_url`, `desc`, `name`, `country`, `job`, `created_at`, `updated_at`) VALUES
(3, 'img_testimonial_7_1558449198.png', 'Awesome service, i found my wife..', 'kusal1', 'india', 'software developer', '2019-05-21 14:33:18', '2019-06-06 03:06:43'),
(4, NULL, 'Nice services..', 'Raj', 'India.', 'Software Engineer..', '2019-05-26 18:58:31', '2019-06-06 03:07:48'),
(5, 'img_testimonial_7_1559413785.jpeg', 'hiiiiiiiiiiiiiiiiiiiiii', 'jyotirmoy', 'india', 'developer', '2019-06-01 12:59:45', '2019-06-01 12:59:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` enum('admin','member','subscriber') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'member'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `type`) VALUES
(7, 'admin@webinch.com', NULL, '$2y$10$rL4Qf.DvignIcQIXhkOAm.cNOS9pGZT7GVvnCWEmUvEIxMf1YEtOG', 'HvWBRI7SlGF5tu4mwlRbtmsdGEDEHENJYYloXBZF8voVnnfF4rcCZkfj12RX', '2019-05-03 06:43:42', '2019-06-14 01:00:34', 'admin'),
(28, 'vicky99407@gmail.com', NULL, '$2y$10$ZhwBJgkI0LUinnIekR0aruFy57riVw0qYXTR3nAdz8LcrEsQmGpZi', NULL, '2019-05-21 12:48:28', '2019-05-21 12:48:28', 'member'),
(29, 'prianka@gmail.com', NULL, '$2y$10$jww2z4PFxjWKiV6nlkPXSes/ioMGyTxu8n42NMc1P/qfP8QaQaXI.', NULL, '2019-05-21 12:49:15', '2019-05-21 12:49:15', 'member'),
(30, 'neetu@gmail.com', NULL, '$2y$10$oDPZhG5/Y6HGALZjC2uMzOlf9GC9j370CVDKIlTBa11Xi.F6f69jC', NULL, '2019-05-21 13:13:38', '2019-05-21 13:13:38', 'member'),
(31, 'harpreet@gmail.com', NULL, '$2y$10$0kSf2Y0pSI579f9hV5T4yOkH5xHCHDfi.zS7fW4enJ2Dsn88txNbW', NULL, '2019-05-21 13:18:32', '2019-05-21 13:18:32', 'member'),
(32, 'prianka1@gmail.com', NULL, '$2y$10$XZ4B6gKW.GhqBkElm0U.8uFUCNK4kkQpBpCxa9O7idapMDE/QPhDi', NULL, '2019-05-21 13:24:02', '2019-05-21 13:24:02', 'member'),
(34, 'sonai@gmail.com', NULL, '$2y$10$1JsKNrEaK8WrtiHR9lIkGukDVvzZhrdFxx27eGELkh07pH4s/XOdy', NULL, '2019-05-21 13:33:51', '2019-05-21 13:33:51', 'member'),
(35, 'shamali@321', NULL, '$2y$10$avsTdLxfixakAvB3dNP6r.0ufVvd..fpKhZFWpODhe56MUrAGmVV.', NULL, '2019-05-21 13:37:56', '2019-05-21 13:37:56', 'member'),
(36, 'mili@gmail.com', NULL, '$2y$10$xHfsEGH4.7Y91cStrJHjyeMFCvApL6yuDJsXLnvet/o4PuXrldq26', NULL, '2019-05-21 13:42:18', '2019-05-21 13:42:18', 'member'),
(37, 'rajib@gmail.com', NULL, '$2y$10$w22TWab4nvjsmX88hyRBtuJE77JDM.15IwrA5NPt.XGWWEIG99evi', NULL, '2019-05-21 13:45:53', '2019-05-21 13:45:53', 'member'),
(38, 'ranjit@gmail.com', NULL, '$2y$10$IFzpoKuiEn7idQiRke2LMejn8mY9A7fKuDczCdu9Q/RsM3u./BD4q', NULL, '2019-05-21 13:50:14', '2019-05-21 13:50:14', 'member'),
(39, 'soham@gmail.com', NULL, '$2y$10$XAVXMRvapERA4KZHovirsuTwXDJb84umCrSsk2IQAG/ZX2eEEMopi', NULL, '2019-05-21 13:54:11', '2019-05-21 13:54:11', 'member'),
(40, 'rahul@gmail.com', NULL, '$2y$10$tOQnLUqqKHRA4f3HpYbVt.3TBDJzdjSWAbmhPRoYzMZdWi3LMjXBm', NULL, '2019-05-21 13:58:49', '2019-05-21 13:58:49', 'member'),
(41, 'himesh@gmail.com', NULL, '$2y$10$wOMR.RRRohY/UVpta8vQqufgBOyvEW4kqpYn2Oo6DXRHcJ7fX59DC', NULL, '2019-05-21 14:04:30', '2019-05-21 14:04:30', 'member'),
(42, 'tanna@gmail.com', NULL, '$2y$10$5vl7o.zZM2zd6UGWt/vUy.MpbR1WijEHW3M7Qgb9cd3lWNxzXuOgy', NULL, '2019-05-21 14:10:03', '2019-05-21 14:10:03', 'member'),
(43, 'param@gmail.com', NULL, '$2y$10$V3zWjmQ7gdeVAl32cukyQemTJs2zoI.0f4X6qtD0MZOUxCxDsalEm', NULL, '2019-05-21 14:13:38', '2019-05-21 14:13:38', 'member'),
(44, 'member@gmail.com', NULL, '$2y$10$IiAcdxRs4/EY8kgxAOtii.Nm6i0HGkZEhQWv50hMp2WMew8CuNrVi', 'VfUIhjxpCJvXHaakINOQQCNQvj8Lq7WYCkwFrec5I1i0SIIH2Js30fGOcrvS', '2019-05-21 14:17:33', '2019-06-08 07:44:36', 'member'),
(45, 'amir1@gmail.com', NULL, '$2y$10$iNhPuEHRgt8zneDEV9dcmehntYg7KbM8FkMwuPLgF2tY6xCuOXkpy', NULL, '2019-05-21 14:20:41', '2019-05-21 14:20:41', 'member'),
(47, 'imteaj@gmail.com', NULL, '$2y$10$ztVbDwu9y7mAAmzxOSDNOuq7N2/girNdrl3VpkxK7Txh/GpHsx7F2', NULL, '2019-05-21 14:27:10', '2019-05-21 14:27:10', 'member'),
(49, 'raj6045@yahoo.com', NULL, '$2y$10$XN8.WpW9fubIdvwAWzeAm.4lDtveMvdT/TjWQgaIakmcNX6QtFSte', NULL, '2019-05-27 16:57:17', '2019-05-27 16:57:17', 'member'),
(51, 'ughiuhg@gmail.com', NULL, '$2y$10$GGX0jDxhMk6sFK2pEUON7O5ReU5MpYCMl3FTxQUlyTHM6q6ECTn16', NULL, '2019-05-30 12:01:28', '2019-05-30 12:01:28', 'member'),
(55, 'jyoti@gmail.com', NULL, '$2y$10$1FDSR0o5Lin0nTw2rkDlz.mrgSLBFOzBP5nEebd/929CDG81z.VFS', NULL, '2019-05-31 07:12:54', '2019-05-31 07:12:54', 'member'),
(56, 'bhatta@gmail.com', NULL, '$2y$10$XcNMPbSeKk2t4jHO/0k.VeZ7x8m1VLNzcAKY/9gfqhPykeZkBYPGS', NULL, '2019-06-01 11:53:16', '2019-06-01 11:53:16', 'member'),
(58, 'tubai.malda.120@gmail.com', NULL, '$2y$10$bI/TtIHqTbxSBHAAlAHiUuloGkCYWzA.Mdsu9.xlS7SaHkTTG6NgW', NULL, '2019-06-04 13:32:09', '2019-06-04 13:32:09', 'member'),
(59, 'sipn@gmail.com', NULL, '$2y$10$I2HYF5IPfYoZVbWVgED4EufG6SgAuoqfLe3jk051/SgQjbBE7kBOy', NULL, '2019-06-06 11:57:14', '2019-06-06 11:57:14', 'member'),
(60, 'jyoti15215@gmail.com', NULL, '$2y$10$E59lPxvXVs0nVdglMBZwYutlUR3H4kYMjYtHgcvD3QkA2NEsQGnrm', NULL, '2019-06-08 11:44:29', '2019-06-08 11:44:29', 'member'),
(68, 'druvisback@gmail.com', '2019-06-08 13:58:05', '$2y$10$8YgCCcJR/vO4o0AJcZPGZ.PPUnsDKFvLT8l.YA2waljsuHRYx4lHG', '96WxZyv8AdGd9uuk2H7nbbGMmjZNZgNbbcwTLWbCk06gm5n3UJfujUvigpSw', '2019-06-08 13:46:35', '2019-06-08 13:58:05', 'member'),
(74, 'druvisback.info@gmail.com', '2019-06-14 00:10:33', '$2y$10$6WIroP2tIEXjB/lOYzwg1eJgd/p32E.coiN0VMjKLTcrx8s.HXsJ6', 'K300DNiLdoH7KQ2QUNNHZH7HMGjuHhM3DDrrVCvaAJi1NtFM2PDUDdWRB9AV', '2019-06-13 16:13:56', '2019-06-21 23:21:35', 'member');

-- --------------------------------------------------------

--
-- Table structure for table `user_plan_mapped`
--

CREATE TABLE `user_plan_mapped` (
  `id` int(10) UNSIGNED NOT NULL,
  `plan_id` int(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `date` date NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Pending','Completed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_plan_mapped`
--

INSERT INTO `user_plan_mapped` (`id`, `plan_id`, `user_id`, `month`, `date`, `price`, `transaction_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 6, 5, 1, '2019-06-30', '500.00', NULL, 'Pending', '2019-05-30 07:44:57', '2019-05-30 07:44:57'),
(2, 4, 5, 20, '2021-01-30', '12347.00', NULL, 'Pending', '2019-05-30 10:25:53', '2019-05-30 10:25:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `casts`
--
ALTER TABLE `casts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `casts_ref_religion_foreign` (`ref_religion`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_ref_country_foreign` (`ref_country`),
  ADD KEY `cities_ref_state_foreign` (`ref_state`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kycs`
--
ALTER TABLE `kycs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_profile_id` (`ref_profile_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `match_preferences`
--
ALTER TABLE `match_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `match_preferences_ref_profile_foreign` (`ref_profile`),
  ADD KEY `match_preferences_ref_religion_foreign` (`ref_religion`),
  ADD KEY `match_preferences_ref_cast_foreign` (`ref_cast`),
  ADD KEY `match_preferences_ref_country_foreign` (`ref_country`),
  ADD KEY `match_preferences_ref_language_foreign` (`ref_language`);

--
-- Indexes for table `memberships`
--
ALTER TABLE `memberships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_activities`
--
ALTER TABLE `member_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_user` (`from_user`),
  ADD KEY `to_user` (`to_user`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `policies`
--
ALTER TABLE `policies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_ref_user_foreign` (`ref_user`),
  ADD KEY `profiles_ref_religion_foreign` (`ref_religion`),
  ADD KEY `profiles_ref_cast_foreign` (`ref_cast`),
  ADD KEY `profiles_ref_country_foreign` (`ref_country`),
  ADD KEY `profiles_ref_language_foreign` (`ref_language`),
  ADD KEY `profiles_ref_current_order_foreign` (`ref_current_order`);

--
-- Indexes for table `religions`
--
ALTER TABLE `religions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `states_ref_country_foreign` (`ref_country`);

--
-- Indexes for table `stories`
--
ALTER TABLE `stories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_plan_mapped`
--
ALTER TABLE `user_plan_mapped`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advocate_plan_mapped_status_index` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `casts`
--
ALTER TABLE `casts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kycs`
--
ALTER TABLE `kycs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `match_preferences`
--
ALTER TABLE `match_preferences`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `memberships`
--
ALTER TABLE `memberships`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_activities`
--
ALTER TABLE `member_activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `policies`
--
ALTER TABLE `policies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `religions`
--
ALTER TABLE `religions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stories`
--
ALTER TABLE `stories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `user_plan_mapped`
--
ALTER TABLE `user_plan_mapped`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `casts`
--
ALTER TABLE `casts`
  ADD CONSTRAINT `casts_ref_religion_foreign` FOREIGN KEY (`ref_religion`) REFERENCES `religions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ref_country_foreign` FOREIGN KEY (`ref_country`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cities_ref_state_foreign` FOREIGN KEY (`ref_state`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kycs`
--
ALTER TABLE `kycs`
  ADD CONSTRAINT `kycs_ibfk_2` FOREIGN KEY (`ref_profile_id`) REFERENCES `profiles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `match_preferences`
--
ALTER TABLE `match_preferences`
  ADD CONSTRAINT `match_preferences_ref_cast_foreign` FOREIGN KEY (`ref_cast`) REFERENCES `casts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `match_preferences_ref_country_foreign` FOREIGN KEY (`ref_country`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `match_preferences_ref_language_foreign` FOREIGN KEY (`ref_language`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `match_preferences_ref_profile_foreign` FOREIGN KEY (`ref_profile`) REFERENCES `profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `match_preferences_ref_religion_foreign` FOREIGN KEY (`ref_religion`) REFERENCES `religions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `member_activities`
--
ALTER TABLE `member_activities`
  ADD CONSTRAINT `member_activities_ibfk_1` FOREIGN KEY (`from_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `member_activities_ibfk_2` FOREIGN KEY (`to_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`ref_user`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profiles_ref_cast_foreign` FOREIGN KEY (`ref_cast`) REFERENCES `casts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profiles_ref_country_foreign` FOREIGN KEY (`ref_country`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profiles_ref_current_order_foreign` FOREIGN KEY (`ref_current_order`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profiles_ref_language_foreign` FOREIGN KEY (`ref_language`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profiles_ref_religion_foreign` FOREIGN KEY (`ref_religion`) REFERENCES `religions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profiles_ref_user_foreign` FOREIGN KEY (`ref_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `states`
--
ALTER TABLE `states`
  ADD CONSTRAINT `states_ref_country_foreign` FOREIGN KEY (`ref_country`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
