<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/resources/')->name('resources.')->group(function () {

    //GET CASTS BY RELIGION ID
    Route::get('/getCastsByReligionID','Admin\CastController@getAllCastsByReligionID')->name('getCastByReligionID');

    //GET STATES BY COUNTRY ID
    Route::get('/getStatesByCountryID','Admin\StateController@getAllStatesByCountryID')->name('getStateByCountryID');

    //GET CITIES BY CITY ID
    Route::get('/getCitiesByStateID','Admin\CityController@getAllCitiesByStateID')->name('getCityByCountryID');
});
